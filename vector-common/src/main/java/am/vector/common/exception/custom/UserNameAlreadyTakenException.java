package am.vector.common.exception.custom;

import am.vector.common.exception.VectorException;

public class UserNameAlreadyTakenException extends VectorException {
    public UserNameAlreadyTakenException(String message) {
        super(message);
    }
}

