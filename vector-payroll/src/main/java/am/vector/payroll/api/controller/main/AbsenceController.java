package am.vector.payroll.api.controller.main;

import am.vector.common.constants.ErrorMessage;
import am.vector.common.exception.generic.BadRequestVectorException;
import am.vector.common.model.payroll.AbsenceModel;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.Absence;
import am.vector.payroll.api.service.AbsenceService;
import am.vector.payroll.util.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/absence", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class AbsenceController {

    private final AbsenceService absenceService;
    private final GsonPayrollAdapter adapter;

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findById(@PathVariable String uuid,
                                          @RequestParam(value = "fields", required = false) String fields){
        Absence response = absenceService.findById(GeneralUtil.decodeUuid(uuid));
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @GetMapping("/")
    public ResponseEntity<String> findByPersonPeriod(@RequestParam(required = false) String personUuid,
                                                     @RequestParam(required = false) String period,
                                                  @RequestParam(required = false) String fields){
        Iterable<Absence> objects;
        if(null == personUuid && null == period)
            throw new BadRequestVectorException(ErrorMessage.NO_OPTION_SELECTED.get(), "userUuid or period");

        if(null != personUuid){
            if(null != period) {
                objects = absenceService.findByPersonByPeriod(GeneralUtil.decodeUuid(personUuid), period);
            } else {
                objects = absenceService.findByPerson(GeneralUtil.decodeUuid(personUuid));
            }
        } else {
            objects = absenceService.findByPeriod(period);
        }
        return ResponseEntity.ok(adapter.toJson(objects));
    }

    @PostMapping
    public ResponseEntity<Map<String,Object>> create(@RequestBody AbsenceModel object) {
        object.setCompanyId(GeneralUtil.getCompanyId());
        Absence absence = absenceService.save(Mapper.map(object));
        return new ResponseEntity<>(GeneralUtil.uuidJSON(absence), HttpStatus.CREATED);
    }

    @PutMapping("/{uuid}")
    public void update(@RequestBody AbsenceModel model, @PathVariable String uuid) {
        String parentId = model.getParentId();
        Absence entity = Mapper.map(model.setParentId(null));
        entity.setParentId(parentId);
        entity.setId(GeneralUtil.decodeUuid(uuid));
        entity.setCompanyId(GeneralUtil.getCompanyId());
        absenceService.update(entity);
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody AbsenceModel updated, @PathVariable String uuid) {
        Absence entity = absenceService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(updated));
        absenceService.update(entity);
    }


    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable String uuid) {
        absenceService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
