package am.vector.payroll.util;

import am.vector.common.constants.Gender;
import am.vector.common.constants.LeaveType;
import am.vector.common.persistence.UUIDEntity;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

public class EntityBuilder {

    private static Person personOne = new Person()
            .setSsn("1034512395")
            .setCode("0112")
            .setFirstName("Hakobyan")
            .setLastName("Ashot")
            .setMiddleName("Grigori")
            .setEmail("hakob.abovyan@gmail.com")
            .setCell("37499618533")
            .setGender(Gender.MALE)
            .setBirthday(LocalDate.parse("2000-05-10"));

    private static Person personTwo = new Person()
            .setSsn("1034524395")
            .setCode("0113")
            .setFirstName("Grigoryan")
            .setLastName("Armine")
            .setMiddleName("Armeni")
            .setEmail("hakob.grigoryan@gmail.com")
            .setCell("37499618511")
            .setGender(Gender.FEMALE)
            .setBirthday(LocalDate.parse("1998-06-07"));

    private static Employment employmentOne = new Employment()
            .setHireDate(LocalDate.of(2019, Month.JANUARY, 21))
            .setVacationBalance(20);

    private static Employment employmentTwo = new Employment()
            .setHireDate(LocalDate.of(2018, Month.MARCH, 30))
            .setTerminationDate(LocalDate.of(2019, Month.JULY, 10))
            .setVacationBalance(25);

    private static Department departmentOne = new Department()
            .setCode("0003")
            .setName("Accounting")
            .setCostCenter("7134");

    private static Department departmentTwo = new Department()
            .setCode("0004")
            .setName("Finance")
            .setCostCenter("7135");

    private static Role roleOne = new Role()
            .setCode("0002")
            .setName("Accountant")
            .setDescription("The accountant")
            .setEligibleDays(20);

    private static Role roleTwo = new Role()
            .setCode("0003")
            .setName("Admin")
            .setDescription("Some internal staff")
            .setEligibleDays(20);

    private static Contract contractOne = new Contract()
            .setSalary(200_000)
            .setStartDate(LocalDate.parse("2018-12-12"))
            .setLaborRelation(false)
            .setDailyHours(8);

    private static Contract contractTwo = new Contract()
            .setSalary(600_000)
            .setStartDate(LocalDate.parse("2015-11-12"))
            .setEndDate(LocalDate.parse("2018-10-31"))
            .setLaborRelation(true)
            .setDailyHours(7);

    private static Absence vactionSameMonth = new Absence()
            .setLeaveType(LeaveType.VACATION)
            .setAmount(7_000L)
            .setPeriod("1018")
            .setIsParent(false)
            .setStartDate(LocalDate.parse("2018-10-13"))
            .setEndDate(LocalDate.parse("2018-10-27"));

    private static Absence vacationThreeMonths = new Absence()
            .setLeaveType(LeaveType.SICK_LEAVE)
            .setAmount(7_555L)
            .setPeriod("1118")
            .setIsParent(false)
            .setStartDate(LocalDate.parse("2018-11-15"))
            .setEndDate(LocalDate.parse("2019-02-19"));

    private static Payslip payslipOne = new Payslip()
            .setPeriod("1018")
            .setWage(100_000L)
            .setBonus(20_000L)
            .setIt(35_000L)
            .setSsp(3_000L)
            .setArmy(1_000L)
            .setOvertime(2L)
            .setHasUpdate(false);

    private static Payslip payslipTwo = new Payslip()
            .setPeriod("1018")
            .setWage(200_000L)
            .setBonus(0L)
            .setIt(55_000L)
            .setSsp(5_000L)
            .setArmy(1_000L)
            .setHasUpdate(true);

    private static TimeSheet timesheetOne = new TimeSheet()
            .setPeriod("1018")
            .setDay1(8)
            .setDay2(8)
            .setDay3(8)
            .setDay4(8)
            .setDay5(8)
            .setDay6(0)
            .setDay7(0)
            .setDay8(8)
            .setDay9(8)
            .setDay10(8)
            .setDay11(8)
            .setDay12(8)
            .setDay13(0)
            .setDay14(0)
            .setDay15(8)
            .setDay16(8)
            .setDay17(8)
            .setDay18(8)
            .setDay19(8)
            .setDay20(0)
            .setDay21(0)
            .setDay22(8)
            .setDay23(8)
            .setDay24(8)
            .setDay25(8)
            .setDay26(8)
            .setDay27(0)
            .setDay28(0)
            .setDay29(8)
            .setDay30(8)
            .setDay31(8)
            .setTotal(184);

    private static TimeSheet timesheetTwo = new TimeSheet()
            .setPeriod("1018")
            .setTotal(168);

    public static long generateId(UUIDEntity entity){
        return 1_000_000 * System.currentTimeMillis()
                + 100 * ((int) (Math.random() * 1000))
                + entity.getObjCodeIndex();
    }

    public static String generateUuid(UUIDEntity entity){
        return GeneralUtil.encodeUuid(generateId(entity));
    }

    public static Person getPersonOne(){
        return personOne.copy();
    }

    public static Person getPersonTwo(){
        return personTwo.copy();
    }

    public static List<Person> getPersonsList(){
        return Arrays.asList(personOne.copy(), personTwo.copy());
    }

    public static Employment getEmploymentOne(Person person) {
        Employment employment = employmentOne.copy();
        return employment.setPerson(person);
    }

    public static Employment getEmploymentTwo(Person person) {
        Employment employment = employmentTwo.copy();
        return employment.setPerson(person);
    }

    public static Department getDepartmentOne(){
        return departmentOne.copy();
    }

    public static Department getDepartmentTwo(){
        return departmentTwo.copy();
    }

    public static List<Department> getDepartmentsList(){
        return Arrays.asList(departmentOne.copy(), departmentTwo.copy());
    }

    public static Role getRoleOne(){
        return roleOne.copy();
    }

    public static Role getRoleTwo(){
        return roleTwo.copy();
    }

    public static List<Role> getRolesList(){
        return Arrays.asList(roleOne.copy(), roleTwo.copy());
    }

    public static Contract getContractOne(){
        return contractOne.copy();
    }

    public static Contract getContractTwo(){
        return contractTwo.copy();
    }

    public static List<Contract> getContractsList(){
        return Arrays.asList(contractOne.copy(), contractTwo.copy());
    }

    public static Absence getVacationSameMonth(){
        return vactionSameMonth.copy();
    }

    public static Absence getVacationThreeMonths(){
        return vacationThreeMonths.copy();
    }

    public static Payslip getPayslipOne(){
        return payslipOne;
    }

    public static Payslip getPayslipTwo(){
        return payslipTwo;
    }

    public static TimeSheet getTimesheetOne(){
        return timesheetOne;
    }

    public static TimeSheet getTimesheetTwo(){
        return timesheetTwo;
    }
}
