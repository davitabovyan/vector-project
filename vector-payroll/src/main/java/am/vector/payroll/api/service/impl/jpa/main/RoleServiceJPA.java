package am.vector.payroll.api.service.impl.jpa.main;

import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Role;
import am.vector.payroll.api.repository.main.RoleRepository;
import am.vector.payroll.api.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class RoleServiceJPA implements RoleService {

    private final RoleRepository roleRepository;

    public Role findById(long id){
        return roleRepository.findById(id).orElseThrow(
            ()-> new EntityNotFoundException(Role.class, GeneralUtil.encodeUuid(id))
        );
    }

    public List<Role> findAllPerCompany(){
        int companyId = GeneralUtil.getCompanyId();
        return roleRepository.findAllPerCompany(companyId);
    }

    @Transactional
    @Override
    public void update(Role object){
        GeneralUtil.setRecordStatus(object);
        roleRepository.save(object);
    }
    @Transactional
    @Override
    public Role save(Role role){
        return roleRepository.save(role);
    }

    @Override
    public List<Role> saveAll(List<Role> roles) {
       return roleRepository.saveAll(roles);
    }

    @Override
    public List<Role> findAllById(List<Long> id) {
        return roleRepository.findAllById(id);
    }

    @Override
    public void deleteById(long id) {
        roleRepository.deleteById(id);
    }

    @Override
    public void deleteByCode(String code) {
        int companyId = GeneralUtil.getCompanyId();
        roleRepository.deleteByCode(code, companyId);
    }

    @Override
    public void deleteAll(int companyId) {
        roleRepository.deleteAll(companyId);
        logger.info(String.format("All record are delete by user %s", GeneralUtil.getUserFromContext().getUsername()));
    }
}
