package am.vector.common.util;

import am.vector.common.constants.EntityObject;
import am.vector.common.exception.VectorException;
import am.vector.common.exception.custom.UnsupportedJsonKeyException;
import am.vector.common.exception.custom.UnsupportedJsonValueException;
import am.vector.common.exception.generic.BadRequestVectorException;
import am.vector.common.persistence.UUIDEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class IOUtil {
    private static final Logger logger = LogManager.getLogger();

    /**
     * Reads xml file into Document file
     */
    public static Document readXML(String fileName) {
        File xmlFile;
        try {
            xmlFile = ResourceUtils.getFile(String.format("classpath:static_data/%s.xml",fileName));
        } catch (FileNotFoundException e) {
            logger.warn("Code #1002: The day-shift.xml file is not present in the server under static_data/ path");
            throw new VectorException("1002");
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        //optional, but recommended
        //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
        if(doc != null) {
            doc.getDocumentElement().normalize();
        }
        return doc;
    }

    /**
     * Parse fields in url to Map
     * @param fields fields separated by comma, complex fields has ":" after it with dot separating its nested fields
     * @return field name as key and in case of complex fields its nested fields separated by comma for later parsing
     */
    public static Map<String, String> parseFields(Class<? extends UUIDEntity> clazz, String fields){
        Map<String, String> entityFields = new HashMap<>();
        if(null == fields) {
            entityFields.put("uuid", null);
            return entityFields;
        }
        for(String field: fields.split(",")){
            field = field.trim();
            if(field.contains(":")) {
                String[] compound = field.split(":");
                String entity = compound[0];
                String nestedFields = compound[1].replace('.',',');
                entityFields.put(entity, nestedFields);
            } else {
                if(!"*".equals(field) && !fieldBelongsToEntity(clazz, field)){
                    throw new BadRequestVectorException("error.unsupported_field", field, EntityObject.getByClass(clazz).getCode());
                }
                entityFields.put(field, null);
            }
        }
        return entityFields;
    }

    private static boolean fieldBelongsToEntity(Class<? extends UUIDEntity> clazz, String provideField){
        for(Field field : clazz.getDeclaredFields()){
            if(provideField.equals(field.getName())) return true;
        }
        return false;
    }

    /**
     * Returns a copy of the object, or null if the object cannot
     * be serialized.
     */
    public static Object copy(Object orig) {
        Object obj = null;
        try {
            // Write the object out to a byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(orig);
            out.flush();
            out.close();

            // Make an input stream from the byte array and read
            // a copy of the object back in.
            ObjectInputStream in = new ObjectInputStream(
                new ByteArrayInputStream(bos.toByteArray()));
            obj = in.readObject();
        }
        catch(IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static <T> T getJSONElement(Map<String, Object> map, String key, Class<T> clazz){
        if(!map.containsKey(key)) throw new UnsupportedJsonKeyException(key);
        try{
            return  clazz.cast(map.get(key));
        } catch (ClassCastException ex) {
            throw new UnsupportedJsonValueException(key, clazz);
        }
    }
}
