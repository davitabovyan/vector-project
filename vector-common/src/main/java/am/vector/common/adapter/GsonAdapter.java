package am.vector.common.adapter;

import am.vector.common.constants.EntityObject;
import am.vector.common.persistence.UUIDEntity;
import am.vector.common.util.IOUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.*;

public abstract class GsonAdapter {
    public static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(LocalDate.class, new LocalDateAdapter().nullSafe())
            .create();

    private static final String EMPTY_LIST = "[]";
    private final Map<EntityObject, List<EntityObject>> dependencies = new HashMap<>();
    private final Map<EntityObject, Gson> gsonMap = new HashMap<>();

    protected void setup() {
        for(EntityObject entityObject : EntityObject.values()){
            gsonMap.put(entityObject, createGson(entityObject, Collections.singletonMap("*", null)));
        }
    }

    public <T extends UUIDEntity> String toJson(T entity) {
        return toJson(entity, Collections.emptyMap());
    }

    public <T extends UUIDEntity> String toJson(T entity, Map<String, String> fields) {
        if(fields.isEmpty()) {
            return getGson(entity.getObjCode()).toJson(entity);
        } else {
            return createGson(entity.getObjCode(), fields).toJson(entity);
        }
    }

    public <T extends UUIDEntity> String toJson(T entity, String fields) {
        return toJson(entity, IOUtil.parseFields(entity.getClass(), fields));
    }

    public <Z> String toJson(Type typeOfT, Z entity) {
        return GSON.toJson(entity, typeOfT);
    }

    public <T extends UUIDEntity> String toJson(Iterable<T> entity) {
        return toJson(entity, Collections.emptyMap());
    }

    public <T extends UUIDEntity> String toJson(List<T> entity, String fields) {
        if (entity.size() == 0) return EMPTY_LIST;
        return toJson(entity, IOUtil.parseFields(entity.get(0).getClass() ,fields));
    }

    public <T extends UUIDEntity> String toJson(Iterable<T> entities, Map<String, String> fields) {
        if(!entities.iterator().hasNext()){
            return EMPTY_LIST;
        }
        if(fields.isEmpty()) {
            return getGson(entities.iterator().next().getObjCode()).toJson(entities);
        } else {
            return createGson(entities.iterator().next().getObjCode(), fields).toJson(entities);
        }
    }

    protected void addToMap(EntityObject... types) {
        dependencies.put(types[0], Arrays.asList(types));
    }

    protected abstract void registerAdapter(GsonBuilder builder, EntityObject objCode, Map<String, String> fields);

    private Gson getGson(EntityObject objCode){
        return gsonMap.get(objCode);
    }

    private Gson createGson(EntityObject objCode, Map<String, String> fields){
        if(!dependencies.containsKey(objCode)) return GSON;
        GsonBuilder builder = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateAdapter().nullSafe());
        for(EntityObject type : dependencies.get(objCode)) {
            registerAdapter(builder, type, fields);
        }
        return builder.create();
    }
}
