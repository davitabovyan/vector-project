package am.vector.journal;

import am.vector.common.util.GeneralUtil;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;

public class Playground {
    static AA a = new AA(111L);

    public static void main(String[] args) {

        ModelMapper mm = new ModelMapper();

        TypeMap<A, B> typeMap = mm.createTypeMap(A.class, B.class)
                .addMapping(A::getId, B::setUuid);

        typeMap.include(AA.class, BB.class);

        BB resultM = mm.map(a, BB.class);
        resultM = null;
    }

    static class A {
        private long id;

        public A(long i) {
            id = i;
        }

        public long getId() {
            return id;
        }

        public void setUUID(String uuid) {
            id = GeneralUtil.decodeUuid(uuid);
        }
    }

    static class AA extends A {
        public AA(long i) {
            super(i);
        }
    }

    static class B {
        String uuid;

        public String getUuid() {
            return uuid;
        }

        public void setUuid(long id) {
            uuid = GeneralUtil.encodeUuid(id);
        }
    }

    static class BB extends B {
        public BB() {
        }
    }
}
