package am.vector.payroll.api.v1.unit.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DayUtilUnitTests.class
})
public class UtilTestSuite {
}
