package am.vector.payroll.api.entity.main.embedded;

import am.vector.common.exception.VectorException;
import am.vector.common.util.CalculationUtil;
import lombok.Data;

import javax.persistence.Embeddable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Embeddable
public class SalaryAverage {
    private Long month1;
    private Long month2;
    private Long month3;
    private Long month4;
    private Long month5;
    private Long month6;
    private Long month7;
    private Long month8;
    private Long month9;
    private Long month10;
    private Long month11;
    private Long month12;

    public List<Long> asList() {
        List<Long> list = new ArrayList<>();
        addToList(list, month1);
        addToList(list, month2);
        addToList(list, month3);
        addToList(list, month4);
        addToList(list, month5);
        addToList(list, month6);
        addToList(list, month7);
        addToList(list, month8);
        addToList(list, month9);
        addToList(list, month10);
        addToList(list, month11);
        addToList(list, month12);
        return list;
    }

    public void setVelue(int index, Long value) {
        switch (index) {
            case 1: this.month1 = value; break;
            case 2: this.month2 = value; break;
            case 3: this.month3 = value; break;
            case 4: this.month4 = value; break;
            case 5: this.month5 = value; break;
            case 6: this.month6 = value; break;
            case 7: this.month7 = value; break;
            case 8: this.month8 = value; break;
            case 9: this.month9 = value; break;
            case 10: this.month10 = value; break;
            case 11: this.month11 = value; break;
            case 12: this.month12 = value; break;
            default: throw new VectorException("Incorrect value for index, should be in [1-12] range.");
        }
    }

    public BigDecimal get() {
        return CalculationUtil.calculateAverage(asList());
    }

    private void addToList(List<Long> list, Long number) {
        if(number != null) list.add(number);
    }
}
