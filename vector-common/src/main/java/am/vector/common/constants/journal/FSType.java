package am.vector.common.constants.journal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum FSType {
    BALANCE_SHEET("Consolidated statement of financial position"),
    INCOME_STATEMENT("Consolidated statement of profit or loss and other comprehensive income"),
    CHANGES_IN_EQUITY("Consolidated statement of changes in equity"),
    CASH_FLOW("Consolidated statement of cash flows"),
    NOTES("Notes to the consolidated financial statements");

    private String description;
}
