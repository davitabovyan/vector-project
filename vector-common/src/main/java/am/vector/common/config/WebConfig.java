package am.vector.common.config;

import am.vector.common.config.interceptor.AuthCodeInterceptor;
import am.vector.common.config.interceptor.LogInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthCodeInterceptor());
        registry.addInterceptor(new LogInterceptor());
    }
}
