package am.vector.payroll;

import am.vector.payroll.api.validator.TimesheetValidator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@SpringBootApplication(scanBasePackages = {"am.vector.payroll", "am.vector.common.config"})
public class PayrollApplication implements RepositoryRestConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(PayrollApplication.class, args);
	}

	@Override
	public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
		validatingListener.addValidator("beforeCreate", new TimesheetValidator());
	}
}
