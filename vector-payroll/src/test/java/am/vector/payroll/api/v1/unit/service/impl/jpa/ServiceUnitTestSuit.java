package am.vector.payroll.api.v1.unit.service.impl.jpa;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AbsenceServiceJPAUnitTests.class
})
public class ServiceUnitTestSuit {
}
