package am.vector.common.rest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

public interface BaseApiInterface {
    ResponseEntity<String> makeGet(HttpHeaders headers, String uri);
    ResponseEntity<String> makePost(HttpHeaders headers, String uri, String body);
    ResponseEntity<String> makePut(HttpHeaders headers, String uri, String body);
    ResponseEntity<String> makePatch(HttpHeaders headers, String uri, String body);
    ResponseEntity<String> makeDelete(HttpHeaders headers, String uri);
}