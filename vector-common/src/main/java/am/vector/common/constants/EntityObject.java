package am.vector.common.constants;

import am.vector.common.exception.VectorException;
import am.vector.common.model.admin.CompanyModel;
import am.vector.common.model.admin.UserModel;
import am.vector.common.model.common.DepartmentModel;
import am.vector.common.model.journal.AccountGroupModel;
import am.vector.common.model.journal.AccountModel;
import am.vector.common.model.payroll.*;

public enum EntityObject {

    CLIENT(         0,"SQ",0b1L, CompanyModel.class),
    COMPANY(        1,"PR",0b10L, CompanyModel.class),
    USER(           2,"SQ",0b100L, UserModel.class),
    PERSON(         3,"PR",0b1000L, PersonModel.class),
    DEPARTMENT(     4,"PR",0b10000L, DepartmentModel.class),
    ROLE(           5,"PR",0b100000L, RoleModel.class),
    CONTRACT(       6,"PR",0b1000000L, ContractModel.class),
    ABSENCE(        7,"PR",0b10000000L, AbsenceModel.class),
    EMPLOYMENT(     8,"PR",0b100000000L, EmploymentModel.class),
    PAYSLIP(        9,"PR",0b1000000000L, PayslipModel.class),
    TIMESHEET(      10,"PR",0b10000000000L, TimeSheetModel.class),
    UPLOAD(         11,"PR",0b100000000000L, Object.class),
    WORKDAY(        12,"PR",0b1000000000000L, WorkdayModel.class),
    ACCOUNT(        13,"GL",0b10000000000000L, AccountModel.class),
    ACCOUNT_GROUP(  14,"GL",0b100000000000000L, AccountGroupModel.class),
    COUNTERPARTY(   15,"GL",0b1000000000000000L, AccountGroupModel.class);

    private final long access;
    private final int index;
    private final String module;
    private final Class<?> clazz;

    EntityObject(int index, String module, long access, Class<?> clazz){
        this.index = index;
        this.module = module;
        this.access = access;
        this.clazz = clazz;
    }

    public String getCode(){
        return this.name().toLowerCase();
    }
    public long getAccess(){
        return access;
    }
    public int getIndex(){
        return index;
    }
    public Class<?> getClazz(){
        return clazz;
    }

    public static EntityObject getByIndex(int index){
        for(EntityObject obj : EntityObject.values()){
            if(obj.getIndex() == index) return obj;
        }
        throw new VectorException(String.format("Incorrect index %d for EntityObject", index));
    }

    public static EntityObject getByClass(Class<?> clazz){
        for(EntityObject obj : EntityObject.values()){
            if(clazz.equals(obj.getClazz())) return obj;
        }
        throw new VectorException(String.format("Incorrect class %d for EntityObject", clazz));
    }

    public static EntityObject getByCode(String objectCode){
        try {
            return EntityObject.valueOf(objectCode.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new VectorException("error.unsupported_object", objectCode);
        }
    }
}
