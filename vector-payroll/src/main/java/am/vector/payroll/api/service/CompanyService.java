package am.vector.payroll.api.service;

import am.vector.common.model.admin.UserModel;
import am.vector.payroll.api.entity.admin.Company;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface CompanyService {
	Logger logger = LogManager.getLogger();

	Iterable<Company> findAll();
	Company findById(int companyId);
	Company save(Company object);
	void deleteById(int id);
	void deleteById(UserModel user);
}
