package am.vector.payroll.api.repository.internal;

import am.vector.payroll.api.entity.admin.Workday;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.QueryHint;
import java.time.LocalDate;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "workday", path = "workday", exported = false)
public interface WorkdayRepository extends CrudRepository<Workday, LocalDate> {

    @Query("SELECT w FROM Workday w where w.day BETWEEN :startDate AND :endDate")
    @QueryHints({
            @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE, value = "true"),
            @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHE_REGION, value = "workdays")
    })
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
    List<Workday> findForPeriod(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Transactional
    @Modifying
    @Query("DELETE FROM Workday w where w.day = :day")
    void deleteById(@Param("day") LocalDate day);
}
