package am.vector.payroll.api.support;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.Month;

@Accessors(chain = true) @Getter @Setter
@AllArgsConstructor
public class PayrollPeriod {
	private int year;
	private Month month;

	public LocalDate getFirstDay(){
		return LocalDate.of(year, month, 1);
	}

	public LocalDate getLastDay(){
		LocalDate date = getFirstDay();
		return date.plusDays(date.lengthOfMonth() - 1);
	}

	public String getPeriodString(){
		return String.format("%02d%d", month.getValue(), year % 100);
	}

	public int getSize(){
		return getFirstDay().lengthOfMonth();
	}
}
