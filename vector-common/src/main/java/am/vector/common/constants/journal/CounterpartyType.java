package am.vector.common.constants.journal;

public enum CounterpartyType {
    SUPPLIER,
    CUSTOMER,
    ON_ACCOUNT,
    BUDGET,
    OTHER;
}
