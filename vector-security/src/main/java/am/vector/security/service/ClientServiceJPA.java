package am.vector.security.service;

import am.vector.common.constants.ClientType;
import am.vector.common.constants.EntityObject;
import am.vector.common.constants.Module;
import am.vector.common.exception.VectorException;
import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.model.admin.CompanyModel;
import am.vector.common.model.admin.UserModel;
import am.vector.common.rest.Api;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.Pair;
import am.vector.security.dto.ClientDto;
import am.vector.security.entity.Client;
import am.vector.security.repository.ClientRepository;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClientServiceJPA implements ClientService {

    private final ClientRepository clientRepository;
    private final UserService userService;

    private final Gson GSON = new Gson();

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    public Client findClientById(String id) {
        Optional<Client> client = clientRepository.findById(Integer.parseInt(id));
        if(client.isPresent()) {
            return client.get();
        } else {
            throw new EntityNotFoundException(Client.class, id);
        }
    }

    public Client findClientByEmail(String email) {
        return clientRepository.findByEmail(email);
    }

    @Transactional
    public void update(Client client) {
        clientRepository.save(client);
    }

    @Transactional
    public CompanyModel addCompany(CompanyModel company, Integer clientId) {

        Pair<Integer, String> response = Api.create(Module.PAYROLL, GSON.toJson(company), EntityObject.COMPANY);
        if(response.getFirst() == 201) {
            int companyId = GeneralUtil.decodeUuidToInt(response.getSecond());
            company.setId(companyId);
            Client client = clientRepository.findById(clientId)
                    .orElseThrow(()-> new EntityNotFoundException(Client.class, clientId.toString()));
            client.getCompanies().put(company.getTin(), companyId);
            clientRepository.save(client);
        }
        return company;
    }

    @Transactional
    public Client save(ClientDto dto) {
        Client client = new Client();
        client.setName(dto.getName());
        client.setCell(dto.getCell());
        client.setState(true);
        client.setType(ClientType.FREE);
        client.setEmail(dto.getEmail());
        client = clientRepository.save(client);

        if(client.getId() > 0){
            CompanyModel company = new CompanyModel();
            company.setEmail(dto.getEmail());
            company.setTin(dto.getTin());
            company.setName(dto.getName());
            company.setWorkWeek(5);

            Pair<Integer, String> response = Api.create(Module.PAYROLL, GSON.toJson(company), EntityObject.COMPANY);
            if(response.getFirst() == 201) {

                Map<String, Integer> companies = new HashMap<>();
                companies.put(dto.getTin(), GeneralUtil.decodeUuidToInt(response.getSecond()));
                client.setCompanies(companies);

                UserModel user = new UserModel()
                        .setClientId(client.getId())
                        .setCompanyId(GeneralUtil.decodeUuidToInt(response.getSecond()))
                        .setAdmin(true)
                        .setSuperAdmin(false)
                        .setUsername(String.format("%s<%s>",dto.getEmail(),dto.getTin()))
                        .setPassword(dto.getPass());
                userService.save(user);
                return client;
            } else {
                throw new VectorException("Failed to save company: " + GSON.toJson(company));
            }
        } else {
            throw new VectorException("Failed to save client: " + GSON.toJson(client));
        }
    }

    public void remove(Integer id) {
        clientRepository.deleteById(id);
    }
}
