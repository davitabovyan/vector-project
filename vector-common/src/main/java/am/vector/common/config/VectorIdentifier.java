package am.vector.common.config;

import java.io.Serializable;

import am.vector.common.persistence.ClientCodeId;
import am.vector.common.persistence.TimestampId;
import am.vector.common.exception.VectorException;
import am.vector.common.util.GeneralUtil;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
// import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;

public class VectorIdentifier implements IdentifierGenerator {

    private static volatile int counter = 0;

    @Override
    public Serializable generate(SharedSessionContractImplementor sharedSessionContractImplementor, Object o)
        throws HibernateException {

        if(o instanceof TimestampId) {
            TimestampId entity = (TimestampId) o;
            return 1_000_000 * System.currentTimeMillis()
                    + 1000 * increment()
                    + entity.getObjCodeIndex();
        } else if (o instanceof ClientCodeId) {
            ClientCodeId entity = (ClientCodeId) o;
            return 10_000_000_000_000L * GeneralUtil.getCompanyId()
                    + 1_000_000 * (System.nanoTime() % 10_000_000)
                    + 1000 * increment()
                    + entity.getObjCodeIndex();
        } else {
            throw new VectorException("Provided Entity object is not supported for id generation");
        }
    }

    private synchronized int increment(){
        return counter < 1000 ? ++counter : 0;
    }
}
