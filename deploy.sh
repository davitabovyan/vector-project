#!/bin/sh
 
echo "** Building and deploying! *********************************************"

echo "git update"
git pull
cd vector-$1
echo "../mvnw clean install -DskipTests"
../mvnw clean install -DskipTests
if [[ "$?" != "0" ]]; then
    echo "[Error] Running clean install"
    exit 1
else
    echo "stopping current run"
    kill $(ps aux | grep -i vector-$1/target | awk '{print $2}' | head -n 1)

    echo "mvn spring-boot:run"
    mvn spring-boot:run
    if [[ "$?" != "0" ]]; then
        echo "[Error] spring-boot:run"
        exit 1
    fi
fi
