package am.vector.payroll.api.entity.main;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.LeaveType;
import am.vector.common.persistence.TimestampId;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.embedded.SalaryAverage;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Accessors(chain = true) @Getter @Setter
public class Absence extends TimestampId {

    @Enumerated(EnumType.STRING)
    private LeaveType leaveType;
    private Long amount;
    private String period;
    private LocalDate startDate;
    private LocalDate endDate;
    private Boolean isParent;
    private Long parentId;
    @Embedded
    private SalaryAverage averageMonthly;
    @ManyToOne(fetch=FetchType.LAZY, optional=false)
    private Person person;

    public void assignParentId(String parentId) {
        this.parentId = GeneralUtil.decodeUuid(parentId);
    }

    public Absence setParentId(Long parentId) {
        this.parentId = parentId;
        return this;
    }
    public void setParentId(String parentId) {
        assignParentId(parentId);
    }

    public void assignLeaveType(String leaveType) {
        this.leaveType = LeaveType.valueOf(leaveType);
    }

    public boolean isNotParent(){
        return !isParent();
    }

    public boolean isParent(){
        return isParent != null && isParent;
    }

    public Absence() {
        super(EntityObject.ABSENCE);
    }

    public Absence(long uuid) {
        super(EntityObject.ABSENCE, uuid);
    }

    public Absence copy(){
        return (Absence) super.copy(this);
    }

}
