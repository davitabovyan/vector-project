package am.vector.payroll.api.adapter;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DepartmentTypeAdapterUnitTests.class,
        AbsenceTypeAdapterUnitTests.class,
        ContractTypeAdapterUnitTests.class,
        EmploymentTypeAdapterUnitTests.class,
        PayslipTypeAdapterUnitTests.class,
        TimeSheetTypeAdapterUnitTests.class,
        PersonTypeAdapterUnitTests.class,
        RoleTypeAdapterUnitTests.class,
})
public class AdapterUnitTestsSuite {
}
