package am.vector.security.service;

import java.util.List;
import java.util.Map;

import am.vector.common.model.admin.UserModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface UserService {
    Logger logger = LogManager.getLogger();

    UserModel findUserByUserName(String userName);
    UserModel save(UserModel user);
    void removeUser(String userName);
    void updatePermissions(UserModel user, Map<String, String> permissions);
    List<UserModel> findAll(int companyId);
}
