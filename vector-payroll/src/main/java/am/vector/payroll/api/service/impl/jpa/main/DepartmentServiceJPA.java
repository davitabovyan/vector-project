package am.vector.payroll.api.service.impl.jpa.main;

import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.repository.main.DepartmentRepository;
import am.vector.payroll.api.service.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class DepartmentServiceJPA implements DepartmentService {

    private final DepartmentRepository departmentRepository;

    public Department findById(long id){
        return departmentRepository.findById(id).orElseThrow(
            ()-> new EntityNotFoundException(Department.class, GeneralUtil.encodeUuid(id))
        );
    }

    public List<Department> findAllPerCompany(){
        return departmentRepository.findAllPerCompany(GeneralUtil.getCompanyId());
    }

    @Transactional
    @Override
    public void update(Department object){
        GeneralUtil.setRecordStatus(object);
        departmentRepository.save(object);
    }

    @Transactional
    @Override
    public Department save(Department object){
        return departmentRepository.save(object);
    }

    @Override
    public List<Department> saveAll(List<Department> departments) {
        return departmentRepository.saveAll(departments);
    }

    @Override
    public List<Department> findAllById(List<Long> id) {
        return departmentRepository.findAllById(id);
    }

    @Override
    public void deleteById(long id) {
        departmentRepository.deleteById(id);
    }

    @Override
    public void deleteAll(int companyId) {
        departmentRepository.deleteAll(companyId);
        logger.info(String.format("All record are delete by user %s", GeneralUtil.getUserFromContext().getUsername()));
    }

    public Department getDepartmentWithHead(long id){
        return departmentRepository.getDepartmentWithHead(id).orElseThrow(
            ()-> new EntityNotFoundException(Department.class, GeneralUtil.encodeUuid(id))
        );
    }

    public List<Department> getAllDepartmentsWithHead(){
        return departmentRepository.getAllDepartmentsWithHead(GeneralUtil.getCompanyId());
    }

    @Override
    public Department getByCode(String code) {
        return departmentRepository.findFirstByCodeAndCompanyIdOrderByIdAsc(code, GeneralUtil.getCompanyId())
            .orElseThrow( ()-> new EntityNotFoundException(Department.class, code));
    }

    @Override
    public void deleteByCode(String code) {
        departmentRepository.deleteByCode(code, GeneralUtil.getCompanyId());
    }
}
