package am.vector.payroll.api.v1.integration.controller.cases;

import am.vector.payroll.PayrollApplication;
import am.vector.payroll.api.entity.main.Absence;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.v1.integration.controller.ControllerBaseTest;
import am.vector.payroll.util.EntityBuilder;
import am.vector.payroll.util.PayrollApi;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PayrollApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AbsenceControllerIntegrationTests extends ControllerBaseTest {

    private Absence vacationSameMonth;

    @Before
    public void setup() {
        Person person = create(EntityBuilder.getPersonOne());
        vacationSameMonth = EntityBuilder.getVacationSameMonth();
        vacationSameMonth.setPerson(person);
    }

    @Test
    public void createAbsence_success() {
        // Setup
        // Act
        // Assert
        testCreate(vacationSameMonth, adapter.toJson(vacationSameMonth));
    }

    @Test
    public void createVacationSameMonth_success() {
        // Setup
        // Act
        Absence result = create(vacationSameMonth);
        // Assert
        compareExpectedWithActual(vacationSameMonth, result);
    }

    @Test
    public void getVacationSameMonth_success() {
        // Setup
        Absence vacation = create(vacationSameMonth);
        // Act
        Absence result = getAbsence(vacation.getId());
        // Assert
        compareExpectedWithActual(vacation, result);
    }

    @Test
    public void createVacationThreeMonths_success() {
        // Setup
        Person person = create(EntityBuilder.getPersonTwo());
        Absence vacationThreeMonths = EntityBuilder.getVacationThreeMonths().setPerson(person);
        // Act
        Absence result = create(vacationThreeMonths);
        // Assert
        compareExpectedWithActual(vacationThreeMonths, result);
    }

    @Ignore
    @Test
    public void createVacationThreeMonthsCheckGroups_success() {
        // Setup
        Person person = create(EntityBuilder.getPersonTwo());
        Absence vacationThreeMonths = EntityBuilder.getVacationThreeMonths().setPerson(person);
        // Act
        create(vacationThreeMonths);
        // Assert
        List<Absence> groupVacations = PayrollApi.getGroupAbsences(getHeaders(), getHostAndPort(), person.getId());
        assertEquals("Number of auto-created vacations in the group is incorrect", 5, groupVacations.size());

        List<Absence> parent = groupVacations.stream().filter(Absence::isParent).collect(Collectors.toList());
        assertEquals("The are more then one parent vacation object", 1, parent.size());
        assertNull("ParentId field is set for parent vacation object", parent.get(0).getParentId());

        List<Absence> children = groupVacations.stream().filter(Predicate.not(Absence::isParent)).collect(Collectors.toList());
        for(Absence child : children){
            assertNotNull("Child object's parentId field is not set", child.getParentId());
            assertEquals("Child object's parentId is incorrect", parent.get(0).getId(),  child.getParentId());
            if("1118".equals(child.getPeriod())){
                assertEquals("Child object's start date is not correctly set",
                        LocalDate.of(2018,11,15), child.getStartDate());
                assertEquals("Child object's end date is not correctly set",
                        LocalDate.of(2018,11,30), child.getEndDate());
            } else if("1218".equals(child.getPeriod())){
                assertEquals("Child object's start date is not correctly set",
                        LocalDate.of(2018,12,1), child.getStartDate());
                assertEquals("Child object's end date is not correctly set",
                        LocalDate.of(2018,12,31), child.getEndDate());
            } else if("0119".equals(child.getPeriod())){
                assertEquals("Child object's start date is not correctly set",
                        LocalDate.of(2019,1,1), child.getStartDate());
                assertEquals("Child object's end date is not correctly set",
                        LocalDate.of(2019,1,31), child.getEndDate());
            } else if("0219".equals(child.getPeriod())){
                assertEquals("Child object's start date is not correctly set",
                        LocalDate.of(2019,2,1), child.getStartDate());
                assertEquals("Child object's end date is not correctly set",
                        LocalDate.of(2019,2,19), child.getEndDate());
            }
        }

    }

    private void compareExpectedWithActual(Absence expected, Absence actual) {
        assertEquals("Leave type is not correct", expected.getLeaveType(), actual.getLeaveType());
        assertEquals("Amount is not correct", expected.getAmount(), actual.getAmount());
        assertEquals("Period is not correct", expected.getPeriod(), actual.getPeriod());
        assertEquals("Start date is not correct", expected.getStartDate(), actual.getStartDate());
        assertEquals("End date is not correct", expected.getEndDate(), actual.getEndDate());
        assertEquals("Person is not set", expected.getPerson().getId(), actual.getPerson().getId());
    }
}
