package am.vector.common.exception.custom;

public class UnsupportedJsonKeyException extends ProvidedJsonException {

    public UnsupportedJsonKeyException(String key) {
        super(key);
    }
}

