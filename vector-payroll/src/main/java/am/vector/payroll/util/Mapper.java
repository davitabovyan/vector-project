package am.vector.payroll.util;

import am.vector.common.model.common.DepartmentModel;
import am.vector.common.model.payroll.*;
import am.vector.payroll.api.entity.main.*;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;

public class Mapper {
    public static ModelMapper MAPPER = new ModelMapper();
    static {
            MAPPER.getConfiguration().setPropertyCondition(Conditions.isNotNull());
    }

    public static Absence map(AbsenceModel model) {
        return MAPPER.map(model, Absence.class);
    }

    public static Contract map(ContractModel model) {
        return MAPPER.map(model, Contract.class);
    }

    public static Department map(DepartmentModel model) {
        return MAPPER.map(model, Department.class);
    }

    public static Employment map(EmploymentModel model) {
        return MAPPER.map(model, Employment.class);
    }

    public static Payslip map(PayslipModel model) {
        return MAPPER.map(model, Payslip.class);
    }

    public static Person map(PersonModel model) {
        return MAPPER.map(model, Person.class);
    }

    public static Role map(RoleModel model) {
        return MAPPER.map(model, Role.class);
    }

    public static TimeSheet map(TimeSheetModel model) {
        return MAPPER.map(model, TimeSheet.class);
    }
}
