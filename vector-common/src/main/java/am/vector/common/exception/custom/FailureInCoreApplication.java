package am.vector.common.exception.custom;

public class FailureInCoreApplication  extends RuntimeException  {
    public FailureInCoreApplication() {}
    public FailureInCoreApplication(String message) {
        super(message);
    }
    public FailureInCoreApplication(String message, Throwable cause) {
        super(message, cause);
    }
}
