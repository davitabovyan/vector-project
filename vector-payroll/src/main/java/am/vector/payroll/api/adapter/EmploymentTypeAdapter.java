package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.payroll.api.entity.main.Employment;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class EmploymentTypeAdapter extends CustomAdapter<Employment> {

    public EmploymentTypeAdapter(String fields){
        super(Employment.class, fields);
    }

    public EmploymentTypeAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, Employment employment, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("hireDate")) parseDate("hireDate", jsonWriter, employment.getHireDate());
        if(all || fields.containsKey("terminationDate")) parseDate("terminationDate", jsonWriter, employment.getTerminationDate());
        if(all || fields.containsKey("vacationBalance")) jsonWriter.name("vacationBalance").value(employment.getVacationBalance());

        if(all || fields.containsKey("person")){
            jsonWriter.name("person");
            new PersonTypeAdapter(fields.getOrDefault("person", null))
                    .write(jsonWriter, employment.getPerson());
        }

        return jsonWriter;
    }
}
