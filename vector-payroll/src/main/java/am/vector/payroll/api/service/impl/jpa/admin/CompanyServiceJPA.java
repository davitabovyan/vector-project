package am.vector.payroll.api.service.impl.jpa.admin;

import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.model.admin.CompanyModel;
import am.vector.common.model.admin.UserModel;
import am.vector.payroll.api.entity.admin.Company;
import am.vector.payroll.api.repository.admin.CompanyRepository;
import am.vector.payroll.api.service.CompanyService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class CompanyServiceJPA implements CompanyService {

    private final CompanyRepository companyRepository;

    public Iterable<Company> findAll() {
        return companyRepository.findAll();
    }

    public Company findById(int companyId){
        return companyRepository.findById(companyId)
                .orElseThrow(() -> new EntityNotFoundException(Company.class, "" + companyId));
    }

    @Transactional
    public Company save(Company object){
        return companyRepository.save(object);
    }

    public void deleteById(int id) {
        companyRepository.deleteById(id);
    }

    public void deleteById(UserModel user) {
        companyRepository.deleteById(user.getCompanyId());
        logger.info(String.format("Delete by user %s", user.getUsername()));
    }

}
