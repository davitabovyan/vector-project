package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.payroll.api.entity.main.Department;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class DepartmentTypeAdapter extends CustomAdapter<Department> {

    public DepartmentTypeAdapter(String fields){
        super(Department.class, fields);
    }

    public DepartmentTypeAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, Department department, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("code")) jsonWriter.name("code").value(department.getCode());
        if(all || fields.containsKey("name")) jsonWriter.name("name").value(department.getName());
        if(all || fields.containsKey("costCenter")) jsonWriter.name("costCenter").value(department.getCostCenter());

        if(all || fields.containsKey("head")){
            jsonWriter.name("head");
            new PersonTypeAdapter(fields.getOrDefault("head", null))
                    .write(jsonWriter, department.getHead());
        }

        return jsonWriter;
    }
}
