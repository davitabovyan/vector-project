package am.vector.common.model.journal;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.journal.FSItems;
import am.vector.common.constants.journal.JournalAccountType;
import am.vector.common.model.MainModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true) @Getter @Setter
public class AccountModel extends MainModel {

    private String code;
    private String name;
    private String displayName;
    private Boolean isParent;
    private JournalAccountType type;
    private FSItems fsItem;
    private AccountModel parent;

    public void setFsItem(String fsItem) {
        this.fsItem = fsItem != null ? FSItems.valueOf(fsItem) : null;
    }

    public void setType(String type) {
        this.type = type != null ? JournalAccountType.valueOf(type) : null;
    }

    public AccountModel() {
        super(EntityObject.ACCOUNT);
    }

    public AccountModel(String uuid) {
        super(EntityObject.ACCOUNT, uuid);
    }

    public AccountModel copy(){
        return (AccountModel) super.copy(this);
    }
}
