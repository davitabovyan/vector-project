package am.vector.payroll.api.entity.admin;

import am.vector.common.constants.Holiday;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "workday")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Accessors(chain = true) @Getter @Setter
@NoArgsConstructor
public class Workday implements Persistable<LocalDate> {
    @Id
    private LocalDate day;

    @Enumerated(EnumType.STRING)
    private DayOfWeek weekDay;

//    @Enumerated(EnumType.STRING)
//    private DayShift dayShift;

    @Enumerated(EnumType.STRING)
    private Holiday holiday;

    private transient boolean isNew;

    public Workday(LocalDate day) {
        this.day = day;
        this.isNew = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Workday workday = (Workday) o;
        return Objects.equals(day, workday.day);
    }

    @Override
    public int hashCode() {
        return Objects.hash(day);
    }

    @Override
    public LocalDate getId() {
        return day;
    }

    @Override
    public boolean isNew() {
        return this.isNew;
    }
}
