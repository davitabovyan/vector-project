package am.vector.common.exception.custom;

public class UnsupportedJsonValueException extends ProvidedJsonException {
    public UnsupportedJsonValueException(String key, Class clazz) {
        super(key + "," + clazz.toString());
    }
}

