package am.vector.common.util;

import am.vector.common.config.Context;
import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import am.vector.common.exception.custom.ActionNotAllowedForUserException;
import am.vector.common.exception.custom.IncorrectIdException;
import am.vector.common.model.UUIDModel;
import am.vector.common.model.admin.UserModel;
import am.vector.common.persistence.UUIDEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.HashMap;
import java.util.Map;

public class GeneralUtil {

    private static final long key = 1587965412487514585L;
    private static final int INT_KEY = 4254381;
    private static final Logger logger = LogManager.getLogger();

    public static void setRecordStatus(Object object){
        if(object instanceof UUIDModel) {
            UUIDModel entity = (UUIDModel) object;
            if(entity.getState() == null) {
                entity.setState(RecordState.CURRENT);
            }
        }
    }

    public static String encodeUuid(int id){
        id = id ^ INT_KEY;
        return Integer.toHexString(id);
    }

    public static int decodeUuidToInt(String uuid){
        if (uuid == null) return 0;
        int g = Integer.decode("0x"+uuid);
        return g ^ INT_KEY;
    }

    public static String encodeUuid(long id){
        id = id ^ key;
        return Long.toHexString(id);
    }

    public static long decodeUuid(String uuid){
        if (uuid == null) return 0;
        long g = Long.decode("0x"+uuid); //Long.parseLong(uuid,16);
        return g ^ key;
    }

    public static long[] decodeUuid(String[] uuids){
        long[] arr = new long[uuids.length];
        for(int i=0; i<uuids.length; ++i){
            arr[i] = decodeUuid(uuids[i]);
        }
        return arr;
    }

    public static Map<String, Object> uuidJSON(UUIDEntity object){
        Map<String, Object> map = new HashMap<>();
        map.put("uuid", GeneralUtil.encodeUuid(object.getId()));
        return map;
    }

    public static UserModel getUserFromContext(){
        return (UserModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static boolean isValidUuid(String uuid, EntityObject entityObject){
        try {
            long id = Long.parseLong(uuid,16);
            if((id ^ key) % 100 != entityObject.getIndex()) return false;
        } catch (NumberFormatException ex){
            return false;
        }
        return true;
    }

    public static EntityObject getObjectTypeFromId(long id) {
        return EntityObject.getByIndex((int) id % 100);
    }

    public static boolean isAnonymous(){
        return "anonymousUser".equals(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }

    public static int getCompanyId(){
        return getUserFromContext().getCompanyId();
    }

    public static Context getContextAndAuthenticate(String uuid, HttpMethod method){
        UserModel user = getUserFromContext();
        if(uuid.length() != 16){
            throw new IncorrectIdException(uuid);
        }
        long id = decodeUuid(uuid);
        EntityObject object = EntityObject.getByIndex((int)(id % 100));
        if(!user.isSuperAdmin()) {
//            checkAuthority(user, id);
            checkObjectAccessAndGetUser(method, object);
        }
        return new Context(user, id);
    }

    public static UserModel checkObjectAccessAndGetUser(HttpMethod method, EntityObject object){
        UserModel user = getUserFromContext();
        if(!user.isSuperAdmin()) {
            long authority = method == HttpMethod.GET ? user.getReadAccess() : user.getEditAccess();
            if (notHasAccess(object.getAccess(), authority)) throw new ActionNotAllowedForUserException(object, method);
        }
        return user;
    }

    public static void checkObjectAccess(String httpMethod, String objectCode){
        EntityObject object = EntityObject.getByCode(objectCode);
        UserModel user = getUserFromContext();
        if(!user.isSuperAdmin()) {
            long authority = "GET".equals(httpMethod) ? user.getReadAccess() : user.getEditAccess();
            if (notHasAccess(object.getAccess(), authority)) throw new ActionNotAllowedForUserException(object, httpMethod);
        }
    }

//    private static void checkAuthority(User user, long uuid){
//        int companyId = (int) (uuid % 100_000L);
//        if(user.getCompanyId() != companyId){
//            LOG.info(String.format("Unauthorised attempt to view/change record: user %s, id %d", user.getUsername(), uuid));
//            throw new NotAllowedForUserException("You are not allowed to view/change this record");
//        }
//    }

    private static boolean notHasAccess(long value, long access){
        return (value & access) == 0b0L;
    }
}
