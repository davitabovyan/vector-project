package am.vector.payroll.api.service.impl.jpa.internal;

import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.Converter;
import am.vector.payroll.aop.LogExecutionTime;
import am.vector.payroll.api.entity.admin.Company;
import am.vector.payroll.api.entity.admin.Workday;
import am.vector.payroll.api.entity.internal.TaxReport;
import am.vector.payroll.api.entity.main.*;
import am.vector.payroll.api.repository.internal.TaxReportRepository;
import am.vector.payroll.api.service.*;
import am.vector.payroll.api.support.PayrollPeriod;
import am.vector.payroll.util.DayUtil;
import am.vector.payroll.util.FileParser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.*;

@Service
@RequiredArgsConstructor
public class UploadServiceJPA implements UploadService {

    private final TaxReportRepository taxReportRepository;
    private final WorkdayService workdayService;
    private final EmploymentService employmentService;
    private final PersonService personService;
    private final DepartmentService departmentService;
    private final RoleService roleService;
    private final ContractService contractService;

    @Override
    public void uploadIncomeTaxReport(MultipartFile[] reports, Company company) {
        final Map<String, List<TaxReport>> allReports = getAllReportsParsedData(reports, company);
        ReportAnalyze reportAnalyze = generatePeriodForAnalyze(allReports);
        if(reportAnalyze.fullPeriods > 3){
            fillHiringAndTermination(reportAnalyze, allReports, company);
        }
        Map<String, Person> personsBySsn = createEmployments(allReports, reportAnalyze, company.getId());
        createRoleAndContracts(reportAnalyze, personsBySsn, company);
    }

    private Map<String, List<TaxReport>> getAllReportsParsedData(MultipartFile[] reports, Company company){
        Map<String, List<TaxReport>> allReports = new HashMap<>();
        List<TaxReport> taxReports = new ArrayList<>();
        List<Integer> periods = new ArrayList<>(reports.length);
        for(MultipartFile report : reports){
            Map<String, TaxReport> monthly = FileParser.parseReportToPayslips(report, company);
            for(Map.Entry<String, TaxReport> perPerson : monthly.entrySet()) {
                if(!allReports.containsKey(perPerson.getKey())){
                    allReports.put(perPerson.getKey(), new ArrayList<>());
                }
                allReports.get(perPerson.getKey()).add(perPerson.getValue());
                taxReports.add(perPerson.getValue());
            }
            periods.add(monthly.entrySet().iterator().next().getValue().getPeriod());
        }
        Collections.sort(periods);
        taxReportRepository.deleteAllForPeriodsInterval(company.getId(), periods.get(0), periods.get(periods.size()-1));
        taxReportRepository.saveAll(taxReports);
        for(Map.Entry<String, List<TaxReport>> entry : allReports.entrySet()){
            Collections.sort(entry.getValue());
        }
        return allReports;
    }



    private ReportAnalyze generatePeriodForAnalyze(Map<String, List<TaxReport>> allReports){
        ReportAnalyze reportAnalyze = new ReportAnalyze();
        for(Map.Entry<String, List<TaxReport>> entry : allReports.entrySet()){
            Collections.sort(entry.getValue());
            int size = entry.getValue().size();
            if(size > reportAnalyze.fullPeriods){
                reportAnalyze.fullPeriods = size;
                reportAnalyze.startPeriod = entry.getValue().get(0).getPeriod();
                reportAnalyze.endPeriod = entry.getValue().get(size - 1).getPeriod();
            }
        }
        return reportAnalyze;
    }

    private void fillHiringAndTermination(ReportAnalyze reportAnalyze,
        Map<String, List<TaxReport>> allReports, Company company) {

        Map<Integer, Map<String, Object>> parsedByMonths = new HashMap<>();
        boolean isSaturdayNonWorking = company.getWorkWeek() == 5;
        int tempPeriod = reportAnalyze.startPeriod;
        Map<String, Object> parsedMonth;
        while (tempPeriod >= reportAnalyze.endPeriod) {
            Iterable<Workday> workDays = workdayService.getWorkdayForMonth(DayUtil.parsePeriod(tempPeriod));
            parsedMonth = DayUtil.parseWorkDays(workDays, isSaturdayNonWorking);
            parsedByMonths.put(tempPeriod, parsedMonth);
            tempPeriod = DayUtil.removeMonthPeriod(tempPeriod);
        }

        for(Map.Entry<String, List<TaxReport>> entry : allReports.entrySet()){
            guessHireOrTerminationDates(reportAnalyze, isSaturdayNonWorking, entry);

            if(entry.getValue().get(0).getSalary() != 0) { // employment contract
                int maxRepeatedSalary = getMaxRepeatedSalary(entry.getValue());
                int workHoursPerDay = guessWorkHoursPerDay(entry.getValue(), parsedByMonths, maxRepeatedSalary);
                int latestSalary = guessLatestSalary(entry.getValue(), parsedByMonths, workHoursPerDay);
                reportAnalyze.hoursPerDay.put(entry.getKey(), workHoursPerDay);
                reportAnalyze.fullSalary.put(entry.getKey(), latestSalary);
            }
        }
    }

    private int guessWorkHoursPerDay(List<TaxReport> payslips,
        Map<Integer, Map<String, Object>> parsedByMonths, int maxRepeatedSalary){
        for (TaxReport payslip : payslips) {
            if (payslip.getSalary() != maxRepeatedSalary) continue;
            int workingDays = (Integer) parsedByMonths.get(payslip.getPeriod()).get("numOfWorkingDays");
            if ((payslip.getWorkedHours() / 10) % workingDays != 0) continue;
            return payslip.getWorkedHours() / (workingDays * 10);
        }
        return 0;
    }

    private int guessLatestSalary(List<TaxReport> payslips,
        Map<Integer, Map<String, Object>> parsedByMonths, int hoursPerDay){
        for(TaxReport payslip : payslips){
            int workingDays = (Integer) parsedByMonths.get(payslip.getPeriod()).get("numOfWorkingDays");
            if(hoursPerDay == 0 || payslip.getWorkedHours() / hoursPerDay != workingDays * 10) continue;
            return payslip.getSalary();
        }
        return 0;
    }

    private void guessHireOrTerminationDates(ReportAnalyze reportAnalyze,
        boolean isSaturdayNonWorking, Map.Entry<String, List<TaxReport>> entry){
        TaxReport firstPayslip = entry.getValue().get(0);
        TaxReport lastPayslip = entry.getValue().get(entry.getValue().size() - 1);
        boolean isNotNewHire = firstPayslip.getPeriod() == reportAnalyze.startPeriod;
        boolean isNotTerminated = reportAnalyze.endPeriod == lastPayslip.getPeriod();
        boolean isEmploymentContract = lastPayslip.getSalary() != 0;

        reportAnalyze.contractType.put(entry.getKey(), isEmploymentContract);
        LocalDate date;
        if(isEmploymentContract) {
            if (!isNotNewHire) {
                PayrollPeriod firstPeriod = DayUtil.parsePeriod(firstPayslip.getPeriod());
                date = LocalDate.of(firstPeriod.getYear(), firstPeriod.getMonth(), 1);
                reportAnalyze.newHire.put(entry.getKey(), date);
            }

            if (!isNotTerminated) {
                LocalDate terminationDate = null;
                if(reportAnalyze.hoursPerDay.containsKey(entry.getKey()) && reportAnalyze.hoursPerDay.get(entry.getKey()) != 0) {
                    PayrollPeriod lastPeriod = DayUtil.parsePeriod(lastPayslip.getPeriod());
                    int actuallyWorked = lastPayslip.getWorkedHours() / reportAnalyze.hoursPerDay.get(entry.getKey());
                    terminationDate = workdayService.getTerminationDay(lastPeriod, actuallyWorked, isSaturdayNonWorking);
                }
                reportAnalyze.terminated.put(entry.getKey(), terminationDate);
            }
        } else { // service contractor
            if (!isNotNewHire) {
                PayrollPeriod firstPeriod = DayUtil.parsePeriod(firstPayslip.getPeriod());
                reportAnalyze.newHire.put(entry.getKey(), firstPeriod.getFirstDay());
            }
            if (!isNotTerminated) {
                date = DayUtil.parsePeriod(lastPayslip.getPeriod()).getLastDay();
                reportAnalyze.terminated.put(entry.getKey(), date);
            }
        }
    }

    private int getMaxRepeatedSalary(List<TaxReport> employeePayslips){
        int salary = 0;
        Map<Integer, Integer> repetitionCounts = new HashMap<>();
        for(TaxReport payslip : employeePayslips){
            if(repetitionCounts.containsKey(payslip.getSalary())){
                repetitionCounts.put(payslip.getSalary(), repetitionCounts.get(payslip.getSalary()) + 1);
            } else {
                repetitionCounts.put(payslip.getSalary(), 1);
            }
        }
        int maxOccurrence = 0;
        for(Map.Entry<Integer, Integer> entry : repetitionCounts.entrySet()){
            if(entry.getValue() > maxOccurrence){
                maxOccurrence = entry.getValue();
                salary = entry.getKey();
            }
        }
        return salary;
    }

    @LogExecutionTime
    private Map<String, Person> createEmployments(Map<String, List<TaxReport>> allReports,
        ReportAnalyze reportAnalyze, int companyId){
        List<Person> personsToPersist = new LinkedList<>();
        Map<String, Person> personsBySsn = new HashMap<>();
        int personSerialNumber = 0;
        List<Employment> employmentsToPersist = new LinkedList<>();
        for(Map.Entry<String, List<TaxReport>> entry : allReports.entrySet()){
            String ssn = entry.getKey();
            Person person = new Person();
            person.setCode(String.format("%04d", ++personSerialNumber));
            person.setSsn(ssn);
            String[] fullName = entry.getValue().get(0).getName().split(" ");
            person.setLastName(fullName[0]);
            person.setFirstName(fullName[1]);
            person.setGender(Converter.getGenderFromSsn(ssn));
            person.setBirthday(Converter.getBirthdayFromSsn(ssn));
            person.setCompanyId(companyId);
            personsToPersist.add(person);
            personsBySsn.put(ssn, person);

            Employment employment = new Employment();
            employment.setPerson(person);
            employment.setCompanyId(companyId);
            if(reportAnalyze.newHire.containsKey(ssn)){
                employment.setHireDate(reportAnalyze.newHire.get(ssn));
            }
            if(reportAnalyze.terminated.containsKey(ssn)){
                employment.setTerminationDate(reportAnalyze.terminated.get(ssn));
            }
            employmentsToPersist.add(employment);
        }
        personService.deleteAll(companyId);
        personService.saveAll(personsToPersist);
        employmentService.deleteAll(companyId);
        employmentService.saveAll(employmentsToPersist);
        return personsBySsn;
    }


    @LogExecutionTime
    private void createRoleAndContracts(ReportAnalyze reportAnalyze, Map<String, Person> personsBySsn, Company company) {
        Department department;
        try {
            department = departmentService.getByCode("001");
        } catch (EntityNotFoundException ex) {
            department = new Department()
                .setCode("001")
                .setName("Բաժին 1");
            department.setCompanyId(company.getId());
            departmentService.save(department);
        }

        Role employeeRole = new Role()
            .setCode("0000")
            .setName("Աշխատանքային");
        employeeRole.setCompanyId(company.getId());
        roleService.deleteByCode("0000");
        roleService.save(employeeRole);

        Role contractorRole = new Role()
            .setCode("9999")
            .setName("Քաղիրավական");
        contractorRole.setCompanyId(company.getId());
        roleService.deleteByCode("9999");
        roleService.save(contractorRole);

        List<Contract> contracts = new LinkedList<>();
        for(Map.Entry<String, Boolean> contractType : reportAnalyze.contractType.entrySet()){
            String ssn = contractType.getKey();
            Contract contract = new Contract()
                .setDepartment(department)
                .setWorkWeek(company.getWorkWeek())
                .setPerson(personsBySsn.get(ssn));
            contract.setCompanyId(company.getId());
            contract.setStartDate(reportAnalyze.newHire.getOrDefault(ssn, LocalDate.EPOCH));
            contract.setEndDate(reportAnalyze.terminated.getOrDefault(ssn, null));
            contract.setLaborRelation(contractType.getValue());
            if(contractType.getValue()){
                contract.setRole(employeeRole);
                contract.setDailyHours(reportAnalyze.hoursPerDay.getOrDefault(ssn, 0));
                contract.setSalary(reportAnalyze.fullSalary.getOrDefault(ssn, 0));
            } else {
                contract.setRole(contractorRole);
                contract.setSalary(0);
            }
            contracts.add(contract);
        }
        contractService.deleteAll(company.getId());
        contractService.saveAll(contracts);
    }

    @Override
    public void uploadEmployeesList(MultipartFile file) {
        FileParser.parseEmployeeList(file);
    }

    public static class TaxTotals {
        public int salary;
        public int service;
        public int otherIncome;
        public int deductibleIncome;
        public int socialPaymentEmployer;
        public int socialPaymentBudget;
        public int returnOfOverpaidSocial;
        public int incomeTax;
        public int ssp;
    }


    static class ReportAnalyze {
        int fullPeriods;
        int startPeriod;
        int endPeriod;
        final Map<String, LocalDate> newHire;
        final Map<String, LocalDate> terminated;
        final Map<String, Integer> hoursPerDay;
        final Map<String, Integer> fullSalary;
        final Map<String, Boolean> contractType;

        ReportAnalyze(){
            newHire = new HashMap<>();
            terminated = new HashMap<>();
            hoursPerDay = new HashMap<>();
            fullSalary = new HashMap<>();
            contractType = new HashMap<>();
        }
    }
}
