package am.vector.common.util;

import java.time.DateTimeException;
import java.time.LocalDate;

import am.vector.common.constants.Gender;

public class Converter {
    public static Gender getGenderFromSsn(String ssn){
        int first = Character.getNumericValue(ssn.charAt(0));
        return first < 5 ? Gender.MALE : Gender.FEMALE;
    }

    public static LocalDate getBirthdayFromSsn(String ssn){
        int[] arr = new int[10];
        int index = 0;
        for(char figure : ssn.toCharArray()){
            arr[index++] = Character.getNumericValue(figure);
        }
        int day = (arr[0] < 5 ? arr[0] - 1 : arr[0] - 5) * 10 + arr[1];
        int month = (arr[2] * 10 + arr[3] == 14) ? 6 : arr[2] * 10 + arr[3];
        int year = (arr[2] > 1 ? 2000 : 1900) + arr[4] * 10 + arr[5];
        try {
            return LocalDate.of(year, month, day);
        } catch (DateTimeException ex){
            return null;
        }
    }

}
