package am.vector.payroll.api.service.impl.jpa.main;

import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.Converter;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.repository.main.PersonRepository;
import am.vector.payroll.api.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class PersonServiceJPA implements PersonService {

    private final PersonRepository personRepository;

    @Override
    public Person findById(long id){
        return personRepository.findById(id).orElseThrow(
            ()-> new EntityNotFoundException(Person.class, GeneralUtil.encodeUuid(id))
        );
    }

    public List<Person> findAllPerCompany(){
        int companyId = GeneralUtil.getCompanyId();
        return personRepository.findAllPerCompany(companyId);
    }

    @Transactional
    @Override
    public void update(Person person){
        personRepository.save(person);
    }

    @Transactional
    @Override
    public Person save(Person object){
        if(null == object.getGender()){
            object.setGender(Converter.getGenderFromSsn(object.getSsn()));
        }
        return personRepository.save(object);
    }

    @Override
    public List<Person> saveAll(List<Person> persons) {
        return personRepository.saveAll(persons);
    }

    @Override
    public List<Person> findAllById(List<Long> id) {
        return personRepository.findAllById(id);
    }

    @Override
    public void deleteById(long id) {
        personRepository.deleteById(id);
    }

    @Override
    public void deleteAll(int companyId) {
        personRepository.deleteAll(companyId);
        logger.info(String.format("All record are delete by user %s", GeneralUtil.getUserFromContext().getUsername()));
    }
}
