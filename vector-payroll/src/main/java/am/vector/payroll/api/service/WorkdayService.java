package am.vector.payroll.api.service;

import am.vector.payroll.api.entity.admin.Workday;
import am.vector.payroll.api.support.PayrollPeriod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.List;

public interface WorkdayService{
    Logger logger = LogManager.getLogger();

    List<Workday> workDaysForPeriod(LocalDate start, LocalDate end);
    List<Workday> workDaysForPeriod(PayrollPeriod period);
    Workday getWorkday(LocalDate date);
    List<Workday> getWorkdayForMonth(PayrollPeriod period);
    LocalDate getTerminationDay(PayrollPeriod period, int numOfWorkedDays, boolean isSaturdayNonWorking);
    Workday updateWorkday(Workday workday);
    void deleteById(LocalDate day);
}
