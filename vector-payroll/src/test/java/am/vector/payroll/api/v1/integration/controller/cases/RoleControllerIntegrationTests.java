package am.vector.payroll.api.v1.integration.controller.cases;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import am.vector.payroll.PayrollApplication;
import am.vector.payroll.api.entity.main.Role;
import am.vector.payroll.api.v1.integration.controller.ControllerBaseTest;
import am.vector.payroll.util.EntityBuilder;
import am.vector.payroll.util.PayrollApi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PayrollApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoleControllerIntegrationTests extends ControllerBaseTest {

    private Role roleOne = EntityBuilder.getRoleOne();

    @Before
    public void setup() {
    }

    @Test
    public void createRole_success() {
        testCreate(roleOne, adapter.toJson(roleOne));
    }

    @Test
    public void getRole_success() {
        // Setup
        Role role = create(roleOne);
        // Act
        Role result = getRole(role.getId());
        // Assert
        assertEquals("Role's code is not returned correctly", role.getCode(), result.getCode());
        assertEquals("Role's name is not returned correctly", role.getName(), result.getName());
        assertEquals("Role's description is not returned correctly", role.getDescription(), result.getDescription());
    }

    @Test
    public void getRoleWithCustomField_success() {
        // Setup
        Map<String, String> fields = new HashMap<>();
        fields.put("eligibleDays",null);
        fields.put("*", null);
        Role role = create(roleOne, fields);
        // Act
        Role result = getRole(role.getId(), fields);
        // Assert
        assertEquals("Role's eligibleDays is not returned correctly", role.getEligibleDays(), result.getEligibleDays());
    }

    @Test
    public void updateRole_success() {
        // Setup
        Map<String, String> fields = new HashMap<>();
        fields.put("eligibleDays",null);
        fields.put("*", null);

        long id = create(roleOne, fields).getId();
        Role expected = roleOne.copy()
                .setCode("9002")
                .setName("_Accountant")
                .setDescription("_The accountant")
                .setEligibleDays(23);
        expected.setId(id);
        expected.setState(RecordState.CURRENT);

        // Act
        int responseStatus = update(expected, fields).getStatusCodeValue();

        // Assert
        assertEquals("Response status on Role update isn't correct", 200, responseStatus);
        Role actual = getRole(id, fields);
        compareExpectedWithActual(expected, actual);
    }

    @Test
    public void updateRole_shouldThrowNotAllowedForUserException() {
        // Setup
        Role role = create(roleOne);
        // Act
        ResponseEntity<String> response =
                PayrollApi.update(getViewerHeaders(), getHostAndPort(), adapter.toJson(role), role.getId(), role.getObjCode());
        // Assert
        testUserAuthority(response, EntityObject.ROLE, HttpMethod.PUT);
    }

    @Test
    public void editRoleSingleField_success() {
        // Setup
        Role role = create(roleOne);
        // Act
        HttpStatus responseStatus = PayrollApi.updateFields(
                getHeaders(), getHostAndPort(), "{\"code\":\"1111\"}", role.getId(), role.getObjCode()).getStatusCode();
        // Assert
        assertEquals("Response status on Person update isn't correct", HttpStatus.OK, responseStatus);
        Role actual = getRole(role.getId());
        compareExpectedWithActual(role.setCode("1111"), actual);
    }

    private void compareExpectedWithActual(Role expected, Role actual) {
        assertEquals("Role's code is not correct", expected.getCode(), actual.getCode());
        assertEquals("Role's name is not correct", expected.getName(), actual.getName());
        assertEquals("Role's description is not correct", expected.getDescription(), actual.getDescription());
    }
}
