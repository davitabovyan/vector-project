package am.vector.payroll.api.controller.main;

import am.vector.common.model.payroll.ContractModel;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.Contract;
import am.vector.payroll.api.service.ContractService;
import am.vector.payroll.api.support.PayrollPeriod;
import am.vector.payroll.util.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Month;
import java.util.List;
import java.util.Map;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/contract", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class ContractController {

    private final ContractService contractService;
    private final GsonPayrollAdapter adapter;

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findByIdCompact(@PathVariable String uuid,
                                          @RequestParam(value = "fields", required = false) String fields){
        Contract response = contractService.findById(GeneralUtil.decodeUuid(uuid));
        return ResponseEntity.ok(adapter.toJson(response));
    }

    @GetMapping
    public ResponseEntity<String> findAllCompact(@RequestParam(value = "fields", required = false) String fields){
        Iterable<Contract> response = contractService.findAllPerCompany();
        return ResponseEntity.ok(adapter.toJson(response));
    }

    @GetMapping("/list/{year}/{month}")
    public ResponseEntity<String> getFullPayrollList(@PathVariable int year, @PathVariable int month){
        Iterable<Contract> response = contractService.getFullPayrollList(
                GeneralUtil.getCompanyId(),
                new PayrollPeriod(year, Month.of(month))
        );
        return ResponseEntity.ok(adapter.toJson(response));
    }

    @PostMapping
    public ResponseEntity<Map<String,Object>> create(@RequestBody ContractModel object) {
        object.setCompanyId(GeneralUtil.getCompanyId());
        Contract entity = contractService.save(Mapper.map(object));
        return new ResponseEntity<>(GeneralUtil.uuidJSON(entity), HttpStatus.CREATED);
    }

    @PutMapping("/{uuid}")
    public void update(@RequestBody ContractModel object, @PathVariable String uuid) {
        object.setId(GeneralUtil.decodeUuid(uuid));
        object.setCompanyId(GeneralUtil.getCompanyId());
        contractService.update(Mapper.map(object));
    }

    @PatchMapping("/setRoleByPersons/{roleUuid}")
    public ResponseEntity<String> setRoleByPersons(@RequestBody String[] personUuids, @PathVariable String roleUuid) {
        List<Contract> contracts = contractService.setRoleByPersons(GeneralUtil.decodeUuid(roleUuid), GeneralUtil.decodeUuid(personUuids));
        return ResponseEntity.ok(adapter.toJson(contracts, "person:code.firstName.lastName"));
    }

    @PatchMapping("/setDepartmentByPersons/{departmentUuid}")
    public ResponseEntity<String> setDepartmentByPersons(@RequestBody String[] personUuids,
                                                        @PathVariable String departmentUuid) {
        List<Contract> contracts = contractService.
            setDepartmentByPersons(GeneralUtil.decodeUuid(departmentUuid), GeneralUtil.decodeUuid(personUuids));
        return ResponseEntity.ok(adapter.toJson(contracts, "person:code.firstName.lastName"));
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody ContractModel updated, @PathVariable String uuid) {
        Contract entity = contractService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(updated));
        contractService.update(entity);
    }

    @DeleteMapping("/{uuid}")
    public void remove(@PathVariable String uuid){
        contractService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
