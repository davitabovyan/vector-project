package am.vector.common.model.payroll;

import am.vector.common.constants.EntityObject;
import am.vector.common.model.MainModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true) @Getter @Setter
public class RoleModel extends MainModel {

    private String code;
    private String name;
    private String description;
    private Integer eligibleDays;

    public RoleModel() {
        super(EntityObject.ROLE);
    }

    public RoleModel(String uuid) {
        super(EntityObject.ROLE, uuid);
    }

    public RoleModel copy(){
        return (RoleModel) super.copy(this);
    }
}
