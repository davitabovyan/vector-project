package am.vector.common.exception.custom;

import am.vector.common.exception.VectorException;

public class UnsupportedValueException extends VectorException {
    public UnsupportedValueException(String... keys) {
        super(keys);
    }
}

