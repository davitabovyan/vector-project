package am.vector.security.adapter;

import java.io.IOException;

import am.vector.common.model.admin.UserModel;
import am.vector.common.util.GeneralUtil;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserTypeAdapter extends TypeAdapter<UserModel> {

	@Override
	public void write(JsonWriter jsonWriter, UserModel user) throws IOException {
		jsonWriter.beginObject();
		jsonWriter.name("username").value(user.getUsername());
		jsonWriter.name("clientUuid").value(user.getClientUuid());
		jsonWriter.name("readAccess").value(Long.toBinaryString(user.getReadAccess()));
		jsonWriter.name("editAccess").value(Long.toBinaryString(user.getEditAccess()));
		jsonWriter.name("isAdmin").value(user.isAdmin());
		jsonWriter.name("enabled").value(user.isEnabled());
		jsonWriter.name("emailVerified").value(user.isEmailVerified());
		jsonWriter.endObject();
	}

	@Override
	public UserModel read(JsonReader jsonReader) throws IOException {
		return null;
	}
}
