package am.vector.journal.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.journal.api.entity.Account;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class AccountAdapter extends CustomAdapter<Account> {

    public AccountAdapter(String fields){
        super(Account.class, fields);
    }

    public AccountAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, Account account, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("code")) jsonWriter.name("code").value(account.getCode());
        if(all || fields.containsKey("name")) jsonWriter.name("name").value(account.getName());
        if(all || fields.containsKey("displayName")) jsonWriter.name("displayName").value(account.getDisplayName());
        if(all || fields.containsKey("isParent")) jsonWriter.name("isParent").value(account.getIsParent());
        if(all || fields.containsKey("type")) jsonWriter.name("type").value(account.getType().name());

        if(all || fields.containsKey("parent")){
            jsonWriter.name("parent");
            new AccountAdapter(fields.getOrDefault("parent", null))
                .write(jsonWriter, account.getParent());
        }

        if(fields.containsKey("fsItem")) jsonWriter.name("fsItem").value(account.getType().name());

        return jsonWriter;
    }
}
