package am.vector.payroll.api.service;

import am.vector.common.persistence.CommonService;
import am.vector.payroll.api.dto.timesheet.MainTimesheetDto;
import am.vector.payroll.api.entity.main.TimeSheet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface TimesheetService extends CommonService<TimeSheet> {
    Logger logger = LogManager.getLogger();

    TimeSheet getTimesheetWithPerson(long id);
    List<MainTimesheetDto> getTimesheetsByPeriod(String period, long departmentId);
    List<TimeSheet> generate(int companyId, String period, long departmentId);
    TimeSheet generateForContract(int companyId, long personId, String period);
}
