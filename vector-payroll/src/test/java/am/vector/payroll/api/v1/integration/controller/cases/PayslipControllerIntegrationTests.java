package am.vector.payroll.api.v1.integration.controller.cases;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import am.vector.payroll.PayrollApplication;
import am.vector.payroll.api.entity.main.Payslip;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.v1.integration.controller.ControllerBaseTest;
import am.vector.payroll.util.EntityBuilder;
import am.vector.payroll.util.PayrollApi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PayrollApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PayslipControllerIntegrationTests extends ControllerBaseTest {

    private Payslip payslipOne;

    @Before
    public void setUp(){
        Person person = create(EntityBuilder.getPersonOne());
        payslipOne = EntityBuilder.getPayslipOne().setPerson(person);
    }

    @Test
    public void createPayslip_success() {
        // Setup
        Payslip payslip = payslipOne.copy();
        // Assert
        testCreate(payslip, adapter.toJson(payslip));
    }

    @Test
    public void createPayslipDetails_success() {
        // Setup
        Payslip payslip = payslipOne.copy();
        // Act
        Payslip response = create(payslip);
        // Assert
        compareExpectedWithActual(payslip, response);
    }
    @Test
    public void getPayslip_success() {
        // Setup
        Payslip payslip = payslipOne.copy();
        long id = create(payslip).getId();
        // Act
        Payslip result = getPayslip(id);
        // Assert
        compareExpectedWithActual(payslip, result);
    }

    @Test
    public void updatePayslip_success() {
        // Setup
        Payslip payslip = payslipOne.copy();
        long id = create(payslip).getId();
        Person person = create(EntityBuilder.getPersonTwo());
        payslip
            .setPerson(person)
            .setPeriod("1118")
            .setWage(120_000L)
            .setBonus(30_000L)
            .setIt(40_000L)
            .setSsp(8_000L)
            .setArmy(2_000L)
            .setOvertime(4L)
            .setHasUpdate(true);
        payslip.setId(id);
        payslip.setState(RecordState.CURRENT);

        // Act
        ResponseEntity<String> response = update(payslip);

        // Assert
        assertEquals("Response status on Payslip update isn't correct", 200, response.getStatusCodeValue());
        Payslip actual = getPayslip(id);
        compareExpectedWithActual(payslip, actual);
    }

    @Test
    public void updatePayslip_shouldThrowNotAllowedForUserException() {
        // Setup
        Payslip payslip = create(payslipOne.copy());
        // Act
        ResponseEntity<String> response =
            PayrollApi.update(getViewerHeaders(), getHostAndPort(), adapter.toJson(payslip), payslip.getId(), payslip.getObjCode());
        // Assert
        testUserAuthority(response, EntityObject.PAYSLIP, HttpMethod.PUT);
    }

    @Test
    public void editPayslipSingleField_success() {
        // Setup
        Payslip payslip = create(payslipOne.copy());
        // Act
        HttpStatus responseStatus = PayrollApi.updateFields(
            getHeaders(), getHostAndPort(), "{\"period\":\"1118\"}", payslip.getId(), payslip.getObjCode()).getStatusCode();
        // Assert
        assertEquals("Response status on Payslip update isn't correct", HttpStatus.OK, responseStatus);
        Payslip actual = getPayslip(payslip.getId());
        compareExpectedWithActual(payslip.setPeriod("1118"), actual);
    }

    private void compareExpectedWithActual(Payslip expected, Payslip actual){
        assertEquals("Payslip period isn't saved/retrieved correctly", expected.getPeriod(), actual.getPeriod());
        assertEquals("Payslip wage isn't saved/retrieved correctly", expected.getWage(), actual.getWage());
        assertEquals("Payslip overtime center isn't saved/retrieved correctly", expected.getOvertime(), actual.getOvertime());
        assertEquals("Payslip bonus isn't saved/retrieved correctly", expected.getBonus(), actual.getBonus());
        assertEquals("Payslip ssp isn't saved/retrieved correctly", expected.getSsp(), actual.getSsp());
        assertEquals("Payslip it isn't saved/retrieved correctly", expected.getIt(), actual.getIt());
        assertEquals("Payslip army isn't saved/retrieved correctly", expected.getArmy(), actual.getArmy());
        assertEquals("Payslip hasUpdate isn't saved/retrieved correctly", expected.hasUpdate(), actual.hasUpdate());
        assertEquals("Payslip person isn't saved/retrieved correctly", expected.getPerson().getId(), actual.getPerson().getId());
    }
}
