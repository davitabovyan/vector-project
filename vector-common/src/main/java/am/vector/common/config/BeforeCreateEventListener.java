package am.vector.common.config;

import am.vector.common.util.GeneralUtil;
import org.springframework.data.rest.core.event.AbstractRepositoryEventListener;

// Not used
public class BeforeCreateEventListener extends AbstractRepositoryEventListener {

    @Override
    public void onBeforeSave(Object object) {
        GeneralUtil.setRecordStatus(object);
    }
}