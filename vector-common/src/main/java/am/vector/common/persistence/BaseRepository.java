package am.vector.common.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {
    Optional<T> findById(ID id);
    List<T> findAllById(List<ID> var1);
    <S extends T> S save(S entity);
    <S extends T> List<S> saveAll(Iterable<S> var1);
    void delete(T var1);
}
