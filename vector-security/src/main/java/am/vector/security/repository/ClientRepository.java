package am.vector.security.repository;

import am.vector.security.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

@RepositoryRestResource(collectionResourceRel = "client", path = "client", exported = false)
public interface ClientRepository extends JpaRepository<Client, Integer> {

    @Query("SELECT c FROM Client c where c.email = :email")
    Client findByEmail(@Param("email") String email);

    @Transactional
    @Modifying
    @Query("DELETE FROM Client c where c.id = :id")
    void deleteById(@Param("id") int id);
}
