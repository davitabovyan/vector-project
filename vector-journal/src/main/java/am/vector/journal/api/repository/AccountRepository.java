package am.vector.journal.api.repository;

import am.vector.common.persistence.ClientCodeIdRepository;
import am.vector.journal.api.entity.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends ClientCodeIdRepository<Account> {

    @Query("SELECT a FROM Account a WHERE a.id > :idStart AND a.id < :idEnd")
    List<Account> findAllPerCompany(@Param("idStart") long start, @Param("idEnd") long end);

    @Query("SELECT a FROM Account a WHERE a.id > :idStart AND a.id < :idEnd AND a.parent.id = :parentId")
    List<Account> findChildren(@Param("parentId") long parentId, @Param("idStart") long start, @Param("idEnd") long end);

    @Query("SELECT a FROM Account a WHERE a.id > :idStart AND a.id < :idEnd AND a.isParent = 0 ")
    List<Account> findAllChildNodes(@Param("idStart") long start, @Param("idEnd") long end);

    @Query("DELETE FROM Account a WHERE a.id > :idStart AND a.id < :idEnd")
    void deleteAll(@Param("idStart") long start, @Param("idEnd") long end);
}
