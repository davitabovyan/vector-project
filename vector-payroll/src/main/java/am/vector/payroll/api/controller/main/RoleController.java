package am.vector.payroll.api.controller.main;

import am.vector.common.model.payroll.RoleModel;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.Role;
import am.vector.payroll.api.service.RoleService;
import am.vector.payroll.util.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/role", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class RoleController {

    private final RoleService roleService;
    private final GsonPayrollAdapter adapter;

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findByIdCompact(@PathVariable String uuid,
                                                  @RequestParam(value = "fields", required = false) String fields){
        Role response = roleService.findById(GeneralUtil.decodeUuid(uuid));
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @GetMapping
    public ResponseEntity<String> findAllCompact(@RequestParam(value = "fields", required = false) String fields){
        List<Role> response = roleService.findAllPerCompany();
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @PostMapping
    public ResponseEntity<Map<String,Object>> create(@RequestBody RoleModel model) {
        model.setCompanyId(GeneralUtil.getCompanyId());
        Role role = roleService.save(Mapper.map(model));
        return new ResponseEntity<>(GeneralUtil.uuidJSON(role), HttpStatus.CREATED);
    }

    @PutMapping("/{uuid}")
    public void update(@RequestBody RoleModel model, @PathVariable String uuid) {
        model.setCompanyId(GeneralUtil.getCompanyId());
        roleService.update(Mapper.map(model));
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody RoleModel updated, @PathVariable String uuid) {
        Role entity = roleService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(updated));
        roleService.update(entity);
    }

    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable String uuid) {
        roleService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
