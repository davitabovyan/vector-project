#!/bin/sh

case $1 in
  p)
    MODULE="payroll"
    ;;
  s)
    MODULE="security"
    ;;
esac

screen -L -Logfile log_${MODULE} -S ${MODULE} /bin/bash "deploy.sh" ${MODULE}
