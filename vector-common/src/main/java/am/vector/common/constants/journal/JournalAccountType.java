package am.vector.common.constants.journal;

public enum JournalAccountType {
    ACTIVE,
    PASSIVE,
    ACTIVE_PASSIVE
}
