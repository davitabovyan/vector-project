package am.vector.payroll.api.repository.main;

import am.vector.common.persistence.BaseRepository;
import am.vector.payroll.api.entity.main.Person;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.QueryHint;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "person", path = "person", exported = false)
public interface PersonRepository extends BaseRepository<Person, Long> {
//
//    @Query("SELECT p FROM Person p where p.id > :idStart and p.id < :idEnd")
//    List<Person> getAllPersons(@Param("idStart") long start, @Param("idEnd") long end);

    @Query("SELECT p FROM Person p where p.companyId = :companyId")
    @QueryHints({
            @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHEABLE, value = "true"),
            @QueryHint(name = org.hibernate.jpa.QueryHints.HINT_CACHE_REGION, value = "allPersonsByCustomerId")
    })
    @Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
    List<Person> findAllPerCompany(@Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Person p where p.id = :id")
    void deleteById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Person a where a.companyId = :companyId")
    void deleteAll(@Param("companyId") int companyId);
}
