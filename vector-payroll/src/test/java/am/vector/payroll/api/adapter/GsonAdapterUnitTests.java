package am.vector.payroll.api.adapter;

import am.vector.common.adapter.GsonAdapter;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.TestUtil;
import am.vector.payroll.api.entity.main.Contract;
import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.entity.main.Role;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class GsonAdapterUnitTests {

    private Role role;
    private Person person;
    private Department department;
    private Contract contract;
    private Map<String, String> fields;
    private GsonAdapter adapter;

    @Before
    public void setup(){
        adapter = new GsonPayrollAdapter();
        person = new Person().setSsn("1034512395").setLastName("Ashot");
        person.setId(TestUtil.generateId(person));
        department = new Department().setCode("0003");
        department.setId(TestUtil.generateId(department));
        role = new Role()
            .setCode("0002")
            .setName("Accountant")
            .setDescription("The accountant")
            .setEligibleDays(20);
        role.setId(TestUtil.generateId(role));
        contract = new Contract();
        contract.setId(TestUtil.generateId(contract));
        contract.setPerson(person);
        contract.setDepartment(department);
        contract.setRole(role);
        fields = new HashMap<>();
    }

    @Test
    public void getGsonWithDefaultFields_success(){
        // Setup
        // Act
        String jsonString = adapter.toJson(role);
        // Assert
        JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
        assertTrue("Json doesn't contain Uuid", json.has("uuid"));
        assertEquals("Uuid field value is incorrect", GeneralUtil.encodeUuid(role.getId()), json.get("uuid").getAsString());
        assertTrue("Json doesn't contain default fields",
                json.has("code") & json.has("name") & json.has("description"));
        assertEquals("Json field values is incorrect", role.getCode(), json.get("code").getAsString());
        assertEquals("Json field values is incorrect", role.getName(), json.get("name").getAsString());
        assertEquals("Json field values is incorrect", role.getDescription(), json.get("description").getAsString());
        assertFalse("Json default fields also contain not default ones", json.has("eligibleDays"));
    }

    @Test
    public void getGsonWithCustomFields_success(){
        // Setup
        // Act
        String jsonString = adapter.toJson(role, Collections.singletonMap("code", null));
        // Assert
        JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
        assertTrue("Json doesn't contain Uuid", json.has("uuid"));
        assertEquals("Uuid field value is incorrect", GeneralUtil.encodeUuid(role.getId()), json.get("uuid").getAsString());
        assertTrue("Json not contains requested field", json.has("code"));
        assertFalse("Json contains not requested default fields", json.has("name"));
    }

    @Test
    public void getGsonWithNotDefaultFields_success(){
        // Setup
        fields.put("*", null);
        fields.put("eligibleDays", null);
        // Act
        String jsonString = adapter.toJson(role, fields);
        // Assert
        JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
        assertTrue("Json doesn't contain Uuid", json.has("uuid"));
        assertEquals("Uuid field value is incorrect", GeneralUtil.encodeUuid(role.getId()), json.get("uuid").getAsString());
        assertTrue("Json not contains requested field", json.has("eligibleDays"));
        assertTrue("Json not contains default fields", json.has("code"));
    }

    @Test
    public void getGsonWithNestedFields_success(){
        // Setup
        // Act
        String jsonString = adapter.toJson(contract);
        // Assert
        JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
        assertTrue("Json doesn't contain Uuid", json.has("uuid"));
        assertEquals("Uuid field value is incorrect",
                GeneralUtil.encodeUuid(contract.getId()), json.get("uuid").getAsString());
        assertTrue("Json not contains nested object",
                json.has("person") && json.has("department") && json.has("role"));
        JsonObject jsonPerson = json.get("person").getAsJsonObject();
        JsonObject jsonDepartment = json.get("department").getAsJsonObject();
        JsonObject jsonRole = json.get("role").getAsJsonObject();
        assertTrue("Json nested object not has uuid fields",
                jsonPerson.has("uuid") && jsonDepartment.has("uuid") && jsonRole.has("uuid"));
        assertEquals("nested object has fields other then uuid", 1, jsonPerson.size());
        assertEquals("nested object has fields other then uuid", 1, jsonDepartment.size());
        assertEquals("nested object has fields other then uuid", 1, jsonRole.size());
    }

    @Test
    public void getGsonWithNestedRequestedFields_success(){
        // Setup
        fields.put("*", null);
        fields.put("person", "ssn,lastName");
        fields.put("department", "code");
        // Act
        String jsonString = adapter.toJson(contract, fields);
        // Assert
        JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
        assertTrue("Json doesn't contain Uuid", json.has("uuid"));
        assertEquals("Uuid field value is incorrect",
                GeneralUtil.encodeUuid(contract.getId()), json.get("uuid").getAsString());
        assertTrue("Json not contains nested object",
                json.has("person") && json.has("department") && json.has("role"));
        JsonObject jsonPerson = json.get("person").getAsJsonObject();
        JsonObject jsonDepartment = json.get("department").getAsJsonObject();
        JsonObject jsonRole = json.get("role").getAsJsonObject();
        assertTrue("Json nested object not has uuid fields",
                jsonPerson.has("uuid") && jsonDepartment.has("uuid") && jsonRole.has("uuid"));
        assertEquals("nested object has fields other then uuid", 3, jsonPerson.size());
        assertEquals("nested object has fields other then uuid", 2, jsonDepartment.size());
        assertEquals("nested object has fields other then uuid", 1, jsonRole.size());
        assertTrue("Nested object's requested fields missing",
                jsonPerson.has("ssn") && jsonPerson.has("lastName"));
        assertEquals("Nested object's requested field value is incorrect",
                person.getSsn(), jsonPerson.get("ssn").getAsString());
    }
}
