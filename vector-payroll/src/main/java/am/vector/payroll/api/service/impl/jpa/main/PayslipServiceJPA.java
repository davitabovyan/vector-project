package am.vector.payroll.api.service.impl.jpa.main;

import am.vector.common.constants.LeaveType;
import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Payslip;
import am.vector.payroll.api.entity.main.embedded.SalaryAverage;
import am.vector.payroll.api.repository.main.PayslipRepository;
import am.vector.payroll.api.service.PayslipService;
import am.vector.payroll.util.DayUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PayslipServiceJPA implements PayslipService {

    private final PayslipRepository payslipRepository;

    public Payslip findById(long id){
        return payslipRepository.findById(id).orElseThrow(
            ()-> new EntityNotFoundException(Payslip.class, GeneralUtil.encodeUuid(id))
        );
    }

    public List<Payslip> getPayslipsByPeriodWithPerson(int companyId, String period){
        return payslipRepository.getPayslipsByPeriodWithPerson(companyId, period);
    }

    public List<Payslip> getPayslipsByPeriod(int companyId, String period){
        return payslipRepository.getPayslipsByPeriod(companyId, period);
    }

    @Override
    public SalaryAverage getAverageForPerson(long personId, LocalDate from, Map<String, LeaveType> absences) {
        SalaryAverage average = new SalaryAverage();
        Map<String, Long> payslips = payslipRepository.getPayslipsByPerson(personId)
                .stream().collect(Collectors.toMap(Payslip::getPeriod,Payslip::getWage));

        for (int i=1;i<=12;++i) {
            String period = DayUtil.generatePeriod(from.minus(i, ChronoUnit.MONTHS));
            average.setVelue(i, absences.containsKey(period) ? null : payslips.get(period));
        }
        return average;
    }

    public List<Payslip> findAll(){
        return Collections.EMPTY_LIST; //payslipRepository.findAll(); TODO should be per customer
    }

    public Payslip getPayslipWithPerson(int companyId, long id){
        return payslipRepository.getPayslipWithPerson(id).orElseThrow(
            ()-> new EntityNotFoundException(Payslip.class, GeneralUtil.encodeUuid(id))
        );
    }

    @Transactional
    @Override
    public void update(Payslip object){
        GeneralUtil.setRecordStatus(object);
        payslipRepository.save(object);
    }
    @Transactional
    @Override
    public Payslip save(Payslip object){
        return payslipRepository.save(object);
    }

    @Override
    public List<Payslip> saveAll(List<Payslip> payslips) {
        return payslipRepository.saveAll(payslips);
    }

    @Override
    public List<Payslip> findAllById(List<Long> id) {
        return payslipRepository.findAllById(id);
    }

    @Override
    public void deleteById(long id) {
        payslipRepository.deleteById(id);
    }

    @Override
    public void deleteAll(int companyId) {
        payslipRepository.deleteAll(companyId);
        logger.info(String.format("All record are delete by user %s", GeneralUtil.getUserFromContext().getUsername()));
    }
}
