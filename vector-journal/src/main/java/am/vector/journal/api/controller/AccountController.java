package am.vector.journal.api.controller;

import am.vector.common.exception.generic.BadRequestVectorException;
import am.vector.common.model.journal.AccountModel;
import am.vector.common.util.GeneralUtil;
import am.vector.journal.api.adapter.GsonJournalAdapter;
import am.vector.journal.api.entity.Account;
import am.vector.journal.api.service.AccountService;
import am.vector.journal.util.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/account", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class AccountController {

    private final AccountService accountService;
    private final GsonJournalAdapter adapter;

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findById(@PathVariable String uuid,
                                            @RequestParam(value = "fields", required = false) String fields){
        Account response = accountService.findById(GeneralUtil.decodeUuid(uuid));
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @GetMapping("/")
    public ResponseEntity<String> findAll(@RequestParam(value = "fields", required = false) String fields){
        List<Account> response = accountService.findAllPerCompany();
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @GetMapping("/nodes")
    public ResponseEntity<String> findAllChildren(@RequestParam(value = "fields", required = false) String fields){
        List<Account> response = accountService.findAllNodes();
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @PostMapping("/")
    public ResponseEntity<Map<String,Object>> create(@RequestBody AccountModel model) {
        Account account = Mapper.map(model);
        account = accountService.save(account);
        return new ResponseEntity<>(GeneralUtil.uuidJSON(account), HttpStatus.CREATED);
    }

    @PatchMapping("/{uuid}/parent/{enable}")
    public void chaneAccountAggregation(@PathVariable String uuid, @PathVariable boolean enable) {
        accountService.chaneAccountAggregation(GeneralUtil.decodeUuid(uuid), enable);
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody AccountModel model, @PathVariable String uuid) {
        if(model.getIsParent() != null) {
            throw new BadRequestVectorException("error.bad_request", "Use \"/{uuid}/parent/{enable}\" to change aggregation");
        }
        Account entity = accountService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(model));
        accountService.update(entity);
    }

    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable String uuid) {
        accountService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
