package am.vector.security.controller;

import java.util.Map;

import am.vector.common.model.admin.UserModel;
import am.vector.common.util.GeneralUtil;
import am.vector.security.adapter.UserTypeAdapter;
import am.vector.security.service.UserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "api/v1/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class UserController {

    private final UserService userService;
    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(UserModel.class,new UserTypeAdapter())
            .create();

    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody UserModel user){
        user.setCompanyId(GeneralUtil.getCompanyId());
        return ResponseEntity.ok(GSON.toJson(userService.save(user)));
    }

    @GetMapping
    public ResponseEntity<String> getAll(){
        Iterable<UserModel> objects = userService.findAll(GeneralUtil.getCompanyId());
        return ResponseEntity.ok(GSON.toJson(objects));
    }

    @PutMapping("/permissions")
    @Secured("ROLE_SUPERADMIN")
    public void updatePermissions(@RequestParam("userName") String userName, @RequestBody Map<String, String> map){
        UserModel updatable = userService.findUserByUserName(userName);
        userService.updatePermissions(updatable, map);
    }

    @DeleteMapping("/{userName}")
    public void removeUser(@PathVariable("userName") String userName){
        userService.removeUser(userName);
    }
}
