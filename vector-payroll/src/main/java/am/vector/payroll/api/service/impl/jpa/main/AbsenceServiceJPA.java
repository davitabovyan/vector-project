package am.vector.payroll.api.service.impl.jpa.main;

import am.vector.common.constants.LeaveType;
import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.CalculationUtil;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Absence;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.entity.main.embedded.SalaryAverage;
import am.vector.payroll.api.repository.main.AbsenceRepository;
import am.vector.payroll.api.service.AbsenceService;
import am.vector.payroll.api.service.ContractService;
import am.vector.payroll.api.service.PayslipService;
import am.vector.payroll.util.DayUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class AbsenceServiceJPA implements AbsenceService {

    private final AbsenceRepository absenceRepository;
    private final ContractService contractService;
    private final PayslipService payslipService;

    public Absence findById(long id){
        return absenceRepository.findById(id).orElseThrow(
            ()-> new EntityNotFoundException(Absence.class, GeneralUtil.encodeUuid(id))
        );
    }

    public List<Absence> findByPeriod(String period){
        return absenceRepository.findByPeriod(GeneralUtil.getCompanyId(), period);
    }

    public List<Absence> findByPerson(long personId){
        return absenceRepository.findByPerson(new Person(personId));
    }

    public List<Absence> findByPersonByPeriod(long personUuid, String period){
        return absenceRepository.findByPersonByPeriod(new Person(personUuid), period);
    }

    public Map<Long, List<Absence>> findAllByCompanyIdAndPeriod(int companyId, String period){
        Iterable<Absence> allByCompanyIdAndPeriod = absenceRepository.findAllByCompanyIdAndPeriod(companyId,
            period);
        return StreamSupport.stream(allByCompanyIdAndPeriod.spliterator(), false)
            .filter(Absence::isNotParent)
            .collect(Collectors.groupingBy(a->a.getPerson().getId()));
    }

    @Transactional
    @Override
    public void update(Absence object){
        GeneralUtil.setRecordStatus(object);
        absenceRepository.save(object);
    }

    @Transactional
    @Override
    public Absence save(Absence object){
        object.setPeriod(DayUtil.generatePeriod(object.getStartDate()));
        boolean isFiveDayWeek = contractService.isFiveDayWeek(object.getPerson().getId());
        SalaryAverage average = getAverage(object);
        if(object.getLeaveType() != LeaveType.MATERNITY_LEAVE &&
                !object.getStartDate().getMonth().equals(object.getEndDate().getMonth())){
            object.setIsParent(true);
            object = absenceRepository.save(object);
            int numberOfMonths = (int) ChronoUnit.MONTHS.between(YearMonth.from(object.getStartDate()),YearMonth.from(object.getEndDate()));

            for(int i=0;i<=numberOfMonths;++i){
                LocalDate startDate = i == 0 ? object.getStartDate() : object.getStartDate().plusMonths(i).withDayOfMonth(1);
                LocalDate endDate;
                if(i == numberOfMonths) {
                    endDate =  object.getEndDate();
                } else {
                    endDate =  startDate.withDayOfMonth(startDate.lengthOfMonth());
                }
                Absence absenceChild = object.copy()
                        .setIsParent(false)
                        .setParentId(object.getId())
                        .setPeriod(DayUtil.generatePeriod(startDate))
                        .setStartDate(startDate)
                        .setEndDate(endDate)
                        .setAverageMonthly(average)
                        .setAmount(calculateVacation(startDate, endDate, average, isFiveDayWeek));

                absenceRepository.save(absenceChild);
            }
        } else {
            object = absenceRepository.save(object);
        }
        return object;
    }

    @Override
    public SalaryAverage getAverage(Absence absence) {
        Map<String, LeaveType> absences = absenceRepository.findByPerson(absence.getPerson())
                .stream().collect(Collectors.toMap(Absence::getPeriod,Absence::getLeaveType));
        return payslipService.getAverageForPerson(absence.getPerson().getId(), absence.getStartDate(), absences);
    }

    @Override
    public List<Absence> saveAll(List<Absence> absences) {
        return absenceRepository.saveAll(absences);
    }

    @Override
    public List<Absence> findAllById(List<Long> id) {
        return absenceRepository.findAllById(id);
    }

    @Override
    public void deleteById(long id) {
        Absence absence = findById(id);
        if(absence.isParent()){
            absenceRepository.deleteChildrenByParentId(id);
        }
        absenceRepository.deleteById(id);
    }

    @Override
    public void deleteAll(int companyId) {
        absenceRepository.deleteAll(companyId);
    }

    public long calculateVacation(LocalDate start, LocalDate end, SalaryAverage average, boolean isFiveDayWeek) {
        BigDecimal averageMonthly = average.get();
        BigDecimal divisor = isFiveDayWeek ? BigDecimal.valueOf(20) : BigDecimal.valueOf(24);
        BigDecimal averageDaily = averageMonthly.divide(divisor, 4, RoundingMode.HALF_EVEN);
        long workingDays = CalculationUtil.numberOfWorkDays(start, end, isFiveDayWeek);
        return averageDaily.multiply(BigDecimal.valueOf(workingDays)).longValue();
    }
}
