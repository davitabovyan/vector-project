package am.vector.journal.api.repository;

import am.vector.common.persistence.ClientCodeIdRepository;
import am.vector.journal.api.entity.AccountGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountGroupRepository extends ClientCodeIdRepository<AccountGroup> {

    @Query("SELECT a FROM AccountGroup a WHERE a.id > :idStart AND a.id < :idEnd")
    List<AccountGroup> findAllPerCompany(@Param("idStart") long start, @Param("idEnd") long end);

    List<AccountGroup> findAllByIdIn(@Param("ids") List<Long> ids);

    @Query("DELETE FROM AccountGroup a WHERE a.id > :idStart AND a.id < :idEnd")
    void deleteAll(@Param("idStart") long start, @Param("idEnd") long end);
 }
