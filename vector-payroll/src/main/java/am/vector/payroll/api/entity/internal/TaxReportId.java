package am.vector.payroll.api.entity.internal;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TaxReportId implements Serializable {
    private int period;
    private String ssn;
}
