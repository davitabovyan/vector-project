package am.vector.common.config.interceptor;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import am.vector.common.util.GeneralUtil;
import com.sun.istack.Nullable;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.UriUtils;

@Component
public class LogInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        URI uri = new URI(request.getRequestURL().toString());
        logger.printf(
                Level.INFO,
                "{\"companyId\": %d, \"user\": \"%s\", \"name\": \"request\", \"sessionId\": \"%s\", \"method\":\"%s\", \"host\": \"%s\", \"endpoint\": \"%s\", \"query\": \"%s\"}",
                GeneralUtil.isAnonymous() ? 0 : GeneralUtil.getCompanyId(),
                GeneralUtil.isAnonymous() ? "" : GeneralUtil.getUserFromContext().getUsername(),
                request.getSession().getId(),
                request.getMethod(),
                uri.getHost(),
                uri.getPath(),
                UriUtils.decode(null != request.getQueryString() ? request.getQueryString() : "","UTF-8")
        );

        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           @Nullable ModelAndView modelAndView) throws Exception {

        logger.printf(
                Level.INFO,
                "{\"companyId\": %d, \"user\": \"%s\", \"name\": \"response\", \"sessionId\": \"%s\", \"status\": \"%s\"}",
                GeneralUtil.isAnonymous() ? 0 : GeneralUtil.getCompanyId(),
                GeneralUtil.isAnonymous() ? "" : GeneralUtil.getUserFromContext().getUsername(),
                request.getSession().getId(),
                response.getStatus()
        );

        super.postHandle(request, response, handler, modelAndView);
    }
}
