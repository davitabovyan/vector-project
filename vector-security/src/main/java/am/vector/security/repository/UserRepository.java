package am.vector.security.repository;

import java.util.List;
import java.util.Optional;

import am.vector.security.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

@RepositoryRestResource(collectionResourceRel = "user", path = "user", exported = false)
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT u FROM User u where u.username = :userName")
    Optional<User> findByUsername(@Param("userName") String userName);

    @Query("SELECT u FROM User u where u.companyId = :companyId")
    List<User> findAllPerCompany(@Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM User u where u.username = :userName")
    void deleteByUserName(@Param("userName") String userName);
}
