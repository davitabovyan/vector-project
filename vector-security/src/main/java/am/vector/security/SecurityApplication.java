package am.vector.security;

import am.vector.common.config.security.SecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@SpringBootApplication
@ComponentScan(
        basePackages = {"am.vector.security", "am.vector.common.config"},
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = SecurityConfig.class)
)
public class SecurityApplication implements RepositoryRestConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }
}