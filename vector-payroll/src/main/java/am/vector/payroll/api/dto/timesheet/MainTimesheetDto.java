package am.vector.payroll.api.dto.timesheet;

import am.vector.payroll.util.DayUtil;
import am.vector.common.util.GeneralUtil;
import lombok.Getter;

import java.time.LocalDate;
import java.util.*;

@Getter
public class MainTimesheetDto {
    private final String uuid;
    private final int total;
    private final String period;
    private final String personUuid;
    private final String code;
    private final String name;
    private final String roleName;
    private final String departmentCode;
    private final Map<LocalDate, Integer> hours;

    public MainTimesheetDto(long personId, String firstName, String lastName, String middleName, String code, String roleName, long timesheetId,
                            String departmentCode, String period, Integer total,
                            Integer day1, Integer day2, Integer day3, Integer day4, Integer day5, Integer day6, Integer day7, Integer day8, Integer day9, Integer day10,
                            Integer day11, Integer day12, Integer day13, Integer day14, Integer day15, Integer day16, Integer day17, Integer day18, Integer day19, Integer day20,
                            Integer day21, Integer day22, Integer day23, Integer day24, Integer day25, Integer day26, Integer day27, Integer day28, Integer day29, Integer day30, Integer day31) {
        hours = new LinkedHashMap<>();
        hours.put(DayUtil.parsePeriod(period, 1), day1);
        hours.put(DayUtil.parsePeriod(period, 2), day2);
        hours.put(DayUtil.parsePeriod(period, 3), day3);
        hours.put(DayUtil.parsePeriod(period, 4), day4);
        hours.put(DayUtil.parsePeriod(period, 5), day5);
        hours.put(DayUtil.parsePeriod(period, 6), day6);
        hours.put(DayUtil.parsePeriod(period, 7), day7);
        hours.put(DayUtil.parsePeriod(period, 8), day8);
        hours.put(DayUtil.parsePeriod(period, 9), day9);
        hours.put(DayUtil.parsePeriod(period, 10), day10);
        hours.put(DayUtil.parsePeriod(period, 11), day11);
        hours.put(DayUtil.parsePeriod(period, 12), day12);
        hours.put(DayUtil.parsePeriod(period, 13), day13);
        hours.put(DayUtil.parsePeriod(period, 14), day14);
        hours.put(DayUtil.parsePeriod(period, 15), day15);
        hours.put(DayUtil.parsePeriod(period, 16), day16);
        hours.put(DayUtil.parsePeriod(period, 17), day17);
        hours.put(DayUtil.parsePeriod(period, 18), day18);
        hours.put(DayUtil.parsePeriod(period, 19), day19);
        hours.put(DayUtil.parsePeriod(period, 20), day20);
        hours.put(DayUtil.parsePeriod(period, 21), day21);
        hours.put(DayUtil.parsePeriod(period, 22), day22);
        hours.put(DayUtil.parsePeriod(period, 23), day23);
        hours.put(DayUtil.parsePeriod(period, 24), day24);
        hours.put(DayUtil.parsePeriod(period, 25), day25);
        hours.put(DayUtil.parsePeriod(period, 26), day26);
        hours.put(DayUtil.parsePeriod(period, 27), day27);
        hours.put(DayUtil.parsePeriod(period, 28), day28);
        LocalDate d29 = DayUtil.parsePeriod(period, 29);
        LocalDate d30 = DayUtil.parsePeriod(period, 30);
        LocalDate d31 = DayUtil.parsePeriod(period, 31);
        if(null != d29) hours.put(d29, day29);
        if(null != d30) hours.put(d30, day30);
        if(null != d31) hours.put(d31, day31);

        this.period = period;
        this.uuid = GeneralUtil.encodeUuid(timesheetId);
        this.total = total;
        this.personUuid = GeneralUtil.encodeUuid(personId);
        this.name = middleName != null ? String.format("%s %s %s", firstName, lastName, middleName) : String.format("%s %s", firstName, lastName);
        this.code = code;
        this.roleName = roleName;
        this.departmentCode = departmentCode;
    }
}
