package am.vector.common.constants;

public enum ErrorMessage {
    
    GENERAL("general"),
    NOT_FOUND("not_found"),
    MISSING_FIELD("missing_field"),
    NO_OPTION_SELECTED("no_option_selected"),
    UNSUPPORTED_FIELD("unsupported_field"),
    UNSUPPORTED_VALUE("unsupported_value"),
    INCORRECT_JSON_KEY("incorrect_json.key"),
    INCORRECT_JSON_VALUE("incorrect_json.value");
    
    private final String key;
    ErrorMessage(String key){
        this.key = key;
    }
    public String get(){
        return "error." + key;
    }
}
