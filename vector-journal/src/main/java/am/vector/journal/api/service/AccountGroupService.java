package am.vector.journal.api.service;

import am.vector.common.persistence.CommonService;
import am.vector.journal.api.entity.AccountGroup;

public interface AccountGroupService extends CommonService<AccountGroup> {
}
