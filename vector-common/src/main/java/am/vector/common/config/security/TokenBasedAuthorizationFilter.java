package am.vector.common.config.security;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import am.vector.common.model.admin.UserModel;
import am.vector.common.util.GeneralUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.SignatureException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class TokenBasedAuthorizationFilter extends BasicAuthenticationFilter {

	private static final Log LOG = LogFactory.getLog(TokenBasedAuthorizationFilter.class);
	private final AuthenticationEntryPoint authenticationEntryPoint;

	public TokenBasedAuthorizationFilter(AuthenticationManager authenticationManager, AuthenticationEntryPoint authenticationEntryPoint) {
		super(authenticationManager, authenticationEntryPoint);
		this.authenticationEntryPoint = authenticationEntryPoint;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
		throws IOException, ServletException {

		boolean debug = LOG.isDebugEnabled();
		String authorizationToken = request.getHeader(HttpHeaders.AUTHORIZATION);
		if (authorizationToken == null || authorizationToken.startsWith(SecurityConfig.TOKEN_PREFIX)) {
			SecurityContextHolder.clearContext();
		}
		if(null == authorizationToken){
			this.authenticationEntryPoint.commence(request, response, new BadCredentialsException("401"));
			return;
		}
		authorizationToken = authorizationToken.replaceFirst(SecurityConfig.TOKEN_PREFIX, "");
		Jws<Claims> claims;
		try {
			claims = Jwts.parser()
				.setSigningKey(SecurityConfig.TOKEN_SECRET.getBytes())
				.parseClaimsJws(authorizationToken);
		} catch (SignatureException ex){
			SecurityContextHolder.clearContext();
			if (debug) {
				LOG.debug("Authentication request for failed: " + authorizationToken);
			}
			this.authenticationEntryPoint.commence(request, response, new BadCredentialsException("401"));
			return;
		} catch (ExpiredJwtException ex){
			SecurityContextHolder.clearContext();
			this.authenticationEntryPoint.commence(request, response, new BadCredentialsException("417"));
			return;
		} catch (Exception ex){
			SecurityContextHolder.clearContext();
			this.authenticationEntryPoint.commence(request, response, new BadCredentialsException("403"));
			return;
		}

		Object readVal = claims.getBody().get(SecurityConfig.READ);
		Object editVal = claims.getBody().get(SecurityConfig.EDIT);
		boolean isSuperAdmin = (boolean) claims.getBody().get(SecurityConfig.IS_SUPERADMIN);
		boolean isAdmin = (boolean) claims.getBody().get(SecurityConfig.IS_ADMIN);
		UserModel user = new UserModel()
			.setUsername(claims.getBody().getSubject())
			.setCompanyId(GeneralUtil.decodeUuidToInt((String) claims.getBody().get(SecurityConfig.COMPANY)))
			.setSuperAdmin(isSuperAdmin)
			.setAdmin(isAdmin)
			.setReadAccess((readVal instanceof Integer) ? Long.valueOf((Integer) readVal) : (long) readVal)
			.setEditAccess((editVal instanceof Integer) ? Long.valueOf((Integer) editVal) : (long) editVal)
			.setLocale((String) claims.getBody().get(SecurityConfig.LOCALE));

		SecurityContextHolder.getContext()
			.setAuthentication(
					new UsernamePasswordAuthenticationToken(
							user,
							null,
							Collections.singleton(()->isSuperAdmin ? "ROLE_SUPERADMIN" : (isAdmin ? "ROLE_ADMIN" : "ROLE_USER")))
			);

		chain.doFilter(request, response);
	}
}
