package am.vector.security.service;

import am.vector.common.model.admin.CompanyModel;
import am.vector.security.dto.ClientDto;
import am.vector.security.entity.Client;

import java.util.List;

public interface ClientService {
    List<Client> findAll();
    Client findClientById(String id);
    Client findClientByEmail(String email);
    CompanyModel addCompany(CompanyModel company, Integer clientId);
    void update(Client client);
    Client save(ClientDto client);
    void remove(Integer id);
}
