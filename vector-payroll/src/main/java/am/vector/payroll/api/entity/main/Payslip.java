package am.vector.payroll.api.entity.main;

import am.vector.common.constants.EntityObject;
import am.vector.common.persistence.TimestampId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
@Accessors(chain = true) @Getter @Setter
public class Payslip extends TimestampId {

    private String period;
    private Long wage;
    private Long overtime;
    private Long bonus;
    private Long ssp;
    private Long it;
    private Long army;
    private Boolean hasUpdate;
    @OneToOne(fetch= FetchType.LAZY, optional=false)
    private Person person;

    public boolean hasUpdate(){
        return null != hasUpdate && hasUpdate;
    }

    public Payslip() {
        super(EntityObject.PAYSLIP);
    }

    public Payslip(long uuid) {
        super(EntityObject.PAYSLIP, uuid);
    }

    public Payslip copy(){
        return (Payslip) super.copy(this);
    }
}
