package am.vector.payroll.aop;

import am.vector.common.util.GeneralUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;


@Aspect
@Component
public class LoggingAspect {
    private static final Logger logger = LogManager.getLogger();

    @Around("@annotation(LogExecutionTime) && args(session,..)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint, HttpSession session) throws Throwable {
        long start = System.currentTimeMillis();

        Object proceed = joinPoint.proceed();

        long executionTime = System.currentTimeMillis() - start;
        logger.info(String.format("{\"companyId\": %d, \"user\": \"%s\", \"name\": \"execution_time\", \"sessionId\": \"%s\", \"class\":\"%s\",\"method\":\"%s\",\"executionTime\":%d}",
                GeneralUtil.getCompanyId(),
                GeneralUtil.getUserFromContext().getUsername(),
                session.getId(),
                joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(),
                executionTime
        ));
//        System.out.println(joinPoint.getSignature() + " executed in " + executionTime + "ms");
        return proceed;
    }
}
