package am.vector.common.persistence;

import java.util.List;

import am.vector.common.persistence.BaseService;

public interface CommonService<T> extends BaseService<T> {
	List<T> findAllPerCompany();
}
