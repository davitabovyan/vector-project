package am.vector.payroll.api.service;

import am.vector.common.constants.LeaveType;
import am.vector.common.persistence.BaseService;
import am.vector.payroll.api.entity.main.Payslip;
import am.vector.payroll.api.entity.main.embedded.SalaryAverage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface PayslipService extends BaseService<Payslip> {
    Logger logger = LogManager.getLogger();

    Payslip getPayslipWithPerson(int companyId, long id);
    List<Payslip> getPayslipsByPeriodWithPerson(int companyId, String period);
    List<Payslip> getPayslipsByPeriod(int companyId, String period);
    SalaryAverage getAverageForPerson(long personId, LocalDate from, Map<String, LeaveType> absences);

}
