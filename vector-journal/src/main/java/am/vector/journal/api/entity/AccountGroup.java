package am.vector.journal.api.entity;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.journal.AccountGroupType;
import am.vector.common.persistence.ClientCodeId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Entity
@Accessors(chain = true) @Getter @Setter
public class AccountGroup extends ClientCodeId {

    @NotNull
    private String name;
    @Enumerated(EnumType.STRING)
    private AccountGroupType type;

    public AccountGroup() {
        super(EntityObject.ACCOUNT_GROUP);
    }

    public AccountGroup(long uuid) {
        super(EntityObject.ACCOUNT_GROUP, uuid);
    }

    public AccountGroup copy(){
        return (AccountGroup) super.copy(this);
    }
}
