package am.vector.common.config.security;

import java.util.Arrays;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final long TOKEN_LIFETIME = 86_400_000; // 1 day
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_SECRET = "thoseWhoKnowEnoughIsEnoughWillAlwaysHaveEnough";

    public static final String IS_SUPERADMIN = "sa";
    public static final String IS_ADMIN = "ad";
    public static final String READ = "rd";
    public static final String EDIT = "ed";
    public static final String LOCALE = "lc";
    public static final String COMPANY = "cp";

    @Value("${app.env}")
    private String env;

    @Resource
    private AuthenticationEntryPoint authenticationEntryPoint;


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/actuator/health");
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
        if("prod".equals(env)) http.requiresChannel().anyRequest().requiresSecure(); // ssl
        http
                .cors().and()
            .httpBasic().disable()
            .formLogin().disable()
            .logout().disable()
            .addFilter(new TokenBasedAuthorizationFilter(authenticationManager(), authenticationEntryPoint))
            .authorizeRequests()
                .antMatchers("/api/**").hasAnyRole("SUPERADMIN", "ADMIN", "USER")
                .antMatchers("/actuator/**").hasRole("SUPERADMIN")
                    .anyRequest().authenticated()
                .antMatchers("/**").hasRole("ANANIMUS").anyRequest().denyAll();
//                .antMatchers(HttpMethod.PATCH,"/api/v1/user/**").hasRole("USER")
    }


    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("HEAD",
                "GET", "POST", "PUT", "DELETE", "PATCH"));
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.setAllowCredentials(true);
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
