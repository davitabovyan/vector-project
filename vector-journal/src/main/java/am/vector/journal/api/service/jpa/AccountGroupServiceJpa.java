package am.vector.journal.api.service.jpa;

import am.vector.common.util.GeneralUtil;
import am.vector.journal.api.entity.AccountGroup;
import am.vector.journal.api.repository.AccountGroupRepository;
import am.vector.journal.api.service.AccountGroupService;
import am.vector.journal.api.service.BaseClientCodeIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountGroupServiceJpa extends BaseClientCodeIdService<AccountGroup>
    implements AccountGroupService {

    private AccountGroupRepository accountGroupRepository;

    @Autowired
    public AccountGroupServiceJpa(AccountGroupRepository accountGroupRepository) {
        super(accountGroupRepository);
        this.accountGroupRepository = accountGroupRepository;
    }

    @Override
    @Transactional
    public void update(AccountGroup object) {
        GeneralUtil.setRecordStatus(object);
        accountGroupRepository.save(object);
    }

    @Override
    public AccountGroup save(AccountGroup object) {
        return accountGroupRepository.save(object);
    }

    @Override
    public List<AccountGroup> saveAll(List<AccountGroup> objects) {
        return accountGroupRepository.saveAll(objects);
    }
}
