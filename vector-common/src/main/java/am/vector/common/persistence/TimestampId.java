package am.vector.common.persistence;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.persistence.MappedSuperclass;

import am.vector.common.constants.EntityObject;

@MappedSuperclass
public abstract class TimestampId extends UUIDEntity {

    private int companyId;

    public int getCompanyId() {
        return companyId;
    }

    public TimestampId setCompanyId(int companyId) {
        this.companyId = companyId;
        return this;
    }

    public TimestampId(EntityObject object) {
        super(object);
    }

    public TimestampId(EntityObject object, long id) {
        super(object, id);
    }

    public LocalDateTime createdOn() {
        return LocalDateTime.ofInstant(
            Instant.ofEpochMilli(this.getId() / 1_000_000), ZoneId.systemDefault());
    };
}
