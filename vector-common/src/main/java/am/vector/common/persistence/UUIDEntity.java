package am.vector.common.persistence;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import am.vector.common.exception.VectorException;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.IOUtil;
import org.hibernate.annotations.GenericGenerator;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Objects;

@MappedSuperclass
public abstract class UUIDEntity implements Serializable {

    public static ModelMapper MAPPER = new ModelMapper();

    @Transient
    private final EntityObject OBJ;

    @Id
    @GeneratedValue(generator = "custom")
    @GenericGenerator(name="custom", strategy = "am.vector.common.config.VectorIdentifier")
    private long id;

    @Enumerated(EnumType.STRING)
    private RecordState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUuid(String uuid) {
        this.id = GeneralUtil.decodeUuid(uuid);
    }

    public String getUuid() {
        return GeneralUtil.encodeUuid(id);
    }

    public RecordState getState() {
        return state;
    }

    public UUIDEntity setState(RecordState state) {
        this.state = state;
        return this;
    }

    public abstract int getCompanyId();

    public EntityObject getObjCode() {
        return this.OBJ;
    }

    public UUIDEntity(EntityObject object) {
        this.state = RecordState.CURRENT;
        this.OBJ = object;
    }

    public UUIDEntity(EntityObject object, long id) {
        this.OBJ = object;
        this.id = id;
    }

    public int getObjCodeIndex() {
        return this.OBJ.getIndex();
    }

    public UUIDEntity copy(Object orig) {
        Object obj = IOUtil.copy(orig);
        UUIDEntity copy = (UUIDEntity) obj;
        if(null != copy){
            copy.setId(0L);
        }
        return copy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UUIDEntity that = (UUIDEntity) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public <T> void mergeObject(T entity) {
        Class<?> clazz = entity.getClass();
        if (this.getClass() != clazz) throw new VectorException("Merging incompatible objects");
        Field[] fields = clazz.getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                Object newValue = field.get(entity);
                if (newValue != null) field.set(this, newValue);
            }
        }catch (IllegalAccessException ex){
            throw new VectorException("",ex);
        }
    }
}
