package am.vector.common.rest;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

public class BaseApi implements BaseApiInterface {

    private RestTemplate restTemplate = setRestTemplate();

    private RestTemplate setRestTemplate() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }

                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSslcontext(sslContext)
                    .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                    .build();
            HttpComponentsClientHttpRequestFactory customRequestFactory = new HttpComponentsClientHttpRequestFactory();
            customRequestFactory.setHttpClient(httpClient);
            return new RestTemplate(customRequestFactory);
        } catch (NoSuchAlgorithmException | KeyManagementException ex){
            return null;
        }
    }

    public ResponseEntity<String> makeGet(HttpHeaders headers, String uri){
        return restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public ResponseEntity<String> makePost(HttpHeaders headers, String uri, String body){
        return restTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), String.class);
    }

    public ResponseEntity<String> makePut(HttpHeaders headers, String uri, String body){
        return restTemplate.exchange(uri, HttpMethod.PUT, new HttpEntity<>(body, headers), String.class);
    }

    public ResponseEntity<String> makePatch(HttpHeaders headers, String uri, String body){
        return restTemplate.exchange(uri, HttpMethod.PATCH, new HttpEntity<>(body, headers), String.class);
    }

    public ResponseEntity<String> makeDelete(HttpHeaders headers, String uri) {
        return restTemplate.exchange(uri, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }
}

