package am.vector.common.config;

import am.vector.common.model.admin.UserModel;

public class Context {

	private UserModel currentUser;
	private long objectId;

	public Context(){}
	public Context(UserModel currentUser, long objectId) {
		this.currentUser = currentUser;
		this.objectId = objectId;
	}

	public UserModel getUser() {
		return currentUser;
	}

	public void setUser(UserModel currentUser) {
		this.currentUser = currentUser;
	}

	public long getId() {
		return objectId;
	}

	public void setId(long objectId) {
		this.objectId = objectId;
	}
}

