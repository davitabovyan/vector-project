package am.vector.payroll.api.controller.main;

import am.vector.common.model.common.DepartmentModel;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.IOUtil;
import am.vector.payroll.aop.LogExecutionTime;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.service.DepartmentService;
import am.vector.payroll.util.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/department", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class DepartmentController {

    private final DepartmentService departmentService;
    private final GsonPayrollAdapter adapter;

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findByIdCompact(@PathVariable String uuid,
                                          @RequestParam(value = "fields", required = false) String fields){
        Department response;
        if(IOUtil.parseFields(Department.class, fields).containsKey("head")) {
            response = departmentService.getDepartmentWithHead(GeneralUtil.decodeUuid(uuid));
        } else {
            response = departmentService.findById(GeneralUtil.decodeUuid(uuid));
        }
        return ResponseEntity.ok(adapter.toJson(response, IOUtil.parseFields(Department.class, fields)));
    }

    @GetMapping
    @LogExecutionTime
    public ResponseEntity<String> findAllCompact(HttpSession session, @RequestParam(value = "fields", required = false) String fields){
        Iterable<Department> response;
        if(IOUtil.parseFields(Department.class, fields).containsKey("head")) {
            response = departmentService.getAllDepartmentsWithHead();
        } else {
            response = departmentService.findAllPerCompany();
        }
        return ResponseEntity.ok(adapter.toJson(response, IOUtil.parseFields(Department.class, fields)));
    }

    @PostMapping
    public ResponseEntity<Map<String,Object>> create(@RequestBody DepartmentModel department) {
        department.setCompanyId(GeneralUtil.getCompanyId());
        Department entity = departmentService.save(Mapper.map(department));
        return new ResponseEntity<>(GeneralUtil.uuidJSON(entity), HttpStatus.CREATED);
    }

    @PutMapping("/{uuid}")
    public void update(@RequestBody DepartmentModel object, @PathVariable String uuid) {
        object.setId(GeneralUtil.decodeUuid(uuid));
        object.setCompanyId(GeneralUtil.getCompanyId());
        departmentService.update(Mapper.map(object));
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody DepartmentModel updated, @PathVariable String uuid) {
        Department entity = departmentService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(updated));
        departmentService.update(entity);
    }

    @DeleteMapping("/{uuid}")
    public void findByIdCompact(@PathVariable String uuid){
        departmentService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}

