package am.vector.common.model.payroll;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.Gender;
import am.vector.common.model.MainModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Accessors(chain = true) @Getter @Setter
public class PersonModel extends MainModel {

    private String ssn;
    private String code;
    private String firstName;
    private String lastName;
    private String middleName;
    private String email;
    private String cell;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-M-d")
    private LocalDate birthday;
    private Gender gender;

    public void assignGender(String gender){
        this.gender = gender != null ? Gender.valueOf(gender) : null;
    }

    public PersonModel() {
        super(EntityObject.PERSON);
    }

    public PersonModel(String uuid) {
        super(EntityObject.PERSON, uuid);
    }

    public PersonModel copy(){
        return (PersonModel) super.copy(this);
    }
}
