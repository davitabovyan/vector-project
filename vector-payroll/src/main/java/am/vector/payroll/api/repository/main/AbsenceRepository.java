package am.vector.payroll.api.repository.main;

import am.vector.common.persistence.BaseRepository;
import am.vector.payroll.api.entity.main.Absence;
import am.vector.payroll.api.entity.main.Person;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "absence", path = "absence", exported = false)
public interface AbsenceRepository extends BaseRepository<Absence, Long> {
    @Query("SELECT a FROM Absence a where a.person = :person and a.period = :period")
    List<Absence> findByPersonByPeriod(@Param("person") Person person, @Param("period") String period);

    @Query("SELECT a FROM Absence a where a.person = :person")
    List<Absence> findByPerson(@Param("person") Person person);

    @Query("SELECT a FROM Absence a where a.companyId = :companyId and a.period = :period")
    List<Absence> findByPeriod(@Param("companyId") int companyId, @Param("period") String period);

    @Query("SELECT a FROM Absence a where a.parentId = :parentId")
    List<Absence> findAllPerPerson(@Param("parentId") long parentId);

    List<Absence> findAllByCompanyIdAndPeriod(@Param("companyId") int companyId, @Param("period") String period);

    @Transactional
    @Modifying
    @Query("DELETE FROM Absence a where a.id = :id")
    void deleteById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Absence a where a.parentId = :parentId")
    void deleteChildrenByParentId(@Param("parentId") long parentId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Absence a where a.companyId = :companyId")
    void deleteAll(@Param("companyId") int companyId);
}
