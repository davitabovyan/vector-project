package am.vector.common.model.payroll;

import am.vector.common.constants.EntityObject;
import am.vector.common.model.MainModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true) @Getter @Setter
public class PayslipModel extends MainModel {

    private String period;
    private Integer wage;
    private Integer overtime;
    private Integer bonus;
    private Integer ssp;
    private Integer it;
    private Integer army;
    private Boolean hasUpdate;
    private PersonModel person;

    public boolean hasUpdate(){
        return null != hasUpdate && hasUpdate;
    }

    public PayslipModel() {
        super(EntityObject.PAYSLIP);
    }

    public PayslipModel(String uuid) {
        super(EntityObject.PAYSLIP, uuid);
    }

    public PayslipModel copy() {
        return (PayslipModel) super.copy(this);
    }
}
