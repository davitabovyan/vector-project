package am.vector.payroll.api.v1.integration.controller.cases;

import am.vector.common.constants.EntityObject;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.Pair;
import am.vector.payroll.PayrollApplication;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.v1.integration.controller.ControllerBaseTest;
import am.vector.payroll.util.EntityBuilder;
import am.vector.payroll.util.PayrollApi;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PayrollApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PersonControllerIntegrationTests extends ControllerBaseTest {

    @Test
    public void createPerson_success() {
        Person person = EntityBuilder.getPersonOne();
        testCreate(person, adapter.toJson(person));
    }

    @Test
    public void createPersonDetailed_success() {
        // Setup
        // Act
        Person person = create(EntityBuilder.getPersonOne());
        // Assert
        compareExpectedWithActual(EntityBuilder.getPersonOne(), person);
    }

    @Test
    public void createPersons_success() {
        // Setup
        // Act
        List<Person> persons = create(EntityBuilder.getPersonsList());
        // Assert
        assertEquals("Saved and returned persons count is incorrect", 2, persons.size());
        String personOneSsn = EntityBuilder.getPersonOne().getSsn();
        Person expected;
        for(Person person : persons) {
            if (personOneSsn.equals(person.getSsn())){
                expected = EntityBuilder.getPersonOne();
            } else {
                expected = EntityBuilder.getPersonTwo();
            }
            compareExpectedWithActual(expected, person);
        }
    }

    @Test
    public void getPerson_success() {
        // Setup
        Person expected = EntityBuilder.getPersonOne();
        Person person = create(expected);
        // Act
        Pair<Person, HttpStatus> response = getResponse(person.getId(), person.getObjCode(), Person.class);
        Person actual = response.getFirst();
        // Assert
        assertEquals("Response status is not correct", HttpStatus.OK, response.getSecond());
        compareExpectedWithActual(expected, actual);
    }

    @Test
    public void getPersonSelectedFields_success() {
        // Setup
        Person expected = EntityBuilder.getPersonOne();
        Person person = create(expected);
        // Act
        String response = PayrollApi.getEntityById(
                getHeaders(),
                getHostAndPort(),
                person.getId(),
                EntityObject.PERSON,
                Collections.singletonMap("email", null)
        ).getBody();
        // Assert
        JsonObject json = new JsonParser().parse(response).getAsJsonObject();
        assertEquals("Incorrect number of fields returned",2, json.keySet().size());
        assertEquals("Uuid is not returned correctly", GeneralUtil.encodeUuid(person.getId()), json.get("uuid").getAsString());
        assertEquals("Requested field is not returned", expected.getEmail(), json.get("email").getAsString());
    }

    @Test
    public void editPerson_success() {
        // Setup
        Person expected = EntityBuilder.getPersonOne()
                .setCode("0113")
                .setFirstName("_Hakobyan")
                .setLastName("_Ashot")
                .setMiddleName("_Grigori")
                .setEmail("_hakob.abovyan@gmail.com")
                .setCell("37499999999")
                .setBirthday(LocalDate.parse("2012-12-12"));

        Person person = create(expected);
        expected.setId(person.getId());
        expected.setState(person.getState());
        // Act
        HttpStatus responseStatus = update(expected).getStatusCode();
        // Assert
        assertEquals("Response status on Person update isn't correct", HttpStatus.OK, responseStatus);
        Person actual = getPerson(person.getId());
        compareExpectedWithActual(expected, actual);
    }

    @Test
    public void editPersonSingleField_success() {
        // Setup
        Person expected = EntityBuilder.getPersonOne();
        long id = create(expected).getId();
        // Act
        HttpStatus responseStatus = PayrollApi.updateFields(
                getHeaders(), getHostAndPort(), "{\"firstName\":\"_Hakobyan\"}", expected.getId(), expected.getObjCode()).getStatusCode();
        // Assert
        assertEquals("Response status on Person update isn't correct", HttpStatus.OK, responseStatus);
        Person actual = getPerson(id);
        compareExpectedWithActual(expected.setFirstName("_Hakobyan"), actual);
    }

    @Test
    public void updatePerson_throwsNotAllowedForUserException() {
        // Setup
        Person person = create(EntityBuilder.getPersonOne());
        // Act
        ResponseEntity<String> response =
                PayrollApi.update(getViewerHeaders(), getHostAndPort(), adapter.toJson(person), person.getId(), person.getObjCode());
        // Assert
        testUserAuthority(response, EntityObject.PERSON, HttpMethod.PUT);
    }

    private void compareExpectedWithActual(Person expected, Person actual){
        assertEquals("Person's code is wrong", expected.getCode(), actual.getCode());
        assertEquals("Person's firstName is wrong", expected.getFirstName(), actual.getFirstName());
        assertEquals("Person's lastName is wrong", expected.getLastName(), actual.getLastName());
        assertEquals("Person's middleName is wrong", expected.getMiddleName(), actual.getMiddleName());
        assertEquals("Person's email is wrong", expected.getEmail(), actual.getEmail());
        assertEquals("Person's cell is wrong", expected.getCell(), actual.getCell());
        assertEquals("Person's birthday is wrong", expected.getBirthday(), actual.getBirthday());
    }
}
