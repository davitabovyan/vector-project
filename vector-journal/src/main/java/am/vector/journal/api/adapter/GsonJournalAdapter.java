package am.vector.journal.api.adapter;

import am.vector.common.adapter.GsonAdapter;
import am.vector.common.constants.EntityObject;
import am.vector.common.model.journal.AccountGroupModel;
import am.vector.common.model.journal.AccountModel;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GsonJournalAdapter extends GsonAdapter {

    public GsonJournalAdapter() {
        addToMap(EntityObject.ACCOUNT);
        addToMap(EntityObject.ACCOUNT_GROUP);
        setup();
    }

    protected void registerAdapter(GsonBuilder builder, EntityObject objCode, Map<String, String> fields){
        switch (objCode) {
            case ACCOUNT:       builder.registerTypeAdapter(AccountModel.class, new AccountAdapter(fields));
                break;
            case ACCOUNT_GROUP: builder.registerTypeAdapter(AccountGroupModel.class, new AccountGroupAdapter(fields));
                break;
        }
    }
}
