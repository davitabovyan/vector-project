package am.vector.payroll.api.service.impl.jpa.main;

import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Employment;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.repository.main.EmploymentRepository;
import am.vector.payroll.api.service.EmploymentService;
import am.vector.payroll.api.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class EmploymentServiceJPA implements EmploymentService {

    private final EmploymentRepository employmentRepository;
    private final PersonService personService;

    public Employment findById(long id){
        Employment employment = employmentRepository.findById(id).orElseThrow(
                ()-> new EntityNotFoundException(Employment.class, GeneralUtil.encodeUuid(id))
            );
        Person person = personService.findById(employment.getPerson().getId());
        employment.setPerson(person);
//        employment.setPerson((Person) Hibernate.unproxy(person));
        return employment;
    }

    public List<Employment> findAll(boolean includeTerminated){
        List<Employment> entities;
        if(includeTerminated){
            entities = employmentRepository.findAll(GeneralUtil.getCompanyId());
        } else {
            entities = employmentRepository.findAllCurrent(GeneralUtil.getCompanyId());
        }
        return entities;
    }

    public List<Employment> findAllPerPerson(long personId){
        return employmentRepository.findAllPerPerson(new Person(personId));
    }

    @Transactional
    @Override
    public void update(Employment object){
        GeneralUtil.setRecordStatus(object);
        employmentRepository.save(object);
    }

    @Transactional
    @Override
    public Employment save(Employment object){
        return employmentRepository.save(object);
    }

    @Override
    public List<Employment> saveAll(List<Employment> employments) {
        return employmentRepository.saveAll(employments);
    }

    @Override
    public List<Employment> findAllById(List<Long> id) {
        return employmentRepository.findAllById(id);
    }

    @Override
    public void deleteById(long id) {
        employmentRepository.deleteById(id);
    }

    @Override
    public void deleteAll(int companyId) {
        employmentRepository.deleteAll(companyId);
        logger.info(String.format("All record are delete by user %s", GeneralUtil.getUserFromContext().getUsername()));
    }
}
