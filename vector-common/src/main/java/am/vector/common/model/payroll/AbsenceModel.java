package am.vector.common.model.payroll;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.LeaveType;
import am.vector.common.model.MainModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Accessors(chain = true) @Getter @Setter
public class AbsenceModel extends MainModel {

    private LeaveType leaveType;
    private Integer amount;
    private String period;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-M-d")
    private LocalDate startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-M-d")
    private LocalDate endDate;
    private Boolean isParent;
    private String parentId;
    private PersonModel person;

    public void assignParentId(String parentId) {
        this.parentId = parentId;
    }

    public void assignLeaveType(String leaveType) {
        this.leaveType = leaveType != null ? LeaveType.valueOf(leaveType) : null;
    }

    public boolean isNotParent(){
        return !isParent();
    }

    public boolean isParent(){
        return isParent != null && isParent;
    }

    public AbsenceModel() {
        super(EntityObject.ABSENCE);
    }

    public AbsenceModel(String uuid) {
        super(EntityObject.ABSENCE, uuid);
    }

    public AbsenceModel copy(){
        return (AbsenceModel) super.copy(this);
    }
}
