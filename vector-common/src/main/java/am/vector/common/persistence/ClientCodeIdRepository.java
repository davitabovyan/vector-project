package am.vector.common.persistence;

import java.util.List;

public interface ClientCodeIdRepository<T> extends BaseRepository<T, Long>{
    List<T> findAllPerCompany(long start, long end);
    void deleteAll(long start, long end);
    List<T> findAllByIdIn(List<Long> ids);
}
