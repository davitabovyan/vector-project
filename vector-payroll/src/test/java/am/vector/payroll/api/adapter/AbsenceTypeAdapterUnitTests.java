package am.vector.payroll.api.adapter;

import am.vector.common.constants.LeaveType;
import am.vector.payroll.api.entity.main.Absence;
import am.vector.payroll.api.entity.main.Person;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.time.LocalDate;
import java.util.Collections;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class AbsenceTypeAdapterUnitTests extends BaseTypeAdapterUnitTest<AbsenceTypeAdapter, Absence> {

    private static Absence absence;
    private AbsenceTypeAdapter adapter;

    @BeforeClass
    public static void setup(){
        absence = new Absence()
            .setLeaveType(LeaveType.VACATION)
            .setAmount(7_000L)
            .setPeriod("1018")
            .setIsParent(false)
            .setStartDate(LocalDate.parse("2018-10-13"))
            .setEndDate(LocalDate.parse("2018-10-27"))
            .setPerson(new Person());
    }

    @Before
    public void beforeTest() throws Exception {
        adapter = new AbsenceTypeAdapter();
        prepare(Collections.singletonList(Absence.class), adapter, absence, Collections.emptySet());
    }

    @Test
    public void write_OnlySelectedFieldsAreReturned() throws Exception {
        new AbsenceTypeAdapter("amount,period").write(writer, absence);

        verify(writer).name("amount");
        verify(writer).name("period");
        verify(writer, never()).name("isParent");
    }

    @Test
    public void write_correctValuesAreReturned() throws Exception {
        adapter.write(writer, absence);

        verify(writer).value(ArgumentMatchers.eq(absence.getLeaveType().name()));
        verify(writer).value(ArgumentMatchers.eq(absence.getAmount()));
        verify(writer).value(ArgumentMatchers.eq(absence.getPeriod()));
        verify(writer).value(ArgumentMatchers.eq(absence.getStartDate().toString()));
        verify(writer).value(ArgumentMatchers.eq(absence.getEndDate().toString()));
        verify(writer).value(ArgumentMatchers.eq(absence.isParent()));
//        verify(writer).value(ArgumentMatchers.eq(absence.getParentId()));
    }
}
