package am.vector.payroll.api.v1.unit;

import am.vector.payroll.api.v1.unit.service.impl.jpa.ServiceUnitTestSuit;
import am.vector.payroll.api.v1.unit.util.UtilTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ServiceUnitTestSuit.class,
        UtilTestSuite.class
})
public class UnitTestSuite {
}
