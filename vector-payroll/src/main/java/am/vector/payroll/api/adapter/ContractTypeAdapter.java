package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.payroll.api.entity.main.Contract;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class ContractTypeAdapter extends CustomAdapter<Contract> {

    public ContractTypeAdapter(String fields){
        super(Contract.class, fields);
    }

    public ContractTypeAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, Contract contract, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("salary")) jsonWriter.name("salary").value(contract.getSalary());
        if(all || fields.containsKey("dailyHours")) jsonWriter.name("dailyHours").value(contract.getDailyHours());
        if(all || fields.containsKey("startDate")) parseDate("startDate", jsonWriter, contract.getStartDate());
        if(all || fields.containsKey("endDate")) parseDate("endDate", jsonWriter, contract.getEndDate());
        if(all || fields.containsKey("laborRelation")) jsonWriter.name("laborRelation").value(contract.isLaborRelation());
        if(all || fields.containsKey("workWeek")) jsonWriter.name("workWeek").value(contract.getWorkWeek());

        if(all || fields.containsKey("person")){
            jsonWriter.name("person");
            new PersonTypeAdapter(fields.getOrDefault("person", null))
                    .write(jsonWriter, contract.getPerson());
        }

        if(all || fields.containsKey("role")){
            jsonWriter.name("role");
            new RoleTypeAdapter(fields.getOrDefault("role", null))
                    .write(jsonWriter, contract.getRole());
        }

        if(all || fields.containsKey("department")){
            jsonWriter.name("department");
            new DepartmentTypeAdapter(fields.getOrDefault("department", null))
                    .write(jsonWriter, contract.getDepartment());
        }

        return jsonWriter;
    }
}
