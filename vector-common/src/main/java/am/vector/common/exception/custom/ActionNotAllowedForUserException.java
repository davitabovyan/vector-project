package am.vector.common.exception.custom;

import am.vector.common.constants.EntityObject;
import org.springframework.http.HttpMethod;

public class ActionNotAllowedForUserException extends ProvidedJsonException {
    public ActionNotAllowedForUserException(EntityObject entityObject, HttpMethod method) {
        super(String.format("%s,%s",entityObject.getCode(), method));
    }
    public ActionNotAllowedForUserException(EntityObject entityObject, String httpMethod) {
        super(String.format("%s,%s",entityObject.getCode(), httpMethod));
    }
}
