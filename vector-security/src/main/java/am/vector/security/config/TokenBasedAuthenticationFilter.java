package am.vector.security.config;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import am.vector.common.model.admin.UserModel;
import am.vector.common.config.security.SecurityConfig;
import am.vector.common.util.GeneralUtil;
import com.google.gson.Gson;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.apache.http.entity.ContentType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class TokenBasedAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	TokenBasedAuthenticationFilter(AuthenticationManager authenticationManager) {
		setAuthenticationManager(authenticationManager);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
											Authentication authentication) throws IOException {
		UserModel user = (UserModel) authentication.getPrincipal();
		long expire = System.currentTimeMillis() + SecurityConfig.TOKEN_LIFETIME;
		String token = Jwts.builder()
			.setId(UUID.randomUUID().toString())
			.claim(SecurityConfig.IS_SUPERADMIN, user.isSuperAdmin())
			.claim(SecurityConfig.IS_ADMIN, user.isAdmin())
			.claim(SecurityConfig.READ, user.getReadAccess())
			.claim(SecurityConfig.EDIT, user.getEditAccess())
			.claim(SecurityConfig.LOCALE, user.getLocale())
			.claim(SecurityConfig.COMPANY, GeneralUtil.encodeUuid(user.getCompanyId()))
			.setSubject(user.getUsername())
			.setExpiration(new Date(expire))
			.signWith(Keys.hmacShaKeyFor(SecurityConfig.TOKEN_SECRET.getBytes()), SignatureAlgorithm.HS256)
			.compact();
		response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
		Map<String, Object> object = new ConcurrentHashMap<>();
		object.put("token", token);
		object.put("expires", expire);
		response.getWriter().write(new Gson().toJson(object));
		response.getWriter().flush();

	}
}
