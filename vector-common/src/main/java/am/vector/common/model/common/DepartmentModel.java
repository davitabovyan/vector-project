package am.vector.common.model.common;

import am.vector.common.constants.EntityObject;
import am.vector.common.model.MainModel;
import am.vector.common.model.payroll.PersonModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true) @Getter @Setter
public class DepartmentModel extends MainModel {

    private String code;
    private String name;
    private String costCenter;
    private PersonModel head;

    public DepartmentModel() {
        super(EntityObject.DEPARTMENT);
    }

    public DepartmentModel(String uuid) {
        super(EntityObject.DEPARTMENT, uuid);
    }

    public DepartmentModel copy(){
        return (DepartmentModel) super.copy(this);
    }
}
