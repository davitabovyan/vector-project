package am.vector.common.exception;

public class VectorException extends RuntimeException {
	public VectorException(String message) {
		super(message);
	}
	public VectorException(String... keys) {
		super(String.join(",",keys));
	}
	public VectorException(String message, Throwable cause) {
		super(message, cause);
	}
}