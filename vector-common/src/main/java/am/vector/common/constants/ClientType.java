package am.vector.common.constants;

public enum ClientType {
    FREE,
    PAID,
    PRIME
}
