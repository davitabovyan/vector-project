package am.vector.security.entity;

import am.vector.common.constants.ClientType;
import am.vector.common.constants.EntityObject;
import am.vector.common.model.admin.ClientModel;
import am.vector.security.converter.HashMapConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Map;

@Entity
@Table(name = "client")
@Accessors(chain = true) @Getter @Setter
@NoArgsConstructor
public class Client {

    transient private static final EntityObject OBJ = EntityObject.CLIENT;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String email;
    private String cell;
    private String name;
    private boolean state;
    private long balance;

    @Enumerated(EnumType.STRING)
    private ClientType type;

    @Convert(converter = HashMapConverter.class)
    private Map<String, Integer> companies;

    public static ClientModel toModel(Client client) {
        return new ClientModel()
                .setId(client.getId())
                .setEmail(client.getEmail())
                .setCell(client.getCell())
                .setName(client.getName())
                .setState(client.isState())
                .setType(client.getType())
                .setBalance(client.getBalance())
                .setCompanies(client.getCompanies());
    }

    public static Client fromModel(ClientModel model) {

        return new Client()
                .setId(model.getId())
                .setEmail(model.getEmail())
                .setCell(model.getCell())
                .setName(model.getName())
                .setState(model.getState())
                .setType(model.getType())
                .setBalance(model.getBalance())
                .setCompanies(model.getCompanies());
    }

}
