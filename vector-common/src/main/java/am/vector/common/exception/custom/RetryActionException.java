package am.vector.common.exception.custom;

public class RetryActionException extends ProvidedJsonException {
    public RetryActionException() {
        this("");
    }
    public RetryActionException(String message) {
        super(message);
    }
}
