package am.vector.common.model.admin;

import am.vector.common.constants.ClientType;
import am.vector.common.constants.EntityObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Map;

@Accessors(chain = true) @Getter @Setter
@NoArgsConstructor
public class ClientModel {

    transient private static final EntityObject OBJ = EntityObject.CLIENT;

    private Integer id;
    private String email;
    private String name;
    private String cell;
    private Boolean state;
    private Long balance;
    private ClientType type;
    private Map<String, Integer> companies;
}
