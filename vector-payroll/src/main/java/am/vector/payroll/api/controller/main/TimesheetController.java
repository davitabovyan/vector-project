package am.vector.payroll.api.controller.main;

import am.vector.common.adapter.GsonAdapter;
import am.vector.common.model.payroll.TimeSheetModel;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.IOUtil;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.dto.timesheet.MainTimesheetDto;
import am.vector.payroll.api.entity.main.TimeSheet;
import am.vector.payroll.api.service.TimesheetService;
import am.vector.payroll.util.Mapper;
import com.google.gson.reflect.TypeToken;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/timesheet", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class TimesheetController {

    private final TimesheetService timesheetService;
    private final GsonPayrollAdapter adapter;

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findByIdCompact(@PathVariable String uuid,
                                          @RequestParam(value = "fields", required = false) String fields){
        TimeSheet response;
        if(IOUtil.parseFields(TimeSheet.class, fields).containsKey("person")) {
            response = timesheetService.getTimesheetWithPerson(GeneralUtil.decodeUuid(uuid));
        } else {
            response = timesheetService.findById(GeneralUtil.decodeUuid(uuid));
        }
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @GetMapping("/byPeriod/{period}")
    public ResponseEntity<String> getTimesheetsByPeriod(@PathVariable("period") String period,
                                         @RequestParam(value = "departmentId", required = false) String departmentId){
        Iterable<MainTimesheetDto> response;
        if(null != departmentId) {
            response = timesheetService.getTimesheetsByPeriod(period, GeneralUtil.decodeUuid(departmentId));
        } else {
            response = timesheetService.getTimesheetsByPeriod(period, 0);
        }
        return ResponseEntity.ok(GsonAdapter.GSON.toJson(response, new TypeToken<List<MainTimesheetDto>>(){}.getType())
        );
    }

    @PostMapping
    public ResponseEntity<Map<String,Object>> create(@RequestBody TimeSheetModel model) {
        model.setCompanyId(GeneralUtil.getCompanyId());
        TimeSheet entity = timesheetService.save(Mapper.map(model));
        return new ResponseEntity<>(GeneralUtil.uuidJSON(entity), HttpStatus.CREATED);
    }

    @PutMapping("/{uuid}")
    public void update(@RequestBody TimeSheetModel model, @PathVariable String uuid) {
        model.setCompanyId(GeneralUtil.getCompanyId());
        timesheetService.update(Mapper.map(model));
    }

    @PostMapping("/calculate/{period}")
    public void calculate(@PathVariable String period, @RequestParam(value = "departmentId", required = false) String departmentId) {
        if(null == departmentId){
            timesheetService.generate(GeneralUtil.getCompanyId(), period, 0);
        } else {
            timesheetService.generate(GeneralUtil.getCompanyId(), period, GeneralUtil.decodeUuid(departmentId));
        }
    }

    @PostMapping("/calculate/{period}/{contractId}")
    public void calculateForContract(@PathVariable String period, @PathVariable String contractId) {
            timesheetService.generateForContract(GeneralUtil.getCompanyId(), GeneralUtil.decodeUuid(contractId), period);
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody TimeSheetModel updated, @PathVariable String uuid) {
        TimeSheet entity = timesheetService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(updated));
        timesheetService.update(entity);
    }

    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable String uuid) {
        timesheetService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
