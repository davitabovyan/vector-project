package am.vector.common.model.payroll;

import am.vector.common.constants.EntityObject;
import am.vector.common.model.MainModel;
import am.vector.common.model.common.DepartmentModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Accessors(chain = true) @Getter @Setter
public class ContractModel extends MainModel {

    private Integer salary;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-M-d")
    private LocalDate startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-M-d")
    private LocalDate endDate;
    private Integer dailyHours;
    private Boolean laborRelation;
    private Integer workWeek;
    private PersonModel person;
    private RoleModel role;
    private DepartmentModel department;

    public boolean isLaborRelation(){
        return null != laborRelation && laborRelation;
    }

    public ContractModel() {
        super(EntityObject.CONTRACT);
    }

    public ContractModel(String uuid) {
        super(EntityObject.CONTRACT, uuid);
    }

    public ContractModel copy(){
        return (ContractModel) super.copy(this);
    }

    public boolean isFiveWorkDay() {
        return workWeek == 5;
    }
}
