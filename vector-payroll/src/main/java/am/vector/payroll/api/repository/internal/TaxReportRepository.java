package am.vector.payroll.api.repository.internal;

import am.vector.payroll.api.entity.internal.TaxReport;
import am.vector.payroll.api.entity.internal.TaxReportId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TaxReportRepository extends CrudRepository<TaxReport, TaxReportId> {
    @Transactional
    @Modifying
    @Query("DELETE FROM TaxReport t where t.companyId = :companyId AND t.period BETWEEN :periodStart AND :periodEnd")
    void deleteAllForPeriodsInterval(@Param("companyId") int companyId, @Param("periodStart") int periodStart, @Param("periodEnd") int periodEnd);
}
