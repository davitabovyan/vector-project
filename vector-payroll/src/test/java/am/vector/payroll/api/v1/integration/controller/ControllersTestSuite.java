package am.vector.payroll.api.v1.integration.controller;

import am.vector.payroll.api.v1.integration.controller.cases.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        PersonControllerIntegrationTests.class,
        DepartmentControllerIntegrationTests.class,
        RoleControllerIntegrationTests.class,
        ContractControllerIntegrationTests.class,
        EmploymentControllerIntegrationTests.class,
        AbsenceControllerIntegrationTests.class,
        PayslipControllerIntegrationTests.class,
        TimeSheetControllerIntegrationTests.class
})
public class ControllersTestSuite extends ControllerBaseTest {
}
