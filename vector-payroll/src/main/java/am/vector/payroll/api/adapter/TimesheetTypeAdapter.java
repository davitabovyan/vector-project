package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.payroll.api.entity.main.TimeSheet;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class TimesheetTypeAdapter extends CustomAdapter<TimeSheet> {

    public TimesheetTypeAdapter(String fields){
        super(TimeSheet.class, fields);
    }

    public TimesheetTypeAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, TimeSheet timesheet, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("period")) jsonWriter.name("period").value(timesheet.getPeriod());
        if(all || fields.containsKey("total")) jsonWriter.name("total").value(timesheet.getTotal());
        if(all || fields.containsKey("day1")) jsonWriter.name("day1").value(timesheet.getDay1());
        if(all || fields.containsKey("day2")) jsonWriter.name("day2").value(timesheet.getDay2());
        if(all || fields.containsKey("day3")) jsonWriter.name("day3").value(timesheet.getDay3());
        if(all || fields.containsKey("day4")) jsonWriter.name("day4").value(timesheet.getDay4());
        if(all || fields.containsKey("day5")) jsonWriter.name("day5").value(timesheet.getDay5());
        if(all || fields.containsKey("day6")) jsonWriter.name("day6").value(timesheet.getDay6());
        if(all || fields.containsKey("day7")) jsonWriter.name("day7").value(timesheet.getDay7());
        if(all || fields.containsKey("day8")) jsonWriter.name("day8").value(timesheet.getDay8());
        if(all || fields.containsKey("day9")) jsonWriter.name("day9").value(timesheet.getDay9());
        if(all || fields.containsKey("day10")) jsonWriter.name("day10").value(timesheet.getDay10());
        if(all || fields.containsKey("day11")) jsonWriter.name("day11").value(timesheet.getDay11());
        if(all || fields.containsKey("day12")) jsonWriter.name("day12").value(timesheet.getDay12());
        if(all || fields.containsKey("day13")) jsonWriter.name("day13").value(timesheet.getDay13());
        if(all || fields.containsKey("day14")) jsonWriter.name("day14").value(timesheet.getDay14());
        if(all || fields.containsKey("day15")) jsonWriter.name("day15").value(timesheet.getDay15());
        if(all || fields.containsKey("day16")) jsonWriter.name("day16").value(timesheet.getDay16());
        if(all || fields.containsKey("day17")) jsonWriter.name("day17").value(timesheet.getDay17());
        if(all || fields.containsKey("day18")) jsonWriter.name("day18").value(timesheet.getDay18());
        if(all || fields.containsKey("day19")) jsonWriter.name("day19").value(timesheet.getDay19());
        if(all || fields.containsKey("day20")) jsonWriter.name("day20").value(timesheet.getDay20());
        if(all || fields.containsKey("day21")) jsonWriter.name("day21").value(timesheet.getDay21());
        if(all || fields.containsKey("day22")) jsonWriter.name("day22").value(timesheet.getDay22());
        if(all || fields.containsKey("day23")) jsonWriter.name("day23").value(timesheet.getDay23());
        if(all || fields.containsKey("day24")) jsonWriter.name("day24").value(timesheet.getDay24());
        if(all || fields.containsKey("day25")) jsonWriter.name("day25").value(timesheet.getDay25());
        if(all || fields.containsKey("day26")) jsonWriter.name("day26").value(timesheet.getDay26());
        if(all || fields.containsKey("day27")) jsonWriter.name("day27").value(timesheet.getDay27());
        if(all || fields.containsKey("day28")) jsonWriter.name("day28").value(timesheet.getDay28());
        if(all || fields.containsKey("day29")) jsonWriter.name("day29").value(timesheet.getDay29());
        if(all || fields.containsKey("day30")) jsonWriter.name("day30").value(timesheet.getDay30());
        if(all || fields.containsKey("day31")) jsonWriter.name("day31").value(timesheet.getDay31());

        if(all || fields.containsKey("contract")){
            jsonWriter.name("contract");
            new ContractTypeAdapter(fields.getOrDefault("contract", null))
                    .write(jsonWriter, timesheet.getContract());
        }

        return jsonWriter;
    }
}
