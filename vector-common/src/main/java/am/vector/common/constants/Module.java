package am.vector.common.constants;

public enum Module {
    SECURITY("8443"),
    PAYROLL("8444"),
    JOURNAL("8445");

    private String port;

    Module(String port) {
        this.port = port;
    }

    public String getPort() {
        return this.port;
    }
}
