package am.vector.payroll.api.adapter;

import am.vector.payroll.api.entity.main.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class TimeSheetTypeAdapterUnitTests extends BaseTypeAdapterUnitTest<TimesheetTypeAdapter, TimeSheet> {

    private static TimeSheet timeSheet;
    private TimesheetTypeAdapter adapter;

    @BeforeClass
    public static void setup(){
        timeSheet = new TimeSheet()
            .setPeriod("1018")
            .setDay1(8)
            .setDay2(8)
            .setDay3(8)
            .setDay4(8)
            .setDay5(8)
            .setDay6(0)
            .setDay7(0)
            .setDay8(8)
            .setDay9(8)
            .setDay10(8)
            .setDay11(8)
            .setDay12(8)
            .setDay13(0)
            .setDay14(0)
            .setDay15(8)
            .setDay16(8)
            .setDay17(8)
            .setDay18(8)
            .setDay19(8)
            .setDay20(0)
            .setDay21(0)
            .setDay22(8)
            .setDay23(8)
            .setDay24(8)
            .setDay25(8)
            .setDay26(8)
            .setDay27(0)
            .setDay28(0)
            .setDay29(8)
            .setDay30(8)
            .setDay31(8)
            .setTotal(184)
            .setContract(new Contract());
    }

    @Before
    public void beforeTest() throws Exception {
        adapter = new TimesheetTypeAdapter();
        prepare(Arrays.asList(TimeSheet.class, Contract.class, Person.class, Role.class, Department.class), adapter,
            timeSheet, Collections.emptySet());
    }

    @Test
    public void write_OnlySelectedFieldsAreReturned() throws Exception {
        new TimesheetTypeAdapter("total,period").write(writer, timeSheet);

        verify(writer).name("total");
        verify(writer).name("period");
        verify(writer, never()).name("day1");
    }

    @Test
    public void write_correctValuesAreReturned() throws Exception {
        adapter.write(writer, timeSheet);

        verify(writer).value(ArgumentMatchers.eq(timeSheet.getPeriod()));
        verify(writer).value(ArgumentMatchers.eq(timeSheet.getTotal()));
        verify(writer,times(23)).value(ArgumentMatchers.eq(Integer.valueOf(8)));
        verify(writer, times(8)).value(ArgumentMatchers.eq(Integer.valueOf(0)));
    }
}
