package am.vector.payroll.api.service.impl.jpa.main;

import am.vector.common.constants.LeaveType;
import am.vector.common.exception.VectorException;
import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.CalculationUtil;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.dto.timesheet.MainTimesheetDto;
import am.vector.payroll.api.entity.main.*;
import am.vector.payroll.api.repository.main.TimesheetRepository;
import am.vector.payroll.api.service.AbsenceService;
import am.vector.payroll.api.service.ContractService;
import am.vector.payroll.api.service.TimesheetService;
import am.vector.payroll.api.support.PayrollPeriod;
import am.vector.payroll.util.DayUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class TimesheetServiceJPA implements TimesheetService {

    private final TimesheetRepository timesheetRepository;
    private final AbsenceService absenceService;
    private final ContractService contractService;


    public List<MainTimesheetDto> getTimesheetsByPeriod(String period, long departmentId){
        if(departmentId == 0) {
            return timesheetRepository.getTimesheetsByPeriod(period, GeneralUtil.getCompanyId());
        } else {
            return timesheetRepository.getTimesheetsByPeriodByDepartment(period, GeneralUtil.getCompanyId(), new Department(departmentId));
        }
    }

    public TimeSheet getTimesheetWithPerson(long id){
        return timesheetRepository.getTimesheetWithPerson(new Person(id)).orElseThrow(
            ()-> new EntityNotFoundException(TimeSheet.class, GeneralUtil.encodeUuid(id))
        );
    }

    public TimeSheet findById(long id){
        return timesheetRepository.findById(id).orElseThrow(
            ()-> new EntityNotFoundException(TimeSheet.class, GeneralUtil.encodeUuid(id))
        );
    }

    public List<TimeSheet> findAllPerCompany(){
        int companyId = GeneralUtil.getCompanyId();
        return timesheetRepository.findAllPerCompany(companyId);
    }

    public TimeSheet generateForContract(int companyId, long contractId, String periodString){
        PayrollPeriod period = DayUtil.parsePeriod(periodString);
        TimeSheet timeSheet = generateDefaultForContract(companyId, contractId, period);
        long personId = timeSheet.getContract().getPerson().getId();
        List<Absence> absencesForPerson = absenceService.findByPersonByPeriod(personId, period.getPeriodString());
        List<Absence> absencePerPerson = new ArrayList<>(absencesForPerson);
        applyAbsencesToTimeSheet(timeSheet, absencePerPerson);
        timesheetRepository.deleteByPeriodByContract(periodString, companyId, new Contract(contractId));
        return timesheetRepository.save(timeSheet);
    }

    @Transactional
    public List<TimeSheet> generate(int companyId, String periodString, long departmentId){
        PayrollPeriod period = DayUtil.parsePeriod(periodString);
        List<TimeSheet> timeSheets = generateDefaultsForPeriod(companyId, period, departmentId);
        Map<Long, List<Absence>> absences = absenceService.findAllByCompanyIdAndPeriod(companyId, periodString);
        for(TimeSheet timeSheet : timeSheets){
            long personId = timeSheet.getContract().getPerson().getId();
            if(absences.containsKey(personId)){
                List<Absence> absencePerPerson = absences.get(personId);
                applyAbsencesToTimeSheet(timeSheet, absencePerPerson);
            }
        }
        timesheetRepository.deleteByPeriod(periodString, companyId);
        return timesheetRepository.saveAll(timeSheets);
    }

    @Transactional
    @Override
    public void update(TimeSheet object){
        GeneralUtil.setRecordStatus(object);
        timesheetRepository.save(object);
    }
    @Transactional
    @Override
    public TimeSheet save(TimeSheet object){
        return timesheetRepository.save(object);
    }

    @Override
    public List<TimeSheet> saveAll(List<TimeSheet> timeSheets) {
        return timesheetRepository.saveAll(timeSheets);
    }

    @Override
    public List<TimeSheet> findAllById(List<Long> id) {
        return timesheetRepository.findAllById(id);
    }

    @Override
    public void deleteById(long id) {
        timesheetRepository.deleteById(id);
    }

    @Override
    public void deleteAll(int companyId) {
        timesheetRepository.deleteAll(companyId);
        logger.info(String.format("All records are delete by user %s", GeneralUtil.getUserFromContext().getUsername()));
    }

    private TimeSheet generateDefaultForContract(int companyId, long contractId, PayrollPeriod period){
        Contract contract = contractService.findById(contractId);
        return createDefaultTimeSheet(companyId, contract, period);
    }

    private List<TimeSheet> generateDefaultsForPeriod(int companyId, PayrollPeriod period, long departmentId){
        Iterable<Contract> allCurrentContracts = contractService.getAllCurrents(companyId, period, departmentId);
        List<TimeSheet> defaultsForPeriod = new ArrayList<>();
        for(Contract contract : allCurrentContracts ){
            TimeSheet timeSheet = createDefaultTimeSheet(companyId, contract, period);
            defaultsForPeriod.add(timeSheet);
        }
        return defaultsForPeriod;
    }

    private TimeSheet createDefaultTimeSheet(int companyId, Contract contract, PayrollPeriod period){
        TimeSheet timeSheet = new TimeSheet()
                .setContract(contract)
                .setPeriod(period.getPeriodString());
        timeSheet.setCompanyId(companyId);
        int workHoursPerDay = contract.getDailyHours();
        LocalDate current = period.getFirstDay();
        LocalDate lastDay = period.getLastDay();
        while(!current.isAfter(lastDay)) {
            int dayOfWeek = current.getDayOfWeek().getValue();
            if (current.isBefore(contract.getStartDate())) {
                setWorkHoursForDay(current, timeSheet, LeaveType.EMPLOYMENT_NOT_STARTED.getCode());
                current = current.plusDays(1);
                continue;
            }
            if (dayOfWeek > contract.getWorkWeek()) {
                setWorkHoursForDay(current, timeSheet, LeaveType.WEEKEND.getCode());
                current = current.plusDays(1);
                continue;
            }
            if (CalculationUtil.isHoliday(current)) {
                setWorkHoursForDay(current, timeSheet, LeaveType.BANK_HOLIDAY.getCode());
                current = current.plusDays(1);
                continue;
            }
            setWorkHoursForDay(current, timeSheet, workHoursPerDay);
            current = current.plusDays(1);
        }
        return timeSheet;
    }

    private void applyAbsencesToTimeSheet(TimeSheet timeSheet, List<Absence> absences){
        for(Absence absence : absences){
            LocalDate date = absence.getStartDate();
            while(!date.isAfter(absence.getEndDate())){
                setWorkHoursForDay(date, timeSheet, absence.getLeaveType().getCode());
                date = date.plusDays(1);
            }
        }
    }

    private void setWorkHoursForDay(LocalDate date, TimeSheet timeSheet, int workHoursPerDay){
        switch (date.getDayOfMonth()){
            case 1: timeSheet.setDay1(workHoursPerDay); break;
            case 2: timeSheet.setDay2(workHoursPerDay); break;
            case 3: timeSheet.setDay3(workHoursPerDay); break;
            case 4: timeSheet.setDay4(workHoursPerDay); break;
            case 5: timeSheet.setDay5(workHoursPerDay); break;
            case 6: timeSheet.setDay6(workHoursPerDay); break;
            case 7: timeSheet.setDay7(workHoursPerDay); break;
            case 8: timeSheet.setDay8(workHoursPerDay); break;
            case 9: timeSheet.setDay9(workHoursPerDay); break;
            case 10: timeSheet.setDay10(workHoursPerDay); break;
            case 11: timeSheet.setDay11(workHoursPerDay); break;
            case 12: timeSheet.setDay12(workHoursPerDay); break;
            case 13: timeSheet.setDay13(workHoursPerDay); break;
            case 14: timeSheet.setDay14(workHoursPerDay); break;
            case 15: timeSheet.setDay15(workHoursPerDay); break;
            case 16: timeSheet.setDay16(workHoursPerDay); break;
            case 17: timeSheet.setDay17(workHoursPerDay); break;
            case 18: timeSheet.setDay18(workHoursPerDay); break;
            case 19: timeSheet.setDay19(workHoursPerDay); break;
            case 20: timeSheet.setDay20(workHoursPerDay); break;
            case 21: timeSheet.setDay21(workHoursPerDay); break;
            case 22: timeSheet.setDay22(workHoursPerDay); break;
            case 23: timeSheet.setDay23(workHoursPerDay); break;
            case 24: timeSheet.setDay24(workHoursPerDay); break;
            case 25: timeSheet.setDay25(workHoursPerDay); break;
            case 26: timeSheet.setDay26(workHoursPerDay); break;
            case 27: timeSheet.setDay27(workHoursPerDay); break;
            case 28: timeSheet.setDay28(workHoursPerDay); break;
            case 29: timeSheet.setDay29(workHoursPerDay); break;
            case 30: timeSheet.setDay30(workHoursPerDay); break;
            case 31: timeSheet.setDay31(workHoursPerDay); break;
            default: throw new VectorException(String.format("Provided date is not in correct format: %s", date.toString()));
        }
    }
}
