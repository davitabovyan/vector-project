package am.vector.payroll.api.v1.integration.controller.cases;

import am.vector.common.constants.RecordState;
import am.vector.payroll.PayrollApplication;
import am.vector.payroll.api.entity.main.*;
import am.vector.payroll.api.v1.integration.controller.ControllerBaseTest;
import am.vector.payroll.util.EntityBuilder;
import am.vector.payroll.util.PayrollApi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PayrollApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TimeSheetControllerIntegrationTests extends ControllerBaseTest {

    private TimeSheet timeSheetOne;
    private Person person;
    private Role role;
    private Department department;

    @Before
    public void setUp(){
        person = create(EntityBuilder.getPersonOne());
        role = create(EntityBuilder.getRoleOne());
        department = create(EntityBuilder.getDepartmentOne().setHead(person));
        Contract contract = create(EntityBuilder.getContractOne().setRole(role).setDepartment(department).setPerson(person));
        timeSheetOne = EntityBuilder.getTimesheetOne().setContract(contract);
    }

    @Test
    public void createTimesheet_success() {
        // Setup
        TimeSheet timesheet = timeSheetOne.copy();
        // Assert
        testCreate(timesheet, adapter.toJson(timesheet));
    }

    @Test
    public void createTimesheetDetails_success() {
        // Setup
        TimeSheet timesheet = timeSheetOne.copy();
        // Act
        TimeSheet response = create(timesheet);
        // Assert
        compareExpectedWithActual(timesheet, response);
    }
    @Test
    public void getTimesheet_success() {
        // Setup
        TimeSheet timesheet = timeSheetOne.copy();
        long id = create(timesheet).getId();
        // Act
        TimeSheet result = getTimesheet(id);
        // Assert
        compareExpectedWithActual(timesheet, result);
    }

    @Test
    public void updateTimesheet_success() {
        // Setup
        Contract contract = create(EntityBuilder.getContractOne().setRole(role).setDepartment(department).setPerson(person));
        TimeSheet timesheet = timeSheetOne.copy();
        long id = create(timesheet).getId();
        timesheet
            .setContract(contract)
            .setPeriod("1118")
            .setDay1(2).setDay2(2).setDay3(2).setDay4(2).setDay5(2).setDay6(2).setDay7(2).setDay8(2).setDay9(2).setDay10(2)
            .setDay11(2).setDay12(2).setDay13(2).setDay14(2).setDay15(2).setDay16(2).setDay17(2).setDay18(2).setDay19(2)
            .setDay20(2).setDay21(2).setDay22(2).setDay23(2).setDay24(2).setDay25(2).setDay26(2).setDay27(2).setDay28(2).setDay29(2)
            .setDay30(2).setDay31(2);
        timesheet.setId(id);
        timesheet.setState(RecordState.CURRENT);

        // Act
        ResponseEntity<String> response = update(timesheet);

        // Assert
        assertEquals("Response status on Timesheet update isn't correct", 200, response.getStatusCodeValue());
        TimeSheet actual = getTimesheet(id);
        compareExpectedWithActual(timesheet, actual);
    }

    @Test
    public void editTimesheetSingleField_success() {
        // Setup
        TimeSheet timesheet = create(timeSheetOne.copy());
        // Act
        HttpStatus responseStatus = PayrollApi.updateFields(
            getHeaders(), getHostAndPort(), "{\"period\":\"1118\"}", timesheet.getId(), timesheet.getObjCode()).getStatusCode();
        // Assert
        assertEquals("Response status on Timesheet update isn't correct", HttpStatus.OK, responseStatus);
        TimeSheet actual = getTimesheet(timesheet.getId());
        compareExpectedWithActual(timesheet.setPeriod("1118"), actual);
    }

    private void compareExpectedWithActual(TimeSheet expected, TimeSheet actual){
        assertEquals("Timesheet period isn't saved/retrieved correctly", expected.getPeriod(), actual.getPeriod());
        assertEquals("Timesheet total isn't saved/retrieved correctly", expected.getTotal(), actual.getTotal());
        assertEquals("Timesheet contract isn't saved/retrieved correctly", expected.getContract().getId(), actual.getContract().getId());
        assertEquals("Timesheet day1 isn't saved/retrieved correctly", expected.getDay1(), actual.getDay1());
        assertEquals("Timesheet day2 isn't saved/retrieved correctly", expected.getDay2(), actual.getDay2());
        assertEquals("Timesheet day3 isn't saved/retrieved correctly", expected.getDay3(), actual.getDay3());
        assertEquals("Timesheet day4 isn't saved/retrieved correctly", expected.getDay4(), actual.getDay4());
        assertEquals("Timesheet day5 isn't saved/retrieved correctly", expected.getDay5(), actual.getDay5());
        assertEquals("Timesheet day6 isn't saved/retrieved correctly", expected.getDay6(), actual.getDay6());
        assertEquals("Timesheet day7 isn't saved/retrieved correctly", expected.getDay7(), actual.getDay7());
        assertEquals("Timesheet day8 isn't saved/retrieved correctly", expected.getDay8(), actual.getDay8());
        assertEquals("Timesheet day9 isn't saved/retrieved correctly", expected.getDay9(), actual.getDay9());
        assertEquals("Timesheet day10 isn't saved/retrieved correctly", expected.getDay10(), actual.getDay10());
        assertEquals("Timesheet day11 isn't saved/retrieved correctly", expected.getDay11(), actual.getDay11());
        assertEquals("Timesheet day12 isn't saved/retrieved correctly", expected.getDay12(), actual.getDay12());
        assertEquals("Timesheet day13 isn't saved/retrieved correctly", expected.getDay13(), actual.getDay13());
        assertEquals("Timesheet day14 isn't saved/retrieved correctly", expected.getDay14(), actual.getDay14());
        assertEquals("Timesheet day15 isn't saved/retrieved correctly", expected.getDay15(), actual.getDay15());
        assertEquals("Timesheet day16 isn't saved/retrieved correctly", expected.getDay16(), actual.getDay16());
        assertEquals("Timesheet day17 isn't saved/retrieved correctly", expected.getDay17(), actual.getDay17());
        assertEquals("Timesheet day18 isn't saved/retrieved correctly", expected.getDay18(), actual.getDay18());
        assertEquals("Timesheet day19 isn't saved/retrieved correctly", expected.getDay19(), actual.getDay19());
        assertEquals("Timesheet day20 isn't saved/retrieved correctly", expected.getDay20(), actual.getDay20());
        assertEquals("Timesheet day21 isn't saved/retrieved correctly", expected.getDay21(), actual.getDay21());
        assertEquals("Timesheet day22 isn't saved/retrieved correctly", expected.getDay22(), actual.getDay22());
        assertEquals("Timesheet day23 isn't saved/retrieved correctly", expected.getDay23(), actual.getDay23());
        assertEquals("Timesheet day24 isn't saved/retrieved correctly", expected.getDay24(), actual.getDay24());
        assertEquals("Timesheet day25 isn't saved/retrieved correctly", expected.getDay25(), actual.getDay25());
        assertEquals("Timesheet day26 isn't saved/retrieved correctly", expected.getDay26(), actual.getDay26());
        assertEquals("Timesheet day27 isn't saved/retrieved correctly", expected.getDay27(), actual.getDay27());
        assertEquals("Timesheet day28 isn't saved/retrieved correctly", expected.getDay28(), actual.getDay28());
        assertEquals("Timesheet day29 isn't saved/retrieved correctly", expected.getDay29(), actual.getDay29());
        assertEquals("Timesheet day30 isn't saved/retrieved correctly", expected.getDay30(), actual.getDay30());
        assertEquals("Timesheet day31 isn't saved/retrieved correctly", expected.getDay31(), actual.getDay31());
    }
}
