package am.vector.payroll.api.controller.internal;

import am.vector.payroll.api.entity.admin.Workday;
import am.vector.payroll.api.service.WorkdayService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/workday", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class WorkdayController {

    private final Gson gsonBase = new Gson();

    private final WorkdayService workdayService;

    @GetMapping("/search/{START}/{END}")
    public ResponseEntity<String> findAll(@PathVariable String START, @PathVariable String END){
        LocalDate start = LocalDate.parse(START);
        LocalDate end = LocalDate.parse(END);
        return ResponseEntity.ok(gsonBase.toJson(workdayService.workDaysForPeriod(start, end)));
    }

    @GetMapping("/{DAY}")
    public ResponseEntity<String> getDay(@PathVariable String DAY){
        return ResponseEntity.ok(gsonBase.toJson(workdayService.getWorkday(LocalDate.parse(DAY))));
    }

    @PutMapping("/")
    public ResponseEntity<String> updateDay(@RequestBody String json){
        Workday object = new Gson().fromJson(json, Workday.class);
        return ResponseEntity.ok(gsonBase.toJson(workdayService.updateWorkday(object)));
    }

    @DeleteMapping("/{DAY}/")
    public void delete(@PathVariable String DAY) {
        workdayService.deleteById(LocalDate.parse(DAY));
    }
}
