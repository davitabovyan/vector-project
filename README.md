### Payroll

This is a backend service Payroll.

#### Dev Setup

Application runs by default at port specified in application.yml ( localhost:8080 )


#### Custom error codes

In cases where an exception has accrued independent to users activity general exception is thrown with specified code :

###### "1001"

 - Incorrect value provided in permission

###### "1002"

The .xml file for day-shift is not present in the server

#### API call formats

##### API GET

- for selecting fields of the object (including nested) to be returned use **object1**:_field1_._field2_,**object2**:_field1_._field2_
in order to get _field1_ and _field2_ of **object1** and _field1_ and _field2_ of **object2**