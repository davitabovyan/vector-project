package am.vector.payroll.api.repository.main;

import am.vector.common.persistence.BaseRepository;
import am.vector.payroll.api.entity.main.Department;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@RepositoryRestResource(collectionResourceRel = "department", path = "department", exported = false)
public interface DepartmentRepository extends BaseRepository<Department, Long> {
//
//    @Query("SELECT d FROM Department d where d.id > :idStart and d.id < :idEnd")
//    Iterable<Department> findByCompanyId(@Param("idStart") long start, @Param("idEnd") long end);

    @Query("SELECT d FROM Department d JOIN FETCH d.head p where d.companyId = :companyId")
    List<Department> getAllDepartmentsWithHead(@Param("companyId") int companyId);

    @Query("SELECT d FROM Department d JOIN FETCH d.head p where d.id = :id")
    Optional<Department> getDepartmentWithHead(@Param("id") long id);

    @Query("SELECT d FROM Department d where d.companyId = :companyId")
    List<Department> findAllPerCompany(@Param("companyId") int companyId);

    Optional<Department> findFirstByCodeAndCompanyIdOrderByIdAsc(String code, int companyId);

//    @Query("SELECT d FROM Department d WHERE d.code = :code AND d.companyId = :companyId ORDER BY d.id ASC LIMIT 1")
//    Optional<Department> findByCode(@Param("code") String code, @Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Department d where d.id = :id")
    void deleteById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Department d WHERE d.code = :code AND d.companyId = :companyId")
    void deleteByCode(@Param("code") String code, @Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Department a where a.companyId = :companyId")
    void deleteAll(@Param("companyId") int companyId);
}
