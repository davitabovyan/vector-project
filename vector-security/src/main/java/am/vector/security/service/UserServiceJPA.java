package am.vector.security.service;


import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.ErrorMessage;
import am.vector.common.constants.Module;
import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.exception.custom.UserNameAlreadyTakenException;
import am.vector.common.exception.generic.BadRequestVectorException;
import am.vector.common.model.admin.CompanyModel;
import am.vector.common.model.admin.UserModel;
import am.vector.common.rest.Api;
import am.vector.common.util.GeneralUtil;
import am.vector.security.entity.Client;
import am.vector.security.entity.User;
import am.vector.security.repository.UserRepository;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class UserServiceJPA implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper mapper;

    public UserModel findUserByUserName(String userName){
        Optional<User> user = userRepository.findByUsername(userName);
        if(user.isPresent()){
            return mapper.map(user.get(), UserModel.class);
        } else throw new EntityNotFoundException(User.class, userName);
    }

    public UserModel findById(long id){
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()){
            return mapper.map(user.get(), UserModel.class);
        } else throw new EntityNotFoundException(User.class, GeneralUtil.encodeUuid(id));
    }


    public List<UserModel> findAll(){
        List<User> list = userRepository.findAll();
        return list.stream().map(this::toModel).collect(Collectors.toList());
    }

    @Transactional
    public UserModel save(UserModel newUser){
        newUser.setPassword(new BCryptPasswordEncoder().encode(newUser.getPassword()))
            .setEnabled(true);
        if(newUser.getCompanyId() == 0){
            newUser.setCompanyId(GeneralUtil.getCompanyId());
        }

        if(newUser.isAdmin()){
            newUser.setReadAccess(Long.MAX_VALUE - 1);
            newUser.setEditAccess(Long.MAX_VALUE - 2);
        }

        Optional<User> user = userRepository.findByUsername(newUser.getUsername());
        if(user.isPresent()){
            throw new UserNameAlreadyTakenException(newUser.getUsername());
        }
        userRepository.save(toEntity(newUser));
        return newUser;
    }

    public void saveAll(List<UserModel> objects) {
        List<User> list = objects.stream().map(this::toEntity).collect(Collectors.toList());
        userRepository.saveAll(list);
    }

    public void removeUser(String userName) {
        Optional<User> user = userRepository.findByUsername(userName);
        if(user.isEmpty()){
            throw new EntityNotFoundException(User.class, userName);
        }
        userRepository.deleteByUserName(userName);
    }

    public void deleteAll(List<UserModel> objects) {
        List<User> list = objects.stream().map(this::toEntity).collect(Collectors.toList());
        userRepository.deleteAll(list);
    }

    public List<UserModel> findAll(int companyId){
        List<User> list = userRepository.findAllPerCompany(companyId);
        return list.stream().map(this::toModel).collect(Collectors.toList());
    }

    public void updatePermissions(UserModel user, Map<String, String> permissions){
        long readAccess = user.getReadAccess();
        long editAccess = user.getEditAccess();
        for (Map.Entry<String, String> entry : permissions.entrySet()) {
            int index = EntityObject.valueOf(entry.getKey().toUpperCase()).getIndex();
            if(index == 0) continue;
            if ("none".equals(entry.getValue())) {
                readAccess = readAccess & ~(1 << index);
                editAccess = editAccess & ~(1 << index);
            } else if ("read".equals(entry.getValue())) {
                readAccess = readAccess | (1 << index);
                editAccess = editAccess & ~(1 << index);
            } else if ("edit".equals(entry.getValue())) {
                readAccess = readAccess | (1 << index);
                editAccess = editAccess | (1 << index);
            } else {
                throw new BadRequestVectorException(ErrorMessage.UNSUPPORTED_VALUE.get(),
                        entry.getKey(), entry.getValue(), "none, read, or edit");
            }
            user.setReadAccess(readAccess);
            user.setEditAccess(editAccess);
            userRepository.save(toEntity(user));
        }
    }

    private UserModel toModel(User entity) {
        return mapper.map(entity, UserModel.class);
    }

    private User toEntity(UserModel model) {
        return mapper.map(model, User.class)
                .setClient(new Client().setId(model.getClientId()));
    }
}
