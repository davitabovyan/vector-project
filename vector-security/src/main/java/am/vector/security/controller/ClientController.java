package am.vector.security.controller;

import am.vector.common.exception.VectorException;
import am.vector.common.model.admin.ClientModel;
import am.vector.common.model.admin.CompanyModel;
import am.vector.security.dto.ClientDto;
import am.vector.security.entity.Client;
import am.vector.security.service.ClientService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "api/v1/client", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class ClientController {

    private final ClientService clientService;

    private static final Gson GSON = new Gson();

    @PostMapping
    public ResponseEntity<String> create(@RequestBody ClientDto dto){
        Client client = clientService.save(dto);
        return ResponseEntity.ok(GSON.toJson(client));
    }

    @GetMapping
    @Secured("ROLE_SUPERADMIN")
    public ResponseEntity<String> getAll(){
        Iterable<Client> objects = clientService.findAll();
        return ResponseEntity.ok(GSON.toJson(objects));
    }

    @PatchMapping("/{clientId}")
    public ResponseEntity<String> addCompany(@PathVariable Integer clientId, @RequestBody CompanyModel company) {
        return ResponseEntity.ok(GSON.toJson(clientService.addCompany(company, clientId)));
    }

    @GetMapping("/{value}")
    public ResponseEntity<String> findById(@PathVariable String value,
                                           @RequestParam(value = "type", required = false) String type) {
        Client client;
        if("id".equals(type)) {
            client = clientService.findClientById(value);
        } else if("email".equals(type)) {
            client = clientService.findClientByEmail(value);
        } else throw new VectorException("Request parameter type should be \"id\" or \"email\"");
        return ResponseEntity.ok(GSON.toJson(Client.toModel(client)));
    }

    @PutMapping
    public void update(@RequestBody ClientModel model){
        clientService.update(Client.fromModel(model));
    }

    @DeleteMapping("/{id}")
    @Secured("ROLE_SUPERADMIN")
    public void remove(@PathVariable("id") Integer id){
        clientService.remove(id);
    }

}
