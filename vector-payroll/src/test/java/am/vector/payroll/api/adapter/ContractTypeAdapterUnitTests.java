package am.vector.payroll.api.adapter;

import am.vector.payroll.api.entity.main.Contract;
import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.entity.main.Role;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class ContractTypeAdapterUnitTests extends BaseTypeAdapterUnitTest<ContractTypeAdapter, Contract> {

    private static Contract contract;
    private ContractTypeAdapter adapter;

    @BeforeClass
    public static void setup(){
        contract = new Contract()
            .setSalary(600_000)
            .setStartDate(LocalDate.parse("2015-11-12"))
            .setEndDate(LocalDate.parse("2018-10-31"))
            .setLaborRelation(true)
            .setDailyHours(7);
    }

    @Before
    public void beforeTest() throws Exception {
        adapter = new ContractTypeAdapter();
        prepare(Arrays.asList(Contract.class, Person.class, Department.class, Role.class), adapter, contract, Collections.emptySet());
    }

    @Test
    public void write_OnlySelectedFieldsAreReturned() throws Exception {
        new ContractTypeAdapter("salary,dailyHours").write(writer, contract);

        verify(writer).name("salary");
        verify(writer).name("dailyHours");
        verify(writer, never()).name("workWeek");
    }

    @Test
    public void write_correctValuesAreReturned() throws Exception {
        adapter.write(writer, contract);

        verify(writer).value(ArgumentMatchers.eq(contract.getSalary()));
        verify(writer).value(ArgumentMatchers.eq(contract.getDailyHours()));
        verify(writer).value(ArgumentMatchers.eq(contract.getStartDate().toString()));
        verify(writer).value(ArgumentMatchers.eq(contract.getEndDate().toString()));
//        verify(writer).value(ArgumentMatchers.eq(contract.getLaborRelation()));
        verify(writer).value(ArgumentMatchers.eq(contract.getWorkWeek()));
    }
}
