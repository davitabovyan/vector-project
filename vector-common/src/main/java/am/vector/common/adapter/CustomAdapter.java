package am.vector.common.adapter;

import am.vector.common.persistence.UUIDEntity;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.IOUtil;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Map;
import java.util.TimeZone;

public abstract class CustomAdapter<T extends UUIDEntity> extends TypeAdapter<T> {

    protected final Map<String, String> fields;

    public CustomAdapter(){
        this.fields = Collections.singletonMap("*",null);
    }

    public CustomAdapter(Class<? extends UUIDEntity> clazz, String fields){
        this.fields = IOUtil.parseFields(clazz, fields);
    }

    public CustomAdapter(Map<String, String> fields){
        this.fields = fields;
    }

    @Override
    public T read(JsonReader jsonReader) throws IOException {
        return null;
    }

    @Override
    public void write(JsonWriter jsonWriter, T entity) throws IOException {
        if(entity == null){
            jsonWriter.nullValue();
            return;
        }
        jsonWriter.beginObject();
        if(entity.getId() != 0) jsonWriter.name("uuid").value(GeneralUtil.encodeUuid(entity.getId()));
        if(fields != null) {
            fillGeneral(jsonWriter, entity);
            writeObject(jsonWriter, entity, fields);
        }
        jsonWriter.endObject();
    }

    private void fillGeneral(JsonWriter jsonWriter, T entity) throws IOException {
        if(fields.containsKey("state")) jsonWriter.name("state").value(entity.getState().name());
//        if(fields.contains("recorderUuid")) jsonWriter.name("recorderUuid").value(entity.getRecorderUuid());
        if(fields.containsKey("recordedDate")) {
            LocalDateTime created = LocalDateTime.ofInstant(Instant.ofEpochMilli(entity.getId()/1_000_000L), TimeZone.getDefault().toZoneId());
            jsonWriter.name("recordedDate").value(created.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        }
    }

    protected void parseDate(String fieldName, JsonWriter jsonWriter, LocalDate date) throws IOException {
        if(date != null){
            jsonWriter.name(fieldName)
                    .value(date.getYear()+"-"+date.getMonthValue()+"-"+date.getDayOfMonth());
        }
    }

    protected void parseDateObject(String fieldName, JsonWriter jsonWriter, LocalDate date) throws IOException {
        if(date != null){
            jsonWriter.name(fieldName).beginObject();
            jsonWriter.name("year").value(date.getYear());
            jsonWriter.name("month").value(date.getMonthValue());
            jsonWriter.name("day").value(date.getDayOfMonth());
            jsonWriter.endObject();
        }
    }

    public abstract JsonWriter writeObject(JsonWriter jsonWriter, T entity, Map<String, String> fields) throws IOException;
}
