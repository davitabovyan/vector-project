package am.vector.journal.api.service.jpa;

import am.vector.common.exception.VectorException;
import am.vector.common.exception.generic.BadRequestVectorException;
import am.vector.common.util.GeneralUtil;
import am.vector.journal.api.entity.Account;
import am.vector.journal.api.repository.AccountRepository;
import am.vector.journal.api.service.AccountService;
import am.vector.journal.api.service.BaseClientCodeIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceJpa
    extends BaseClientCodeIdService<Account>
    implements AccountService {

    private AccountRepository accountRepository;

    @Autowired
    public AccountServiceJpa(AccountRepository accountRepository) {
        super(accountRepository);
        this.accountRepository = accountRepository;
    }

    @Override
    @Transactional
    public void update(Account object) {
        GeneralUtil.setRecordStatus(object);
        accountRepository.save(object);
    }

    @Override
    @Transactional
    public Account save(Account account) {
        if(null == account.getParent()) {
            return accountRepository.save(account);
        } else {
            Account parent = account.getParent();
            if(parent.getId() == 0) {
                parent = accountRepository.save(parent);
            } else {
                long id = account.getParent().getId();
                parent = findEntity(id).setIsParent(true);
            }
            return accountRepository.save(account.setParent(parent));
        }
    }

    @Override
    public List<Account> saveAll(List<Account> objects) {
        throw new VectorException("Operation not supported, all accounts can't be saved at once");
    }

    @Override
    public List<Account> findAllById(List<Long> id) {
        throw new VectorException("Operation not supported, all accounts can't be retrieved at once");
    }

    @Override
    public void deleteById(long id) {
        Account entity = findEntity(id);
        if(entity.getIsParent()) {
            throw new BadRequestVectorException("error.journal.aggregate_account.remove", entity.getCode(), entity.getName());
        }
        accountRepository.deleteById(id);
    }

    @Override
    public void chaneAccountAggregation(long id, boolean enable) {
        Account entity = findEntity(id);
        if (enable) {
            // TODO check whether there are booked transactions
        } else {
            int companyId = GeneralUtil.getCompanyId();
            List<Account> children = accountRepository.findChildren(id, companyId * ID_LIMIT,
                (companyId+1) * ID_LIMIT);
            if(!children.isEmpty()) {
                throw new BadRequestVectorException("error.journal.aggregate_account.change",
                    children.stream().map(Account::getCode).collect(Collectors.joining(",")));
            }
        }
        accountRepository.save(entity.setIsParent(enable));
    }

    @Override
    public List<Account> findAllNodes() {
        int customer = GeneralUtil.getCompanyId();
        return accountRepository.findAllChildNodes(customer * ID_LIMIT, (customer+1) * ID_LIMIT);
    }

}
