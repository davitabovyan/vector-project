package am.vector.common.util.functional;

import java.io.IOException;

@FunctionalInterface
public interface Function<T, R> {
    R apply(T var1) throws IOException;
}
