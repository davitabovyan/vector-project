package am.vector.payroll.api.adapter;

import am.vector.common.constants.Gender;
import am.vector.payroll.api.entity.main.Person;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.time.LocalDate;
import java.util.Collections;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class PersonTypeAdapterUnitTests extends BaseTypeAdapterUnitTest<PersonTypeAdapter, Person> {

    private static Person person;
    private PersonTypeAdapter adapter;

    @BeforeClass
    public static void setup(){
        person = new Person()
            .setSsn("1034512395")
            .setCode("0112")
            .setFirstName("Hakobyan")
            .setLastName("Ashot")
            .setMiddleName("Grigori")
            .setEmail("hakob.abovyan@gmail.com")
            .setCell("37499618533")
            .setGender(Gender.MALE)
            .setBirthday(LocalDate.parse("2000-05-10"));
    }

    @Before
    public void beforeTest() throws Exception {
        adapter = new PersonTypeAdapter();
        prepare(Collections.singletonList(Person.class), adapter, person, Collections.emptySet());
    }

    @Test
    public void write_OnlySelectedFieldsAreReturned() throws Exception {
        new PersonTypeAdapter("ssn,email").write(writer, person);

        verify(writer).name("ssn");
        verify(writer).name("email");
        verify(writer, never()).name("cell");
    }

    @Test
    public void write_correctValuesAreReturned() throws Exception {
        adapter.write(writer, person);

        verify(writer).value(ArgumentMatchers.eq(person.getCode()));
        verify(writer).value(ArgumentMatchers.eq(person.getEmail()));
        verify(writer).value(ArgumentMatchers.eq(person.getSsn()));
        verify(writer).value(ArgumentMatchers.eq(person.getFirstName()));
        verify(writer).value(ArgumentMatchers.eq(person.getLastName()));
        verify(writer).value(ArgumentMatchers.eq(person.getMiddleName()));
        verify(writer).value(ArgumentMatchers.eq(person.getCell()));
        verify(writer).value(ArgumentMatchers.eq("2000-5-10"));
        verify(writer).value(ArgumentMatchers.eq(person.getGender().name()));
    }
}
