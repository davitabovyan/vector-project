package am.vector.payroll.api.service;

import am.vector.common.persistence.CommonService;
import am.vector.payroll.api.entity.main.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface RoleService extends CommonService<Role> {
    Logger logger = LogManager.getLogger();

    void deleteByCode(String code);
}
