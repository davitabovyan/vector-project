package am.vector.payroll.api.adapter;

import am.vector.common.adapter.GsonAdapter;
import am.vector.common.constants.EntityObject;
import am.vector.payroll.api.entity.main.*;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GsonPayrollAdapter extends GsonAdapter {

    public GsonPayrollAdapter() {
        addToMap(EntityObject.ABSENCE, EntityObject.PERSON);
        addToMap(EntityObject.CONTRACT, EntityObject.PERSON, EntityObject.ROLE, EntityObject.DEPARTMENT);
        addToMap(EntityObject.DEPARTMENT, EntityObject.PERSON);
        addToMap(EntityObject.EMPLOYMENT, EntityObject.PERSON);
        addToMap(EntityObject.PAYSLIP, EntityObject.PERSON);
        addToMap(EntityObject.PERSON);
        addToMap(EntityObject.ROLE);
        addToMap(EntityObject.TIMESHEET, EntityObject.CONTRACT);
        setup();
    }

    protected void registerAdapter(GsonBuilder builder, EntityObject objCode, Map<String, String> fields){
        switch (objCode) {
            case ABSENCE:       builder.registerTypeAdapter(Absence.class, new AbsenceTypeAdapter(fields));
                break;
            case CONTRACT:      builder.registerTypeAdapter(Contract.class, new ContractTypeAdapter(fields));
                break;
            case DEPARTMENT:    builder.registerTypeAdapter(Department.class, new DepartmentTypeAdapter(fields));
                break;
            case EMPLOYMENT:    builder.registerTypeAdapter(Employment.class, new EmploymentTypeAdapter(fields));
                break;
            case PAYSLIP:       builder.registerTypeAdapter(Payslip.class, new PayslipTypeAdapter(fields));
                break;
            case PERSON:        builder.registerTypeAdapter(Person.class, new PersonTypeAdapter(fields));
                break;
            case ROLE:          builder.registerTypeAdapter(Role.class, new RoleTypeAdapter(fields));
                break;
            case TIMESHEET:     builder.registerTypeAdapter(TimeSheet.class, new TimesheetTypeAdapter(fields));
                break;
        }
    }
}
