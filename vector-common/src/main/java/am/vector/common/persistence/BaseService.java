package am.vector.common.persistence;

import java.util.List;

public interface BaseService<T> {

    long ID_LIMIT = 10_000_000_000_000L;

    T findById(long id);

    void update(T object);

    T save(T object);

    List<T> saveAll(List<T> objects);

    List<T> findAllById(List<Long> id);

    void deleteById(long id);

    void deleteAll(int customerId);
}
