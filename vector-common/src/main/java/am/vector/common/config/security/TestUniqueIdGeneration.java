package am.vector.common.config.security;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TestUniqueIdGeneration {
    private static volatile int counter = 0;

    // run in debug mode not to get ConcurrentModificationException
    public static void main(String[] args) {
        Set<Long> set = new HashSet<>();
        TestUniqueIdGeneration test = new TestUniqueIdGeneration();
        List<Thread> threads = new LinkedList<>();
        List<MyThread> myThreads = new LinkedList<>();
        for(int i = 0; i<10; ++i){
            MyThread myThread = test.new MyThread();
            myThreads.add(myThread);
            Thread thread = new Thread(myThread);
            thread.start();
            threads.add(thread);
        }

        LinkedHashSet<Long> sorted = new LinkedHashSet<>();
        for(MyThread thread : myThreads){
            sorted.addAll(thread.set);
        }

        System.out.println(sorted.size());
        sorted = sorted.stream().sorted().collect(Collectors.toCollection(LinkedHashSet::new));
        for(Long l : sorted) {
            System.out.println(l);
        }
    }


    private long getId() {
        return 1_000_000 * System.currentTimeMillis()
            + 1000 * increment()
            + 2;
    }
    private synchronized int increment(){
        return counter < 1000 ? ++counter : 0;
    }

    class MyThread implements Runnable {

        public LinkedHashSet<Long> set;

        MyThread() {
            this.set = new LinkedHashSet<>();
        }

        @Override
        public void run() {
            for(int i = 0; i<100; ++i) {
                set.add(getId());
            }
        }
    }
}
