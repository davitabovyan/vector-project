package am.vector.payroll.api.entity.main;

import am.vector.common.constants.EntityObject;
import am.vector.common.persistence.TimestampId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
@Accessors(chain = true) @Getter @Setter
public class Department extends TimestampId {

    private String code;
    private String name;
    private String costCenter;
    @OneToOne(fetch= FetchType.LAZY, optional=false)
    private Person head;

    public Department() {
        super(EntityObject.DEPARTMENT);
    }

    public Department(long uuid) {
        super(EntityObject.DEPARTMENT, uuid);
    }

    public Department copy(){
        return (Department) super.copy(this);
    }
}
