package am.vector.payroll.api.service;

import am.vector.common.persistence.CommonService;
import am.vector.payroll.api.entity.main.Department;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface DepartmentService extends CommonService<Department> {
    Logger logger = LogManager.getLogger();

    Department getDepartmentWithHead(long uuid);
    List<Department> getAllDepartmentsWithHead();
    Department getByCode(String code);
    void deleteByCode(String code);
}
