package am.vector.payroll.api;

import am.vector.common.model.UUIDModel;
import am.vector.common.persistence.UUIDEntity;

public class TestUtil {
    public static long generateId(UUIDModel entity){
        return 1_000_000 * System.currentTimeMillis()
                + 100 * ((int) (Math.random() * 1000))
                + entity.getObjCodeIndex();
    }

    public static long generateId(UUIDEntity entity){
        return 1_000_000 * System.currentTimeMillis()
                + 100 * ((int) (Math.random() * 1000))
                + entity.getObjCodeIndex();
    }
}
