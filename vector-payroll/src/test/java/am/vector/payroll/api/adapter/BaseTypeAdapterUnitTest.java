package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.common.persistence.UUIDEntity;
import com.google.gson.stream.JsonWriter;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

abstract class BaseTypeAdapterUnitTest<T extends CustomAdapter<Z>, Z extends UUIDEntity> {
    private List<Class<? extends UUIDEntity>> classesUnderTest;
    private Z entity;
    private T adapter;
    private Set<String> secondaryFields;
    JsonWriter writer;

    void prepare(List<Class<? extends UUIDEntity>> classesUnderTest, T adapter, Z entity, Set<String> secondaryFields) throws IOException{
        this.classesUnderTest = classesUnderTest;
        this.adapter = adapter;
        this.entity = entity;
        this.secondaryFields = secondaryFields;
        setUpMock();
    }

    private void setUpMock() throws IOException {
        writer = mock(JsonWriter.class);
        when(writer.name("uuid")).thenReturn(writer);
        when(writer.name("state")).thenReturn(writer);
        when(writer.name("year")).thenReturn(writer);
        when(writer.name("month")).thenReturn(writer);
        when(writer.name("day")).thenReturn(writer);
        for(Class<? extends UUIDEntity> clazz : classesUnderTest){
            for(Field field : clazz.getDeclaredFields()) {
                when(writer.name(field.getName())).thenReturn(writer);
            }
        }
    }

    @Test
    public void write_AsteriskReturnAllMainFields() throws Exception {
        adapter.write(writer, entity);

        for(Field field : entity.getClass().getDeclaredFields()) {
            if(secondaryFields.contains(field.getName())){
                verify(writer, never()).name(ArgumentMatchers.eq(field.getName()));
            } else {
                verify(writer).name(ArgumentMatchers.eq(field.getName()));
            }
        }
    }

    @Test
    public void write_AsteriskNotReturnAnyNotMainField() throws Exception {
        adapter.write(writer, entity);

        for(String key : new String[]{"state", "recorderUuid","recordedDate"}) {
            verify(writer, never()).name(key);
        }

        for(String key : secondaryFields){
            verify(writer, never()).name(key);
        }
    }

    public abstract void write_OnlySelectedFieldsAreReturned() throws Exception;
    public abstract void write_correctValuesAreReturned() throws Exception;
}
