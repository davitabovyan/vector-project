package am.vector.payroll.api.repository.main;

import am.vector.common.persistence.BaseRepository;
import am.vector.payroll.api.dto.timesheet.MainTimesheetDto;
import am.vector.payroll.api.entity.main.Contract;
import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.entity.main.TimeSheet;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "timesheet", path = "timesheet", exported = false)
public interface TimesheetRepository extends BaseRepository<TimeSheet, Long> {

    @Query("SELECT t FROM TimeSheet t JOIN FETCH t.contract c where c.person = :person")
    Optional<TimeSheet> getTimesheetWithPerson(@Param("person") Person person);

    @Query(
            "SELECT new am.vector.payroll.api.dto.timesheet.MainTimesheetDto(c.person.id, c.person.firstName, c.person.lastName, c.person.middleName, c.person.code, c.role.name, t.id, c.person.code, t.period, t.total, t.day1, t.day2, t.day3, t.day4, t.day5, t.day6, t.day7, t.day8, t.day9, t.day10, t.day11, t.day12, t.day13, t.day14, t.day15, t.day16, t.day17, t.day18, t.day19, t.day20, t.day21, t.day22, t.day23, t.day24, t.day25, t.day26, t.day27, t.day28, t.day29, t.day30, t.day31)" +
                    " FROM TimeSheet t JOIN t.contract c JOIN c.person p JOIN c.role r where t.period = :period AND t.companyId = :companyId AND c.department = :department")
    List<MainTimesheetDto> getTimesheetsByPeriodByDepartment(@Param("period") String period, @Param("companyId") int companyId, @Param("department") Department department);

//    @Query("SELECT t FROM Timesheet t where t.period = :period AND t.companyId = :companyId")
//    List<Timesheet> getTimesheetsByPeriod(@Param("period") String period, @Param("companyId") int companyId);

    @Query(
            "SELECT new am.vector.payroll.api.dto.timesheet.MainTimesheetDto(c.person.id, c.person.firstName, c.person.lastName, c.person.middleName, c.person.code, c.role.name, t.id, c.department.code, t.period, t.total, t.day1, t.day2, t.day3, t.day4, t.day5, t.day6, t.day7, t.day8, t.day9, t.day10, t.day11, t.day12, t.day13, t.day14, t.day15, t.day16, t.day17, t.day18, t.day19, t.day20, t.day21, t.day22, t.day23, t.day24, t.day25, t.day26, t.day27, t.day28, t.day29, t.day30, t.day31)" +
                    " FROM TimeSheet t JOIN t.contract c JOIN c.person p JOIN c.role r JOIN c.department d where t.period = :period AND t.companyId = :companyId")
    List<MainTimesheetDto> getTimesheetsByPeriod(@Param("period") String period, @Param("companyId") int companyId);

    @Query("SELECT t FROM TimeSheet t where t.companyId = :companyId")
    List<TimeSheet> findAllPerCompany(@Param("companyId") int companyId);

    @Query("SELECT t FROM TimeSheet t where t.period = :period AND t.companyId = :companyId")
    List<TimeSheet> findAllByPeriod(@Param("period") String period, @Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM TimeSheet t where t.companyId = :companyId AND t.period = :period")
    void deleteByPeriod(@Param("period") String period, @Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM TimeSheet t where t.period = :period AND t.companyId = :companyId AND t.contract = :contract")
    void deleteByPeriodByContract(@Param("period") String period, @Param("companyId") int companyId, @Param("contract") Contract contract);

    @Transactional
    @Modifying
    @Query("DELETE FROM TimeSheet t where t.id = :id")
    void deleteById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM TimeSheet a where a.companyId = :companyId")
    void deleteAll(@Param("companyId") int companyId);
}