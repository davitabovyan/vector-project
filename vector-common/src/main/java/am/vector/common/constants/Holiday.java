package am.vector.common.constants;

import java.time.Month;

public enum Holiday {
    NONE(null, 0),
    JAN01(Month.JANUARY, 1),
    JAN02(Month.JANUARY, 2),
    JAN03(Month.JANUARY, 3),
    JAN04(Month.JANUARY, 4),
    JAN05(Month.JANUARY, 5),
    JAN06(Month.JANUARY, 6),
    JAN07(Month.JANUARY, 7),
    JAN28(Month.JANUARY, 28),
    MAR08(Month.MARCH, 8),
    APR24(Month.APRIL, 24),
    MAY01(Month.MAY, 1),
    MAY09(Month.MAY, 9),
    MAY28(Month.MAY, 28),
    JUL05(Month.JULY, 5),
    SEP21(Month.SEPTEMBER, 21),
    DEC31(Month.DECEMBER, 31);

    final Month month;
    final int day;

    Holiday(Month month, int day){
        this.month = month;
        this.day = day;
    }

    public Month getMonth() {
        return this.month;
    }

    public int getDay() {
        return this.day;
    }
}
