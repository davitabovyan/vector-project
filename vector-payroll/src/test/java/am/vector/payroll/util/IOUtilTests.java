package am.vector.payroll.util;

import am.vector.common.util.IOUtil;
import am.vector.payroll.api.entity.main.Employment;
import am.vector.payroll.api.entity.main.Person;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class IOUtilTests {

    @Test
    public void simpleEntityAllFields_success(){
        // Setup
        String fields = "*";
        // Act
        Map<String, String> parsed = IOUtil.parseFields(Person.class, fields);
        // Assert
        assertTrue("Map doesn't contain *", parsed.containsKey("*"));
        assertNull("Map's value for key \"*\" is not null", parsed.get("*"));
    }

    @Test
    public void simpleEntitySomeFields_success(){
        // Setup
        String fields = "email,firstName,cell";
        // Act
        Map<String, String> parsed = IOUtil.parseFields(Person.class, fields);
        // Assert
        assertTrue("Map doesn't contain field \"name\"", parsed.containsKey("email"));
        assertTrue("Map doesn't contain field \"some\"", parsed.containsKey("firstName"));
        assertTrue("Map doesn't contain field \"other\"", parsed.containsKey("cell"));
        assertNull("Map's value for key \"name\" is not null", parsed.get("email"));
        assertNull("Map's value for key \"some\" is not null", parsed.get("firstName"));
        assertNull("Map's value for key \"other\" is not null", parsed.get("cell"));
    }

    @Test
    public void allMainAndNestedEntity_success(){
        // Setup
        String fields = "*,person:*";
        // Act
        Map<String, String> parsed = IOUtil.parseFields(Employment.class, fields);
        // Assert
        assertTrue("Map doesn't contain field \"*\"", parsed.containsKey("*"));
        assertTrue("Map doesn't contain field \"person\"", parsed.containsKey("person"));
        assertNull("Map's value for key \"*\" is not null", parsed.get("*"));
        assertEquals("Map's value for key \"person\" is not correct", "*", parsed.get("person"));
    }

    @Test
    public void allMainAndNestedEntitySomeFields_success(){
        // Setup
        String fields = "*,person:email.firstName.cell";
        // Act
        Map<String, String> parsed = IOUtil.parseFields(Employment.class, fields);
        // Assert
        assertTrue("Map doesn't contain field \"*\"", parsed.containsKey("*"));
        assertTrue("Map doesn't contain field \"person\"", parsed.containsKey("person"));
        assertNull("Map's value for key \"*\" is not null", parsed.get("*"));
        assertEquals("Map's value for key \"person\" is not correct", "email,firstName,cell", parsed.get("person"));
    }
}
