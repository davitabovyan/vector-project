package am.vector.payroll.api.service.impl.jpa.internal;

import am.vector.payroll.api.entity.admin.Workday;
import am.vector.payroll.api.repository.internal.WorkdayRepository;
import am.vector.payroll.api.service.WorkdayService;
import am.vector.payroll.api.support.PayrollPeriod;
import am.vector.payroll.util.DayUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class WorkdayServiceJPA implements WorkdayService {

    private final WorkdayRepository workdayRepository;

    public List<Workday> getWorkdayForMonth(PayrollPeriod period){
        LocalDate firstDay = period.getFirstDay();
        LocalDate lastDay = period.getLastDay();
        Iterable<Workday> days = workDaysForPeriod(firstDay, lastDay);
        List<Workday> list = StreamSupport.stream(days.spliterator(), false).collect(Collectors.toList());
        if(period.getSize() == list.size()){
            return list;
        } else {
            List<Workday> daysToBeAdded = new ArrayList<>();
            LocalDate current = firstDay;
            while(!current.isAfter(lastDay)){
                daysToBeAdded.add(DayUtil.generateDay(current));
                current = current.plusDays(1);
            }
            workdayRepository.saveAll(daysToBeAdded);
            return daysToBeAdded;
        }
    }

    public Workday getWorkday(LocalDate date){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Workday workDay;
        Optional<Workday> day = workdayRepository.findById(date);
        if(day.isPresent()){
            workDay = day.get();
        } else {
            workDay = DayUtil.generateDay(date);
            workdayRepository.save(workDay);
        }
        return workDay;
    }

    public Workday updateWorkday(Workday workday){
        return workdayRepository.save(workday);
    }

    public void deleteById(LocalDate day) {
        workdayRepository.deleteById(day);
    }

    public List<Workday> workDaysForPeriod(PayrollPeriod period){
        return workDaysForPeriod(period.getFirstDay(), period.getLastDay());
    }

    public List<Workday> workDaysForPeriod(LocalDate start, LocalDate end){
        return workdayRepository.findForPeriod(start, end);
    }

    public LocalDate getTerminationDay(PayrollPeriod period, int numOfWorkedDays, boolean isSaturdayNonWorking){
        int counter = 0;
        LocalDate date = LocalDate.of(period.getYear(), period.getMonth(), 1);
        Map<LocalDate, Workday> periodDays = StreamSupport.stream(workDaysForPeriod(period).spliterator(), false)
                .collect(Collectors.toMap(Workday::getDay, Function.identity()));
        while (counter < numOfWorkedDays){
            date = date.plusDays(1);
            if(DayUtil.isWorkingDay(periodDays.get(date), isSaturdayNonWorking)){
                counter++;
            }
        }
        return date;
    }
}
