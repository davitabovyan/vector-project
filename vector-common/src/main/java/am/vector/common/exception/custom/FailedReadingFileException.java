package am.vector.common.exception.custom;

public class FailedReadingFileException extends ProvidedJsonException {
    public FailedReadingFileException(String fileName, long size) {
        super(String.format("Failed to read file %s with size %d",fileName, size));
    }
}
