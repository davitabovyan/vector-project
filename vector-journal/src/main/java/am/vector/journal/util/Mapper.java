package am.vector.journal.util;

import am.vector.common.model.journal.AccountGroupModel;
import am.vector.common.model.journal.AccountModel;
import am.vector.journal.api.entity.Account;
import am.vector.journal.api.entity.AccountGroup;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;


public class Mapper {
    private static ModelMapper MAPPER = new ModelMapper();
    static {
        MAPPER.getConfiguration().setPropertyCondition(Conditions.isNotNull());
    }

    public static Account map(AccountModel model) {
        return MAPPER.map(model, Account.class);
    }

    public static AccountGroup map(AccountGroupModel model) {
        return MAPPER.map(model, AccountGroup.class);
    }

}
