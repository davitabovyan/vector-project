package am.vector.payroll.api.v1.unit.service.impl.jpa;

import am.vector.common.model.payroll.AbsenceModel;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.service.AbsenceService;
import am.vector.payroll.api.service.impl.jpa.main.AbsenceServiceJPA;
import am.vector.payroll.api.v1.unit.BaseTest;
import am.vector.payroll.util.EntityBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class AbsenceServiceJPAUnitTests extends BaseTest {

    private static Person person;

    private AbsenceService absenceService;

    private AbsenceModel vacation;

    @BeforeClass
    public static void beforeClass(){
        person = EntityBuilder.getPersonOne();
    }

    @Before
    public void setup(){
        absenceService = mock(AbsenceServiceJPA.class);
    }

    @After
    public void tearDown(){
        absenceService = null;
    }

    @Test
    public void breakDownVacationIntoPartsOnCreation_success(){

    }
}
