package am.vector.journal.api.entity;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.journal.FSItems;
import am.vector.common.constants.journal.JournalAccountType;
import am.vector.common.persistence.ClientCodeId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Accessors(chain = true) @Getter @Setter
public class Account extends ClientCodeId {

    @NotNull
    private String code;
    @NotNull
    private String name;
    private String displayName;
    private Boolean isParent;

    @Enumerated(EnumType.STRING)
    private JournalAccountType type;

    @Enumerated(EnumType.STRING)
    private FSItems fsItem;

    @ManyToOne(fetch= FetchType.LAZY)
    private Account parent;

    public Account() {
        super(EntityObject.ACCOUNT);
    }

    public Account(long uuid) {
        super(EntityObject.ACCOUNT, uuid);
    }

    public Account copy(){
        return (Account) super.copy(this);
    }
}
