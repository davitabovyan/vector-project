package am.vector.payroll.api.controller.main;

import am.vector.common.model.payroll.PayslipModel;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.IOUtil;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.Payslip;
import am.vector.payroll.api.service.PayslipService;
import am.vector.payroll.util.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/payslip", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class PayslipController {

    private final PayslipService payslipService;
    private final GsonPayrollAdapter adapter;

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findByIdCompact(@PathVariable String uuid,
                                          @RequestParam(value = "fields", required = false) String fields){
        Payslip response;
        if(IOUtil.parseFields(Payslip.class, fields).containsKey("person")) {
            response = payslipService.getPayslipWithPerson(GeneralUtil.getCompanyId(), GeneralUtil.decodeUuid(uuid));
        } else {
            response = payslipService.findById(GeneralUtil.decodeUuid(uuid));
        }
        return ResponseEntity.ok(adapter.toJson(response));
    }

    @GetMapping("/")
    public ResponseEntity<String> getPayslipsByPeriod(@RequestParam("period") String period,
                                         @RequestParam(value = "fields", required = false) String fields){
        Iterable<Payslip> response;
        if(IOUtil.parseFields(Payslip.class, fields).containsKey("person")) {
            response = payslipService.getPayslipsByPeriodWithPerson(GeneralUtil.getCompanyId(), period);
        } else {
            response = payslipService.getPayslipsByPeriod(GeneralUtil.getCompanyId(), period);
        }
        return ResponseEntity.ok(adapter.toJson(response));
    }

    @PostMapping
    public ResponseEntity<Map<String,Object>> create(@RequestBody PayslipModel model) {
        model.setCompanyId(GeneralUtil.getCompanyId());
        Payslip entity = payslipService.save(Mapper.map(model));
        return new ResponseEntity<>(GeneralUtil.uuidJSON(entity), HttpStatus.CREATED);
    }

    @PutMapping("/{uuid}")
    public void update(@RequestBody PayslipModel model, @PathVariable String uuid) {
        model.setCompanyId(GeneralUtil.getCompanyId());
        payslipService.update(Mapper.map(model));
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody PayslipModel model, @PathVariable String uuid) {
        Payslip entity = payslipService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(model));
        payslipService.update(entity);
    }


    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable String uuid) {
        payslipService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
