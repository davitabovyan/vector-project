package am.vector.common.exception.custom;

public class RequiredFieldMissingException extends ProvidedJsonException {
    public RequiredFieldMissingException(String message) {
        super(message);
    }
}
