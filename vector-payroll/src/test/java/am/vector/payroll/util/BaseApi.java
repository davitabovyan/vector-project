package am.vector.payroll.util;

import am.vector.common.rest.BaseApiInterface;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class BaseApi implements BaseApiInterface {

    private TestRestTemplate restTemplate = new TestRestTemplate();

    public ResponseEntity<String> makeGet(HttpHeaders headers, String uri){
        return restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), String.class);
    }

    public ResponseEntity<String> makePost(HttpHeaders headers, String uri, String body){
        return restTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), String.class);
    }

    public ResponseEntity<String> makePut(HttpHeaders headers, String uri, String body){
        return restTemplate.exchange(uri, HttpMethod.PUT, new HttpEntity<>(body, headers), String.class);
    }

    public ResponseEntity<String> makePatch(HttpHeaders headers, String uri, String body){
        return restTemplate.exchange(uri, HttpMethod.PATCH, new HttpEntity<>(body, headers), String.class);
    }

    public ResponseEntity<String> makeDelete(HttpHeaders headers, String uri) {
        return restTemplate.exchange(uri, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
    }
}

