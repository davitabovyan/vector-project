package am.vector.payroll.api.controller.internal;


import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.admin.Company;
import am.vector.payroll.api.service.CompanyService;
import am.vector.payroll.api.service.UploadService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/upload")
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class UploadController {

    private final UploadService uploadService;
	private final CompanyService companyService;

    @PostMapping("/incomeTaxReports")
    public void uploadIncomeTaxReport(@RequestParam("reports") MultipartFile[] reports) {
		Company company = companyService.findById(GeneralUtil.getCompanyId());
		uploadService.uploadIncomeTaxReport(reports, company);
    }

    @PostMapping("/employeesList")
    public void uploadEmployeesList(@RequestParam("file") MultipartFile file) {
        uploadService.uploadEmployeesList(file);
    }

}
