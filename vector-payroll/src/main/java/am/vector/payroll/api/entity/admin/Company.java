package am.vector.payroll.api.entity.admin;

import am.vector.common.constants.EntityObject;
import am.vector.common.model.admin.CompanyModel;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "company")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Accessors(chain = true) @Getter @Setter
@NoArgsConstructor
public class Company {

    transient private static final EntityObject OBJ = EntityObject.COMPANY;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String tin;
    private String email;
    private String phone;
    private String name;
    private String address;
    private Integer workWeek;

    @OneToOne(fetch= FetchType.LAZY, optional=false)
    private Person ceo;
    @OneToOne(fetch= FetchType.LAZY, optional=false)
    private Person cfo;

    public static CompanyModel toMode(Company company) {
        return new CompanyModel()
                .setName(company.getName())
                .setTin(company.getTin())
                .setEmail(company.getEmail())
                .setAddress(company.getAddress())
                .setId(company.getId())
                .setPhone(company.getPhone())
                .setWorkWeek(company.getWorkWeek())
                .setCeo(company.getCeo().getUuid())
                .setCfo(company.getCfo().getUuid());
    }

    public static Company fromMode(CompanyModel model) {
        return new Company()
                .setId(model.getId())
                .setName(model.getName())
                .setEmail(model.getEmail())
                .setAddress(model.getAddress())
                .setPhone(model.getPhone())
                .setTin(model.getTin())
                .setWorkWeek(model.getWorkWeek())
                .setCeo(model.getCeo() == null ? null : new Person(GeneralUtil.decodeUuid(model.getCeo())))
                .setCfo(model.getCfo() == null ? null : new Person(GeneralUtil.decodeUuid(model.getCfo())));
    }
}
