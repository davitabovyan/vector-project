package am.vector.payroll.api.entity.main;

import am.vector.common.constants.EntityObject;
import am.vector.common.persistence.TimestampId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;

@Entity
@Accessors(chain = true) @Getter @Setter
public class Role extends TimestampId {

    private String code;
    private String name;
    private String description;
    private Integer eligibleDays;

    public Role() {
        super(EntityObject.ROLE);
    }

    public Role(long uuid) {
        super(EntityObject.ROLE, uuid);
    }

    public Role copy(){
        return (Role) super.copy(this);
    }
}
