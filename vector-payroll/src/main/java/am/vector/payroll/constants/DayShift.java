package am.vector.payroll.constants;

public enum DayShift {
    NONE,
    TO_NONE_WORKING,
    TO_WORKING
}
