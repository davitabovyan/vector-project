package am.vector.payroll.api.entity.main;

import am.vector.common.constants.EntityObject;
import am.vector.common.persistence.TimestampId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "timesheet")
@Accessors(chain = true) @Getter @Setter
public class TimeSheet extends TimestampId {

    private String period;
    private Integer total;
    private Integer day1;
    private Integer day2;
    private Integer day3;
    private Integer day4;
    private Integer day5;
    private Integer day6;
    private Integer day7;
    private Integer day8;
    private Integer day9;
    private Integer day10;
    private Integer day11;
    private Integer day12;
    private Integer day13;
    private Integer day14;
    private Integer day15;
    private Integer day16;
    private Integer day17;
    private Integer day18;
    private Integer day19;
    private Integer day20;
    private Integer day21;
    private Integer day22;
    private Integer day23;
    private Integer day24;
    private Integer day25;
    private Integer day26;
    private Integer day27;
    private Integer day28;
    private Integer day29;
    private Integer day30;
    private Integer day31;

    @ManyToOne(fetch= FetchType.LAZY, optional=false)
    private Contract contract;

    public TimeSheet setTotal(int hours){
        return this;
    }

    public TimeSheet setDay1(int hours) {
        updateTotal(this.day1, hours);
        this.day1 = hours;
        return this;
    }

    public TimeSheet setDay2(int hours) {
        updateTotal(this.day2, hours);
        this.day2 = hours;
        return this;
    }

    public TimeSheet setDay3(int hours) {
        updateTotal(this.day3, hours);
        this.day3 = hours;
        return this;
    }

    public TimeSheet setDay4(int hours) {
        updateTotal(this.day4, hours);
        this.day4 = hours;
        return this;
    }

    public TimeSheet setDay5(int hours) {
        updateTotal(this.day5, hours);
        this.day5 = hours;
        return this;
    }

    public TimeSheet setDay6(int hours) {
        updateTotal(this.day6, hours);
        this.day6 = hours;
        return this;
    }

    public TimeSheet setDay7(int hours) {
        updateTotal(this.day7, hours);
        this.day7 = hours;
        return this;
    }

    public TimeSheet setDay8(int hours) {
        updateTotal(this.day8, hours);
        this.day8 = hours;
        return this;
    }

    public TimeSheet setDay9(int hours) {
        updateTotal(this.day9, hours);
        this.day9 = hours;
        return this;
    }

    public TimeSheet setDay10(int hours) {
        updateTotal(this.day10, hours);
        this.day10 = hours;
        return this;
    }

    public TimeSheet setDay11(int hours) {
        updateTotal(this.day11, hours);
        this.day11 = hours;
        return this;
    }

    public TimeSheet setDay12(int hours) {
        updateTotal(this.day12, hours);
        this.day12 = hours;
        return this;
    }

    public TimeSheet setDay13(int hours) {
        updateTotal(this.day13, hours);
        this.day13 = hours;
        return this;
    }

    public TimeSheet setDay14(int hours) {
        updateTotal(this.day14, hours);
        this.day14 = hours;
        return this;
    }

    public TimeSheet setDay15(int hours) {
        updateTotal(this.day15, hours);
        this.day15 = hours;
        return this;
    }

    public TimeSheet setDay16(int hours) {
        updateTotal(this.day16, hours);
        this.day16 = hours;
        return this;
    }

    public TimeSheet setDay17(int hours) {
        updateTotal(this.day17, hours);
        this.day17 = hours;
        return this;
    }

    public TimeSheet setDay18(int hours) {
        updateTotal(this.day18, hours);
        this.day18 = hours;
        return this;
    }

    public TimeSheet setDay19(int hours) {
        updateTotal(this.day19, hours);
        this.day19 = hours;
        return this;
    }

    public TimeSheet setDay20(int hours) {
        updateTotal(this.day20, hours);
        this.day20 = hours;
        return this;
    }

    public TimeSheet setDay21(int hours) {
        updateTotal(this.day21, hours);
        this.day21 = hours;
        return this;
    }

    public TimeSheet setDay22(int hours) {
        updateTotal(this.day22, hours);
        this.day22 = hours;
        return this;
    }

    public TimeSheet setDay23(int hours) {
        updateTotal(this.day23, hours);
        this.day23 = hours;
        return this;
    }

    public TimeSheet setDay24(int hours) {
        updateTotal(this.day24, hours);
        this.day24 = hours;
        return this;
    }

    public TimeSheet setDay25(int hours) {
        updateTotal(this.day25, hours);
        this.day25 = hours;
        return this;
    }

    public TimeSheet setDay26(int hours) {
        updateTotal(this.day26, hours);
        this.day26 = hours;
        return this;
    }

    public TimeSheet setDay27(int hours) {
        updateTotal(this.day27, hours);
        this.day27 = hours;
        return this;
    }

    public TimeSheet setDay28(int hours) {
        updateTotal(this.day28, hours);
        this.day28 = hours;
        return this;
    }

    public TimeSheet setDay29(int hours) {
        updateTotal(this.day29, hours);
        this.day29 = hours;
        return this;
    }

    public TimeSheet setDay30(int hours) {
        updateTotal(this.day30, hours);
        this.day30 = hours;
        return this;
    }

    public TimeSheet setDay31(int hours) {
        updateTotal(this.day31, hours);
        this.day31 = hours;
        return this;
    }

    public TimeSheet() {
        super(EntityObject.TIMESHEET);
    }

    public TimeSheet(long uuid) {
        super(EntityObject.TIMESHEET, uuid);
    }

    public TimeSheet copy(){
        return (TimeSheet) super.copy(this);
    }

    private void updateTotal(Integer oldValue, Integer newValue){
        oldValue = null != oldValue ? oldValue : 0;
        if(newValue <= 24){
            this.total = (null != this.total ? this.total : 0) - oldValue + newValue;
        }
    }
}
