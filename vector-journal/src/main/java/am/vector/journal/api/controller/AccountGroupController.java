package am.vector.journal.api.controller;

import am.vector.common.model.journal.AccountGroupModel;
import am.vector.common.util.GeneralUtil;
import am.vector.journal.api.adapter.GsonJournalAdapter;
import am.vector.journal.api.entity.AccountGroup;
import am.vector.journal.api.service.AccountGroupService;
import am.vector.journal.util.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/account_group", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes =
    MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class AccountGroupController {

    private final AccountGroupService accountGroupService;
    private final GsonJournalAdapter adapter;

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findById(@PathVariable String uuid,
                                           @RequestParam(value = "fields", required = false) String fields){
        AccountGroup response = accountGroupService.findById(GeneralUtil.decodeUuid(uuid));
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @GetMapping("/")
    public ResponseEntity<String> findAll(@RequestParam(value = "fields", required = false) String fields){
        List<AccountGroup> response = accountGroupService.findAllPerCompany();
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @PostMapping("/")
    public ResponseEntity<Map<String,Object>> create(@RequestBody AccountGroupModel model) {
        AccountGroup accountGroup = accountGroupService.save(Mapper.map(model));
        return new ResponseEntity<>(GeneralUtil.uuidJSON(accountGroup), HttpStatus.CREATED);
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody AccountGroupModel model, @PathVariable String uuid) {
        AccountGroup entity = accountGroupService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(model));
        accountGroupService.update(entity);
    }

    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable String uuid) {
        accountGroupService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
