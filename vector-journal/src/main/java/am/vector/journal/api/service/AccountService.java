package am.vector.journal.api.service;

import am.vector.common.persistence.CommonService;
import am.vector.journal.api.entity.Account;

import java.util.List;

public interface AccountService extends CommonService<Account> {
    void chaneAccountAggregation(long id, boolean enable);
    List<Account> findAllNodes();
}
