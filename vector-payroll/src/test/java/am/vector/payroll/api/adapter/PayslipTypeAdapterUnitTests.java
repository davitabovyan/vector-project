package am.vector.payroll.api.adapter;

import am.vector.payroll.api.entity.main.Payslip;
import am.vector.payroll.api.entity.main.Person;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class PayslipTypeAdapterUnitTests extends BaseTypeAdapterUnitTest<PayslipTypeAdapter, Payslip> {

    private static Payslip payslip;
    private PayslipTypeAdapter adapter;

    @BeforeClass
    public static void setup(){
        payslip = new Payslip()
            .setPeriod("1018")
            .setWage(200_000L)
            .setBonus(0L)
            .setIt(55_000L)
            .setSsp(5_000L)
            .setArmy(1_000L)
            .setHasUpdate(true)
            .setPerson(new Person());
    }

    @Before
    public void beforeTest() throws Exception {
        adapter = new PayslipTypeAdapter();
        prepare(Arrays.asList(Payslip.class, Person.class), adapter, payslip, Collections.emptySet());
    }

    @Test
    public void write_OnlySelectedFieldsAreReturned() throws Exception {
        new PayslipTypeAdapter("period,wage").write(writer, payslip);

        verify(writer).name("period");
        verify(writer).name("wage");
        verify(writer, never()).name("overtime");
    }

    @Test
    public void write_correctValuesAreReturned() throws Exception {
        adapter.write(writer, payslip);

        verify(writer).value(ArgumentMatchers.eq(payslip.getPeriod()));
        verify(writer).value(ArgumentMatchers.eq(payslip.getWage()));
        verify(writer).value(ArgumentMatchers.eq(payslip.getOvertime()));
        verify(writer).value(ArgumentMatchers.eq(payslip.getBonus()));
        verify(writer).value(ArgumentMatchers.eq(payslip.getSsp()));
        verify(writer).value(ArgumentMatchers.eq(payslip.getIt()));
        verify(writer).value(ArgumentMatchers.eq(payslip.getArmy()));
//        verify(writer).value(ArgumentMatchers.eq(payslip.getHasUpdate()));
    }
}
