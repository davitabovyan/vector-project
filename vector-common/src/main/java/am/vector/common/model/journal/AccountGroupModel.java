package am.vector.common.model.journal;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.journal.AccountGroupType;
import am.vector.common.model.MainModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true) @Getter @Setter
public class AccountGroupModel extends MainModel {
    private String name;
    private AccountGroupType type;

    public void setType(String type) {
        this.type = type != null ? AccountGroupType.valueOf(type) : null;
    }

    public AccountGroupModel() {
        super(EntityObject.ACCOUNT_GROUP);
    }

    public AccountGroupModel(String uuid) {
        super(EntityObject.ACCOUNT_GROUP, uuid);
    }

    public AccountGroupModel copy(){
        return (AccountGroupModel) super.copy(this);
    }
}
