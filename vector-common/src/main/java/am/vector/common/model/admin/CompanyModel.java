package am.vector.common.model.admin;

import am.vector.common.constants.EntityObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true) @Getter @Setter
@NoArgsConstructor
public class CompanyModel {

    transient private static final EntityObject OBJ = EntityObject.COMPANY;

    private Integer id;
    private String tin;
    private String email;
    private String phone;
    private String name;
    private String address;
    private Integer workWeek;
    private String ceo;
    private String cfo;
}
