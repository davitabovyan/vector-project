package am.vector.common.exception.custom;

import am.vector.common.exception.VectorException;

public class UnsupportedFieldException extends VectorException {
    public UnsupportedFieldException(String... keys) {
        super(keys);
    }
}

