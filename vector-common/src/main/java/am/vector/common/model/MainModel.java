package am.vector.common.model;

import javax.persistence.Transient;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MainModel extends UUIDModel {
    @Transient
    private final EntityObject objCode;

    private int companyId;

    public MainModel(EntityObject object) {
        this.objCode = object;
    }

    public MainModel(EntityObject object, String uuid) {
        super(uuid, RecordState.CURRENT);
        this.objCode = object;
    }

    @Override
    public int getObjCodeIndex() {
        return objCode.getIndex();
    }
}
