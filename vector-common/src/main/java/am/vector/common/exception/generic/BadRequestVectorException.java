package am.vector.common.exception.generic;

import am.vector.common.exception.VectorException;

public class BadRequestVectorException extends VectorException {
    public BadRequestVectorException(String... keys) {
        super(keys);
    }
}

