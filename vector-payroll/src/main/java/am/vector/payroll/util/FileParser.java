package am.vector.payroll.util;

import am.vector.common.exception.custom.FailedReadingFileException;
import am.vector.common.exception.custom.RetryActionException;
import am.vector.payroll.api.entity.admin.Company;
import am.vector.payroll.api.entity.internal.TaxReport;
import am.vector.payroll.api.service.impl.jpa.internal.UploadServiceJPA.TaxTotals;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileParser {
    private static final Logger LOG = LoggerFactory.getLogger(FileParser.class);

    public static Map<String, TaxReport> parseReportToPayslips(MultipartFile file, Company company) {
        try (PDDocument document = PDDocument.load(file.getInputStream())) {
            PDFTextStripper stripper = new PDFTextStripper();
            String text = stripper.getText(document);
            String[] sections = text.split("1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 1.10 1.11 1.12 1.13 1.14 1.15");
            String header = sections[0];

            Matcher tinMatcher = Pattern.compile(".*\\(\\bՀՎՀՀ\\b\\)(\\s\\d)(\\s\\d)(\\s\\d)(\\s\\d)(\\s\\d)(\\s\\d)(\\s\\d)(\\s\\d).*").matcher(header);
            if (!tinMatcher.find( ) || tinMatcher.groupCount() != 8) {
                throw new RetryActionException("Reporting company could not be detected");
            }

            StringBuilder sb = new StringBuilder(8);
            for(int i = 1; i < 9; ++i){
                sb.append(tinMatcher.group(i).trim());
            }
            String extractedTin = sb.toString();
            if(!company.getTin().equals(extractedTin)){
                throw new RetryActionException(String.format("Reporting company's tin not matching, expecting %s", company.getTin()));
            }

            Matcher periodMatcher = Pattern.compile(".*(\\d{4})\\s\\(\\bտարի\\b\\)\\s(\\d{2})\\s\\(\\bամիս\\b\\).*").matcher(header);
            if ( !periodMatcher.find( ) || periodMatcher.groupCount() != 2) {
                throw new RetryActionException("Report period could not be detected");
            }

            int period = Integer.parseInt(periodMatcher.group(1) + periodMatcher.group(2));

            String content = sections[1];
            String tablesContent = content.split("Բաժին 1.1")[0];

            Map<String, TaxReport> paySlips = new HashMap<>();
            TaxTotals totalTaxes = new TaxTotals();

            int count = 0;
            sb = new StringBuilder(2);
            for (String s : tablesContent.split("\\r?\\n")) {
                if ("".equals(s)) continue;
                if (s.matches("\\D*") || s.matches("\\d*")) continue;
                if (s.matches("\\d{7},")) continue; // for multiple positions
                sb.append(s);
                sb.append(" ");
                ++count;
                if (count >= 2) {
                    // split by position code or several position codes
                    String[] parts = sb.toString().split("\\s\\d{7}\\s|\\s\\d{7},\\s");
                    String name = parts[0].replaceAll("\\d","").trim();
                    String ssn = parts[0].replaceAll("\\D", "");
                    TaxReport payslip = getFigures(period, parts[1], name, ssn, company.getId());
                    paySlips.put(ssn, payslip);
                    updatePayslipTotals(payslip, totalTaxes);

                    sb = new StringBuilder(2);
                    count = 0;
                }
            }

            String totalsContent = content.split("Ընդամենը 1.17")[1].split("\\r?\\n")[0];
            TaxReport parsedTotal = getFigures(period, totalsContent, null, null, company.getId());
            if(parsedTotal.getIncomeTax()!= totalTaxes.incomeTax){
                throw new RetryActionException("Total fields don't match with the totals of each fields on each row");
            }
            return paySlips;
        } catch (IOException ex){
            LOG.info(String.format("Failed to read file. %s", ex.getMessage()));
            throw new FailedReadingFileException(file.getOriginalFilename(), file.getSize());
        }
    }

    private static TaxReport getFigures(int period, String textToParse, String name, String ssn, int companyId) {
        String[] figures = textToParse.replaceAll("[^\\d,^\\s]", "").trim().split(" ");
        if(figures.length != 10){
            return new TaxReport(period, ssn, name, companyId);
        }
        int salary = Integer.parseInt(figures[0].replaceAll(",", "").trim());
        int service = Integer.parseInt(figures[1].replaceAll(",", "").trim());
        int otherIncome = Integer.parseInt(figures[2].replaceAll(",", "").trim());
        int deductibleIncome = Integer.parseInt(figures[3].replaceAll(",", "").trim());
        int socialPaymentEmployer = Integer.parseInt(figures[4].replaceAll(",", "").trim());
        int socialPaymentBudget = Integer.parseInt(figures[5].replaceAll(",", "").trim());
        int returnOfOverpaidSocial = Integer.parseInt(figures[6].replaceAll(",", "").trim());
        int incomeTax = Integer.parseInt(figures[7].replaceAll(",", "").trim());
        int ssp = Integer.parseInt(figures[8].replaceAll(",", "").trim());
        int workedHours = Integer.parseInt(figures[9].replaceAll(",", "").trim());
        return new TaxReport(period, ssn, name, companyId, salary, service, otherIncome, deductibleIncome, socialPaymentEmployer,
            socialPaymentBudget, returnOfOverpaidSocial, incomeTax, ssp, workedHours);
    }

    private static void updatePayslipTotals(TaxReport taxReport, TaxTotals taxTotals){
        taxTotals.salary += taxReport.getSalary();
        taxTotals.service += taxReport.getService();
        taxTotals.otherIncome += taxReport.getOtherIncome();
        taxTotals.deductibleIncome  += taxReport.getDeductibleIncome();
        taxTotals.socialPaymentEmployer += taxReport.getSocialPaymentEmployer();
        taxTotals.socialPaymentBudget += taxReport.getSocialPaymentBudget();
        taxTotals.returnOfOverpaidSocial += taxReport.getReturnOfOverpaidSocial();
        taxTotals.incomeTax += taxReport.getIncomeTax();
        taxTotals.ssp += taxReport.getSsp();
    }

    public static void parseEmployeeList(MultipartFile file){
        // we create an XSSF Workbook object for our XLSX Excel File
        try (HSSFWorkbook workbook = new HSSFWorkbook(file.getInputStream())) {

            // we get first sheet
            HSSFSheet sheet = workbook.getSheetAt(0);

            Map<String, String> info = new HashMap<>();
            List<Map<String, Object>> list = new LinkedList<>();
            boolean namesStarted = false;
            // we iterate on rows
            Iterator<Row> rowIt = sheet.iterator();

            while (rowIt.hasNext() && !namesStarted) {
                Row row = rowIt.next();

                // iterate on cells for the current row
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    if ("ՀՎՀՀ".equals(cell.getStringCellValue())) {
                        info.put("tin", cellIterator.next().getStringCellValue());
                        break;
                    }
                    if ("Հարկ վճարողի անվանում".equals(cell.getStringCellValue())) {
                        info.put("name", cellIterator.next().getStringCellValue());
                        break;
                    }
                    if ("Անուն".equals(cell.getStringCellValue())) {
                        if ("Ազգանուն".equals(cellIterator.next().getStringCellValue())) {
                            namesStarted = true;
                        }
                        break;
                    }
                }
            }

            while (rowIt.hasNext()) {
                Row row = rowIt.next();

                // iterate on cells for the current row
                Iterator<Cell> cellIterator = row.cellIterator();

                Map<String, Object> map = new HashMap<>();
                map.put("name", cellIterator.next().getStringCellValue());
                map.put("surname", cellIterator.next().getStringCellValue());
                map.put("ssn", cellIterator.next().getStringCellValue());
                map.put("lastDate", cellIterator.next().getStringCellValue());
                map.put("isSspPayer", "Սոց. վճար կատարող չէ".equals(cellIterator.next().getStringCellValue()));
                list.add(map);
            }
            list = null;
        } catch(IOException ex){

        }
    }
}
