package am.vector.payroll.api.service;

import am.vector.common.persistence.BaseService;
import am.vector.payroll.api.entity.main.Absence;
import am.vector.payroll.api.entity.main.embedded.SalaryAverage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

public interface AbsenceService extends BaseService<Absence> {
    Logger logger = LogManager.getLogger();

    Map<Long, List<Absence>> findAllByCompanyIdAndPeriod(int companyId, String period);
    List<Absence> findByPeriod(String period);
    List<Absence> findByPerson(long personId);
    List<Absence> findByPersonByPeriod(long personId, String period);
    SalaryAverage getAverage(Absence absence);
//    Optional<Absence> getAbsenceWithPerson(User user, long personId);
}
