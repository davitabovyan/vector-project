package am.vector.payroll.api.v1.integration.controller.cases;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import am.vector.common.model.payroll.EmploymentModel;
import am.vector.payroll.PayrollApplication;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.Employment;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.v1.integration.controller.ControllerBaseTest;
import am.vector.payroll.util.EntityBuilder;
import am.vector.payroll.util.Mapper;
import am.vector.payroll.util.PayrollApi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PayrollApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmploymentControllerIntegrationTests extends ControllerBaseTest {

    private Employment employment;
    private Map<String, String> fields;

    @Before
    public void setup() {
        Person person = create(EntityBuilder.getPersonOne());
        employment = create(EntityBuilder.getEmploymentOne(person));
        fields = new HashMap<>();
        fields.put("*", null);
        fields.put("person","*");
    }

    @Test
    public void createEmployment_success() {
        Person person = create(EntityBuilder.getPersonTwo());
        Employment employment = EntityBuilder.getEmploymentTwo(person);
        ResponseEntity<String> response = PayrollApi.createEntity(getHeaders(), getHostAndPort(), adapter.toJson(employment), EntityObject.EMPLOYMENT);
        assertEquals("Response status on Employment creation isn't correct", HttpStatus.CREATED, response.getStatusCode());
        Employment saved = Mapper.map(GsonPayrollAdapter.GSON.fromJson(response.getBody(), EmploymentModel.class));
        addToAutoDelete(saved);
        assertTrue("Employment is not saved correctly", saved.getId() > 0);
        compareExpectedWithActual(employment, saved);
    }

    @Test
    public void createEmploymentAndPersonAtOnce_success() {
        // Setup
        // Act
        Employment employment = EntityBuilder.getEmploymentTwo(EntityBuilder.getPersonTwo());
        ResponseEntity<String> response = PayrollApi.createEntity(getHeaders(), getHostAndPort(), adapter.toJson(employment, fields), EntityObject.EMPLOYMENT);
        Employment saved = Mapper.map(GsonPayrollAdapter.GSON.fromJson(response.getBody(), EmploymentModel.class));
        addToAutoDelete(saved);
        // Assert
        assertTrue("Employment object is not created", saved.getId() > 0);
        assertTrue("Person object is not created", saved.getPerson().getId() > 0);
    }

    @Test
    public void getEmployment_success() {
        // Setup
        // Act
        Employment entity = getEmployment(employment.getId());
        // Assert
        compareExpectedWithActual(employment, entity);
    }

    @Test
    public void updateEmployment_success() {
        // Setup
        Person person = create(EntityBuilder.getPersonTwo());
        Employment expected = employment.copy()
                .setPerson(person)
                .setHireDate(LocalDate.of(2019, Month.APRIL, 1))
                .setTerminationDate(LocalDate.of(2019, Month.DECEMBER, 31))
                .setVacationBalance(30);
        expected.setId(employment.getId());
        expected.setState(RecordState.CURRENT);

        // Act
        ResponseEntity<String> updated = update(expected);

        // Assert
        assertEquals("Response status on Employment update isn't correct", 200, updated.getStatusCodeValue());
        Employment actual = getEmployment(employment.getId());
        compareExpectedWithActual(expected, actual);
    }

    @Test
    public void updateEmployment_shouldThrowNotAllowedForUserException() {
        // Setup
        // Act
        ResponseEntity<String> updated =
                PayrollApi.update(getViewerHeaders(), getHostAndPort(), adapter.toJson(employment), employment.getId(), employment.getObjCode());
        // Assert
        testUserAuthority(updated, EntityObject.EMPLOYMENT, HttpMethod.PUT);
    }

    @Test
    public void editEmploymentSingleField_success() {
        // Setup
        // Act
        HttpStatus responseStatus = PayrollApi.updateFields(
                getHeaders(), getHostAndPort(), "{\"vacationBalance\": 1}", employment.getId(), employment.getObjCode()).getStatusCode();
        // Assert
        assertEquals("Response status on Employment update isn't correct", HttpStatus.OK, responseStatus);
        Employment actual = getEmployment(employment.getId());
        compareExpectedWithActual(employment.setVacationBalance(1), actual);
    }

    private void compareExpectedWithActual(Employment expected, Employment actual){
        assertEquals("Employment hire date isn't saved/retrieved correctly",
                expected.getHireDate(), actual.getHireDate());
        assertEquals("Employment termination date isn't saved/retrieved correctly",
                expected.getTerminationDate(), actual.getTerminationDate());
        assertEquals("Employment vacationBalance isn't saved/retrieved correctly",
                expected.getVacationBalance(), actual.getVacationBalance());
        assertEquals("Employment head isn't saved/retrieved correctly",
                expected.getPerson().getId(), actual.getPerson().getId());
    }
}
