use vector_security;

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS client;

CREATE TABLE client
(
    id MEDIUMINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(50) NOT NULL,
    name TEXT NOT NULL,
    cell CHAR(11) NOT NULL,
    state BIT DEFAULT 1,
    balance INT UNSIGNED DEFAULT 0,
    type ENUM('FREE','PAID','PRIME') DEFAULT 'FREE',
    companies JSON
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4 AUTO_INCREMENT=10001;

INSERT INTO client (email, name, cell) VALUES  ('askanazian.partners@gmail.com','Askanazian Partners','37491618335');

CREATE  TABLE user
(
    username VARCHAR(45) PRIMARY KEY,
    password VARCHAR(255),
    client_id MEDIUMINT UNSIGNED NOT NULL,
    company_id MEDIUMINT UNSIGNED NOT NULL,
    edit_access BIGINT NOT NULL DEFAULT 0,
    read_access BIGINT NOT NULL DEFAULT 0,
    locale VARCHAR(5),
    is_admin TINYINT NOT NULL DEFAULT 0,
    is_super_admin TINYINT NOT NULL DEFAULT 0,
    enabled TINYINT NOT NULL,
    email_verified TINYINT NOT NULL DEFAULT 0,
    INDEX user_company_idx (client_id),
    CONSTRAINT user_client_fk FOREIGN KEY (client_id) REFERENCES client (id)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

INSERT INTO user(username,password,read_access, edit_access,client_id, company_id, enabled,is_admin,is_super_admin)
VALUES ('davit.abovyan@gmail.com<00111472>','$2a$10$Gr6tvIr0E2lH2SGJYUHEcecYasPIcCCQHJEaAQL035NltcjEEZMgq', 9223372036854775807, 9223372036854775807, 10001, '10001',true,true,true);

INSERT INTO user(username,password,read_access, edit_access,client_id, company_id, enabled,is_admin,is_super_admin)
VALUES ('test@vector.am<00111472>','$2a$10$KUsGHCaM2YV1AZlnl9yUbONvioNoYh1i4JFnHm7FMXrAACvOV2Sci', 9223372036854775807, 9223372036854775806, 10001, '10001',true,true,false);

INSERT INTO user(username,password,read_access, edit_access,client_id, company_id, enabled,is_admin,is_super_admin)
VALUES ('viewer@vector.am<00111472>','$2a$10$KUsGHCaM2YV1AZlnl9yUbONvioNoYh1i4JFnHm7FMXrAACvOV2Sci', 668, 0, 10001, '10001',true,false,false);
