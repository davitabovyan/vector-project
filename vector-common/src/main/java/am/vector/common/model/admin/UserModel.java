package am.vector.common.model.admin;


import java.util.Collection;

import am.vector.common.constants.EntityObject;
import am.vector.common.util.GeneralUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Accessors(chain = true) @Getter @Setter
public class UserModel implements UserDetails {

    transient private static final EntityObject OBJ = EntityObject.USER;

    private String username;
    private String clientUuid;
    private int companyId;
    private String password;
    private long readAccess;
    private long editAccess;
    private String locale;
    private boolean isAdmin;
    private boolean isSuperAdmin;
    private boolean enabled;
    private boolean emailVerified;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public int getClientId() {
        return GeneralUtil.decodeUuidToInt(clientUuid);
    }

    public UserModel setClientId(int clientId) {
        this.clientUuid = GeneralUtil.encodeUuid(clientId);
        return this;
    }
}
