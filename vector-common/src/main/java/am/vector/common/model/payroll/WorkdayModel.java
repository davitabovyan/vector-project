package am.vector.common.model.payroll;

import am.vector.common.constants.Holiday;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class WorkdayModel {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-M-d")
    private LocalDate day;
    private DayOfWeek weekDay;
//    private DayShift dayShift;
    private Holiday holiday;
}
