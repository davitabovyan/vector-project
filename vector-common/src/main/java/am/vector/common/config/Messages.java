package am.vector.common.config;

import java.util.Arrays;
import java.util.Locale;

import javax.annotation.PostConstruct;

import am.vector.common.util.GeneralUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

@Component
public class Messages {

	private MessageSource messageSource;
	private MessageSourceAccessor accessor;

	@Autowired
	public Messages(MessageSource messageSource){
		this.messageSource = messageSource;
	}

	@PostConstruct
	@SuppressWarnings("unused")
	private void init() {
		accessor = new MessageSourceAccessor(messageSource);
	}

	public String get(String code, String args) {
		return accessor.getMessage(code, args.split(","), getUserLocale());
	}

	public String getGeneric(String args) {
		String[] parsedArgs = args.split(",");
		return accessor.getMessage(parsedArgs[0], Arrays.copyOfRange(parsedArgs,1,parsedArgs.length), getUserLocale());
	}

	public String get(String code) {
		return accessor.getMessage(code, GeneralUtil.getUserFromContext().getLocale());
	}

	private Locale getUserLocale(){
		return null == GeneralUtil.getUserFromContext().getLocale()
				? LocaleContextHolder.getLocale()
				: new Locale(GeneralUtil.getUserFromContext().getLocale());
	}
}