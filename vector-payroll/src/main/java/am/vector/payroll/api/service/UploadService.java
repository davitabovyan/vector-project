package am.vector.payroll.api.service;

import am.vector.payroll.api.entity.admin.Company;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

public interface UploadService {
    Logger logger = LogManager.getLogger();

    void uploadIncomeTaxReport(MultipartFile[] reports, Company company);
    void uploadEmployeesList(MultipartFile file);
}
