package am.vector.payroll.api.service.impl.jpa.main;

import am.vector.common.exception.VectorException;
import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Contract;
import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.entity.main.Role;
import am.vector.payroll.api.repository.admin.CompanyRepository;
import am.vector.payroll.api.repository.main.ContractRepository;
import am.vector.payroll.api.service.ContractService;
import am.vector.payroll.api.service.DepartmentService;
import am.vector.payroll.api.service.RoleService;
import am.vector.payroll.api.support.PayrollPeriod;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class ContractServiceJPA implements ContractService {

    private final RoleService roleService;
    private final DepartmentService departmentService;
    private final CompanyRepository companyRepository;
    private final ContractRepository contractRepository;


    public Contract findById(long id){
        return contractRepository.findById(id).orElseThrow(
            ()-> new EntityNotFoundException(Contract.class, GeneralUtil.encodeUuid(id))
        );
    }

    public List<Contract> getAllCurrents(int companyId, PayrollPeriod period, long departmentId){
        List<Contract> list;
        if(departmentId == 0) {
            list = contractRepository.getAllCurrentContracts(companyId, period.getFirstDay(), period.getLastDay());
        } else {
            list = contractRepository.getAllCurrentContractsByDepartment(companyId, new Department(departmentId),
                period.getFirstDay(), period.getLastDay());
        }
        return list;
    }

    @Override
    public List<Contract> setRoleByPersons(long roleId, long[] personIds) {
        Role role = roleService.findById(roleId);
        List<Contract> contracts = contractRepository.getAllContractsByPersons(GeneralUtil.getCompanyId(), personIds);
        contracts.forEach( c -> c.setRole(role));
        return contractRepository.saveAll(contracts);
    }

    @Override
    public List<Contract> setDepartmentByPersons(long departmentId, long[] personIds) {
        Department department = departmentService.findById(departmentId);
        List<Contract> contracts = contractRepository.getAllContractsByPersons(GeneralUtil.getCompanyId(), personIds);
        contracts.forEach( c -> c.setDepartment(department));
        return contractRepository.saveAll(contracts);
    }

    @Override
    public List<Contract> getFullPayrollList(int companyId, PayrollPeriod period) {
        return contractRepository.getFullPayrollList(companyId, period.getFirstDay(), period.getLastDay());
    }

    @Override
    public boolean isFiveDayWeek(long personId) {
        List<Contract> currentContracts = contractRepository.getCurrentContractByPerson(GeneralUtil.getCompanyId(), personId);
        if (currentContracts.size()>0) {
           return currentContracts.get(0).getWorkWeek() == 5;
        }
        throw new VectorException("The person has no current contract");
    }

    public List<Contract> findAllPerCompany(){
        return contractRepository.getAllByCompanyId(GeneralUtil.getCompanyId());
    }

    @Transactional
    @Override
    public void update(Contract object){
        GeneralUtil.setRecordStatus(object);
        contractRepository.save(object);
    }

    @Transactional
    @Override
    public Contract save(Contract contract){
        if(contract.getWorkWeek() == null){
            companyRepository.findById(GeneralUtil.getCompanyId())
                    .ifPresent(value -> contract.setWorkWeek(value.getWorkWeek()));
        }
        return contractRepository.save(contract);
    }

    @Override
    public List<Contract> saveAll(List<Contract> contracts) {
        return contractRepository.saveAll(contracts);
    }

    @Override
    public List<Contract> findAllById(List<Long> id) {
        return contractRepository.findAllById(id);
    }

    @Override
    public void deleteById(long id) {
        contractRepository.deleteById(id);
    }

    @Override
    public void deleteAll(int companyId) {
        contractRepository.deleteAll(companyId);
        logger.info(String.format("All record are delete by user %s", GeneralUtil.getUserFromContext().getUsername()));
    }
}
