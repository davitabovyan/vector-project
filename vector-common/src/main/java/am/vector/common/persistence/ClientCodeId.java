package am.vector.common.persistence;

import am.vector.common.constants.EntityObject;

public abstract class ClientCodeId extends UUIDEntity {

    public ClientCodeId(EntityObject object) {
        super(object);
    }

    public ClientCodeId(EntityObject object, long id) {
        super(object, id);
    }

    public int getCompanyId() {
        return (int) (this.getId() / BaseService.ID_LIMIT);
    }
}
