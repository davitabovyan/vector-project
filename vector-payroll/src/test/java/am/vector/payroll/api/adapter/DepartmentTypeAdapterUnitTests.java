package am.vector.payroll.api.adapter;

import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.entity.main.Person;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class DepartmentTypeAdapterUnitTests extends BaseTypeAdapterUnitTest<DepartmentTypeAdapter, Department> {

    private static Department department;
    private DepartmentTypeAdapter adapter;

    @BeforeClass
    public static void setup(){
        department = new Department()
            .setCode("0003")
            .setName("Accounting")
            .setCostCenter("7134");
    }

    @Before
    public void beforeTest() throws Exception {
        adapter = new DepartmentTypeAdapter();
        prepare(Arrays.asList(Department.class, Person.class), adapter, department, Collections.emptySet());
    }


    @Test
    public void write_OnlySelectedFieldsAreReturned() throws Exception {
        new DepartmentTypeAdapter("code,costCenter").write(writer, department);

        verify(writer).name("code");
        verify(writer).name("costCenter");
        verify(writer, never()).name("name");
    }

    @Test
    public void write_correctValuesAreReturned() throws Exception {
        adapter.write(writer, department);

        verify(writer).value(ArgumentMatchers.eq(department.getCode()));
        verify(writer).value(ArgumentMatchers.eq(department.getCostCenter()));
        verify(writer).value(ArgumentMatchers.eq(department.getName()));
    }
}
