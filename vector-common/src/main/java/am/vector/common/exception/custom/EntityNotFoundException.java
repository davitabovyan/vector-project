package am.vector.common.exception.custom;

import am.vector.common.exception.VectorException;

public class EntityNotFoundException extends VectorException {

    public EntityNotFoundException(Class clazz, String id) {
        super(clazz.getSimpleName() + "," + id);
    }
}

