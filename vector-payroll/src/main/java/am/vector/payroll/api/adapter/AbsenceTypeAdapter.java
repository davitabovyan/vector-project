package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.main.Absence;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class AbsenceTypeAdapter extends CustomAdapter<Absence> {

    public AbsenceTypeAdapter(String fields){
        super(Absence.class, fields);
    }

    public AbsenceTypeAdapter(Map<String, String> fields){
        super(fields);
    }


    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, Absence absence, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("leaveType")) jsonWriter.name("leaveType").value(absence.getLeaveType().name());
        if(all || fields.containsKey("amount")) jsonWriter.name("amount").value(absence.getAmount());
        if(all || fields.containsKey("period")) jsonWriter.name("period").value(absence.getPeriod());
        if(all || fields.containsKey("startDate")) parseDate("startDate", jsonWriter,absence.getStartDate());
        if(all || fields.containsKey("endDate")) parseDate("endDate",jsonWriter,absence.getEndDate());
        if(all || fields.containsKey("isParent")) jsonWriter.name("isParent").value(absence.isParent());
        String parentId = absence.getParentId() != null ? GeneralUtil.encodeUuid(absence.getParentId()) : null;
        if(all || fields.containsKey("parentId")) jsonWriter.name("parentId").value(parentId);

        if(all || fields.containsKey("person")){
            jsonWriter.name("person");
            new PersonTypeAdapter(fields.getOrDefault("person", null))
                    .write(jsonWriter, absence.getPerson());
        }

        return jsonWriter;
    }
}
