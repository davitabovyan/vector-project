package am.vector.payroll.api.v1.integration.controller.cases;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import am.vector.payroll.PayrollApplication;
import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.v1.integration.controller.ControllerBaseTest;
import am.vector.payroll.util.EntityBuilder;
import am.vector.payroll.util.PayrollApi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PayrollApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DepartmentControllerIntegrationTests extends ControllerBaseTest {

    private Department departmentOne = EntityBuilder.getDepartmentOne();

    @Before
    public void setup() {
        Person person = create(EntityBuilder.getPersonOne());
        departmentOne.setHead(person);
    }

    @Test
    public void createDepartment_success() {
        testCreate(departmentOne, adapter.toJson(departmentOne));
    }

    @Test
    public void createDepartmentDetails_success() {
        // Setup
        // Act
        Department response = create(departmentOne);
        // Assert
        compareExpectedWithActual(departmentOne, response);
    }

    @Test
    public void getDepartment_success() {
        // Setup
        long id = create(departmentOne).getId();
        // Act
        Department department = getDepartment(id);
        // Assert
        compareExpectedWithActual(departmentOne, department);
//        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    @Test
    public void updateDepartment_success() {
        // Setup
        long id = create(departmentOne).getId();
        Person person = create(EntityBuilder.getPersonTwo());
        Department expected = departmentOne.copy()
                .setHead(person)
                .setCode("9003")
                .setName("_Accounting")
                .setCostCenter("9134");
        expected.setId(id);
        expected.setState(RecordState.CURRENT);

        // Act
        ResponseEntity<String> response = update(expected);

        // Assert
        assertEquals("Response status on Department update isn't correct", 200, response.getStatusCodeValue());
        Department actual = getDepartment(id);
        compareExpectedWithActual(expected, actual);
   }

    @Test
    public void updateDepartment_shouldThrowNotAllowedForUserException() {
        // Setup
        Department department = create(departmentOne);
        // Act
        ResponseEntity<String> response =
                PayrollApi.update(getViewerHeaders(), getHostAndPort(), adapter.toJson(department), department.getId(), department.getObjCode());
        // Assert
        testUserAuthority(response, EntityObject.DEPARTMENT, HttpMethod.PUT);
    }

    @Test
    public void editDepartmentSingleField_success() {
        // Setup
        Department department = create(departmentOne);
        // Act
        HttpStatus responseStatus = PayrollApi.updateFields(
                getHeaders(), getHostAndPort(), "{\"code\":\"1111\"}", department.getId(), department.getObjCode()).getStatusCode();
        // Assert
        assertEquals("Response status on Department update isn't correct", HttpStatus.OK, responseStatus);
        Department actual = getDepartment(department.getId());
        compareExpectedWithActual(department.setCode("1111"), actual);
    }

    private void compareExpectedWithActual(Department expected, Department actual){
        assertEquals("Department code isn't saved/retrieved correctly", expected.getCode(), actual.getCode());
        assertEquals("Department name isn't saved/retrieved correctly", expected.getName(), actual.getName());
        assertEquals("Department cost center isn't saved/retrieved correctly", expected.getCostCenter(), actual.getCostCenter());
        assertEquals("Department head isn't saved/retrieved correctly", expected.getHead().getId(), actual.getHead().getId());
    }
}
