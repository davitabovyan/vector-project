package am.vector.common.model.payroll;

import am.vector.common.constants.EntityObject;
import am.vector.common.model.MainModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Accessors(chain = true) @Getter @Setter
public class EmploymentModel extends MainModel {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-M-d")
    private LocalDate hireDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-M-d")
    private LocalDate terminationDate;
    private Integer vacationBalance;
    private PersonModel person;

    public EmploymentModel() {
        super(EntityObject.EMPLOYMENT);
    }

    public EmploymentModel(String uuid) {
        super(EntityObject.EMPLOYMENT, uuid);
    }

    public EmploymentModel copy(){
        return (EmploymentModel) super.copy(this);
    }
}
