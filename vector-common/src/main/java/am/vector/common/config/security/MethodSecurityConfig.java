package am.vector.common.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;

@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = false,     // enables @pre/@post annotations
        securedEnabled = true,      // enables @Secured annotation
        jsr250Enabled = false       // enables @RoleAllowed annotation
)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
}

