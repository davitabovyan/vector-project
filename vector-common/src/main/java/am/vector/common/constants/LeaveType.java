package am.vector.common.constants;

public enum LeaveType {
    VACATION(25),
    SICK_LEAVE(26),
    MATERNITY_LEAVE(27),
    UNPAID_LEAVE(28),
    WEEKEND(29),
    EMPLOYMENT_NOT_STARTED(30),
    BANK_HOLIDAY(31);

    private final int code;

    LeaveType(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }
}
