package am.vector.common.constants;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import am.vector.common.model.admin.*;
import am.vector.common.model.journal.AccountGroupModel;
import am.vector.common.model.journal.AccountModel;
import am.vector.common.model.payroll.*;
import am.vector.common.model.common.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EntityObjectUnitTests {

    @Test
    public void enumContainsAllElementsTest() {
        // given
        Set<EntityObject> expected = new HashSet<>(
            Arrays.asList(
            EntityObject.COMPANY, EntityObject.USER, EntityObject.PERSON, EntityObject.DEPARTMENT, EntityObject.ROLE,
            EntityObject.CONTRACT, EntityObject.ABSENCE, EntityObject.EMPLOYMENT, EntityObject.PAYSLIP,
            EntityObject.TIMESHEET, EntityObject.UPLOAD, EntityObject.WORKDAY, EntityObject.ACCOUNT, EntityObject.ACCOUNT_GROUP)
        );
        // when
        Set<EntityObject> result = new HashSet<>(Arrays.asList(EntityObject.values()));
        // then
        assertEquals("Missing object in enum", expected, result);
    }

    @Test
    public void objectProperties_COMPANY_test() {
        // given
        EntityObject object = EntityObject.COMPANY;
        // then
        assertEquals("Enum index is incorrect", 0, object.getIndex());
        assertEquals("Enum access is incorrect", 0b1L, object.getAccess());
        assertEquals("Enum class is incorrect", CompanyModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_USER_test() {
        // given
        EntityObject object = EntityObject.USER;
        // then
        assertEquals("Enum index is incorrect", 1, object.getIndex());
        assertEquals("Enum access is incorrect", 0b10L, object.getAccess());
        assertEquals("Enum class is incorrect", UserModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_PERSON_test() {
        // given
        EntityObject object = EntityObject.PERSON;
        // then
        assertEquals("Enum index is incorrect", 2, object.getIndex());
        assertEquals("Enum access is incorrect", 0b100L, object.getAccess());
        assertEquals("Enum class is incorrect", PersonModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_DEPARTMENT_test() {
        // given
        EntityObject object = EntityObject.DEPARTMENT;
        // then
        assertEquals("Enum index is incorrect", 3, object.getIndex());
        assertEquals("Enum access is incorrect", 0b1000L, object.getAccess());
        assertEquals("Enum class is incorrect", DepartmentModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_ROLE_test() {
        // given
        EntityObject object = EntityObject.ROLE;
        // then
        assertEquals("Enum index is incorrect", 4, object.getIndex());
        assertEquals("Enum access is incorrect", 0b10000L, object.getAccess());
        assertEquals("Enum class is incorrect", RoleModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_CONTRACT_test() {
        // given
        EntityObject object = EntityObject.CONTRACT;
        // then
        assertEquals("Enum index is incorrect", 5, object.getIndex());
        assertEquals("Enum access is incorrect", 0b100000L, object.getAccess());
        assertEquals("Enum class is incorrect", ContractModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_ABSENCE_test() {
        // given
        EntityObject object = EntityObject.ABSENCE;
        // then
        assertEquals("Enum index is incorrect", 6, object.getIndex());
        assertEquals("Enum access is incorrect", 0b1000000L, object.getAccess());
        assertEquals("Enum class is incorrect", AbsenceModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_EMPLOYMENT_test() {
        // given
        EntityObject object = EntityObject.EMPLOYMENT;
        // then
        assertEquals("Enum index is incorrect", 7, object.getIndex());
        assertEquals("Enum access is incorrect", 0b10000000L, object.getAccess());
        assertEquals("Enum class is incorrect", EmploymentModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_PAYSLIP_test() {
        // given
        EntityObject object = EntityObject.PAYSLIP;
        // then
        assertEquals("Enum index is incorrect", 8, object.getIndex());
        assertEquals("Enum access is incorrect", 0b100000000L, object.getAccess());
        assertEquals("Enum class is incorrect", PayslipModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_TIMESHEET_test() {
        // given
        EntityObject object = EntityObject.TIMESHEET;
        // then
        assertEquals("Enum index is incorrect", 9, object.getIndex());
        assertEquals("Enum access is incorrect", 0b1000000000L, object.getAccess());
        assertEquals("Enum class is incorrect", TimeSheetModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_WORKDAY_test() {
        // given
        EntityObject object = EntityObject.WORKDAY;
        // then
        assertEquals("Enum index is incorrect", 11, object.getIndex());
        assertEquals("Enum access is incorrect", 0b100000000000L, object.getAccess());
        assertEquals("Enum class is incorrect", WorkdayModel.class, object.getClazz());
    }


    @Test
    public void objectProperties_ACCOUNT_test() {
        // given
        EntityObject object = EntityObject.ACCOUNT;
        // then
        assertEquals("Enum index is incorrect", 12, object.getIndex());
        assertEquals("Enum access is incorrect", 0b1000000000000L, object.getAccess());
        assertEquals("Enum class is incorrect", AccountModel.class, object.getClazz());
    }

    @Test
    public void objectProperties_ACCOUNT_GROUP_test() {
        // given
        EntityObject object = EntityObject.ACCOUNT_GROUP;
        // then
        assertEquals("Enum index is incorrect", 13, object.getIndex());
        assertEquals("Enum access is incorrect", 0b10000000000000L, object.getAccess());
        assertEquals("Enum class is incorrect", AccountGroupModel.class, object.getClazz());
    }
}
