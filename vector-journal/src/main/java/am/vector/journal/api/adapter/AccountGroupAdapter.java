package am.vector.journal.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.journal.api.entity.AccountGroup;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class AccountGroupAdapter extends CustomAdapter<AccountGroup> {

    public AccountGroupAdapter(String fields){
        super(AccountGroup.class, fields);
    }

    public AccountGroupAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, AccountGroup group, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("name")) jsonWriter.name("name").value(group.getName());
        if(all || fields.containsKey("type")) jsonWriter.name("type").value(group.getType().name());

        return jsonWriter;
    }
}
