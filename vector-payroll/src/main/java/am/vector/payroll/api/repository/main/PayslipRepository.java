package am.vector.payroll.api.repository.main;

import am.vector.common.persistence.BaseRepository;
import am.vector.payroll.api.entity.main.Payslip;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "payslip", path = "payslip", exported = false)
public interface PayslipRepository extends BaseRepository<Payslip, Long> {

    @Query("SELECT p FROM Payslip p JOIN FETCH p.person pr where p.id = :id")
    Optional<Payslip> getPayslipWithPerson(@Param("id") long id);

    @Query("SELECT p FROM Payslip p JOIN FETCH p.person pr where p.companyId = :companyId and p.period = :period")
    List<Payslip> getPayslipsByPeriodWithPerson(@Param("companyId") int companyId, @Param("period") String period);

    @Query("SELECT p FROM Payslip p where p.companyId = :companyId and p.period = :period")
    List<Payslip> getPayslipsByPeriod(@Param("companyId") int companyId, @Param("period") String period);

    @Query(value = "SELECT * FROM payslip as p where p.person_id = :personId ORDER BY period DESC LIMIT 16", nativeQuery = true)
    List<Payslip> getPayslipsByPerson(@Param("personId") long personId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Payslip p where p.id = :id")
    void deleteById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Payslip a where a.companyId = :companyId")
    void deleteAll(@Param("companyId") int companyId);
}
