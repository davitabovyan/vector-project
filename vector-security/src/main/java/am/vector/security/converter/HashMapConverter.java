package am.vector.security.converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.lang.reflect.Type;
import java.util.Map;

@Converter
public class HashMapConverter implements AttributeConverter<Map<String, Object>, String> {

    private static Gson GSON = new Gson();
    private static Type empMapType = new TypeToken<Map<String, Integer>>() {}.getType();

    @Override
    public String convertToDatabaseColumn(Map<String, Object> companies) {
        return GSON.toJson(companies);
    }

    @Override
    public Map<String, Object> convertToEntityAttribute(String companies) {
        return GSON.fromJson(companies, empMapType);
    }

}
