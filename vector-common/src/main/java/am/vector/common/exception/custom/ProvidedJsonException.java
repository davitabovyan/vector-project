package am.vector.common.exception.custom;

import am.vector.common.exception.VectorException;

public class ProvidedJsonException extends VectorException {
    public ProvidedJsonException(String message) {
        super(message);
    }
    public ProvidedJsonException(String message, Throwable cause) {
        super(message, cause);
    }
}
