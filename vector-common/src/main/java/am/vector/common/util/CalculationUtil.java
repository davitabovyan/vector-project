package am.vector.common.util;

import am.vector.common.constants.Holiday;
import org.apache.poi.ss.usermodel.DateUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;

public class CalculationUtil {

    public static BigDecimal calculateAverage(List<Long> numbers) {
        long total = 0;
        for (Long number : numbers) {
            total += number;
        }
        return BigDecimal.valueOf(total).divide(BigDecimal.valueOf(numbers.size()),2, RoundingMode.HALF_EVEN);
    }

    public static long numberOfWorkDays(LocalDate start, LocalDate end, boolean isFiveDay) {
        return start.datesUntil(end.plusDays(1))
                .filter(d->d.getDayOfWeek() != DayOfWeek.SUNDAY)
                .filter(d-> !isFiveDay || d.getDayOfWeek() != DayOfWeek.SATURDAY)
                .filter(d->!isHoliday(d))
                .count();
    }

    public static Holiday getHolidayStatus(LocalDate date){
        Month month = date.getMonth();
        int day = date.getDayOfMonth();

        if(month == Month.JANUARY){
            if(day == 1) return Holiday.JAN01;
            else if(day == 2) return Holiday.JAN02;
            else if(day == 3) return Holiday.JAN03;
            else if(day == 4) return Holiday.JAN04;
            else if(day == 5) return Holiday.JAN05;
            else if(day == 6) return Holiday.JAN06;
            else if(day == 7) return Holiday.JAN07;
            else if(day == 28) return Holiday.JAN28;
            else return Holiday.NONE;
        } else if(month == Month.MARCH){
            if(day==8) return Holiday.MAR08;
            else return Holiday.NONE;
        } else if(month == Month.APRIL){
            if(day==24) return Holiday.APR24;
            else return Holiday.NONE;
        } else if(month == Month.MAY){
            if(day==1) return Holiday.MAY01;
            else if(day==9) return Holiday.MAY09;
            else if(day==28) return Holiday.MAY28;
            else return Holiday.NONE;
        } else if(month == Month.JULY){
            if(day==5) return Holiday.JUL05;
            else return Holiday.NONE;
        } else if(month == Month.SEPTEMBER){
            if(day==21) return Holiday.SEP21;
            else return Holiday.NONE;
        } else if(month == Month.DECEMBER){
            if(day==31) return Holiday.DEC31;
            else return Holiday.NONE;
        } else return Holiday.NONE;
    }

    public static boolean isHoliday(LocalDate date) {
        return getHolidayStatus(date) != Holiday.NONE;
    }
}
