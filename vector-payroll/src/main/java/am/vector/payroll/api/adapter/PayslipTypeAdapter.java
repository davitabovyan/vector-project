package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.payroll.api.entity.main.Payslip;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class PayslipTypeAdapter extends CustomAdapter<Payslip> {

    public PayslipTypeAdapter(String fields){
        super(Payslip.class, fields);
    }

    public PayslipTypeAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, Payslip payslip, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("period")) jsonWriter.name("period").value(payslip.getPeriod());
        if(all || fields.containsKey("wage")) jsonWriter.name("wage").value(payslip.getWage());
        if(all || fields.containsKey("overtime")) jsonWriter.name("overtime").value(payslip.getOvertime());
        if(all || fields.containsKey("bonus")) jsonWriter.name("bonus").value(payslip.getBonus());
        if(all || fields.containsKey("ssp")) jsonWriter.name("ssp").value(payslip.getSsp());
        if(all || fields.containsKey("it")) jsonWriter.name("it").value(payslip.getIt());
        if(all || fields.containsKey("army")) jsonWriter.name("army").value(payslip.getArmy());
        if(all || fields.containsKey("hasUpdate")) jsonWriter.name("hasUpdate").value(payslip.hasUpdate());

        if(all || fields.containsKey("person")){
            jsonWriter.name("person");
            new PersonTypeAdapter(fields.getOrDefault("person", null))
                    .write(jsonWriter, payslip.getPerson());
        }

        return jsonWriter;
    }
}
