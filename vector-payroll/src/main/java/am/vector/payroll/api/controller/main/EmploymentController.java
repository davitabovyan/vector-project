package am.vector.payroll.api.controller.main;

import am.vector.common.model.payroll.EmploymentModel;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.Employment;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.service.EmploymentService;
import am.vector.payroll.api.service.PersonService;
import am.vector.payroll.util.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/employment", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class EmploymentController {

    private final EmploymentService employmentService;
    private final PersonService personService;
    private final GsonPayrollAdapter adapter;


    @Transactional
    @PostMapping
    public ResponseEntity<String> createNested(@RequestBody EmploymentModel model){
        Employment employment = Mapper.map(model);
        Person person = employment.getPerson();
        if (person.getId() != 0) {
            person = personService.findById(GeneralUtil.decodeUuid(person.getUuid()));
        } else {
            person.setCompanyId(GeneralUtil.getCompanyId());
            person = personService.save(person);
        }
        employment.setPerson(person);
        employment = employmentService.save(employment);
        return new ResponseEntity<>(adapter.toJson(employment), HttpStatus.CREATED);

    }

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findByIdCompact(@PathVariable String uuid,
                                          @RequestParam(value = "fields", required = false) String fields){
        Employment response = employmentService.findById(GeneralUtil.decodeUuid(uuid));
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @GetMapping("/")
    public ResponseEntity<String> findAll(@RequestParam(value = "includeTerminated", defaultValue = "false") boolean includeTerminated,
                                          @RequestParam(value = "fields", required = false) String fields){
        List<Employment> response = employmentService.findAll(includeTerminated);
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @PutMapping("/{uuid}")
    public void update(@RequestBody EmploymentModel model, @PathVariable String uuid) {
        Employment employment = Mapper.map(model);
        employment.setId(GeneralUtil.decodeUuid(uuid));
        employment.setCompanyId(GeneralUtil.getCompanyId());
        employmentService.update(employment);
    }

    @GetMapping("/all/{personId}")
    public ResponseEntity<String> findAllCompact(@PathVariable String personId,
                                                 @RequestParam(value = "fields", required = false) String fields){
        List<Employment> response = employmentService.findAllPerPerson(GeneralUtil.decodeUuid(personId));
        return ResponseEntity.ok(adapter.toJson(response, fields));
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody EmploymentModel model, @PathVariable String uuid) {
        Employment entity = employmentService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(model));
        entity.setCompanyId(GeneralUtil.getCompanyId());
        employmentService.update(entity);
    }

    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable String uuid) {
        employmentService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
