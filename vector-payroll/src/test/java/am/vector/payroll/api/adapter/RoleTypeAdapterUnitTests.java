package am.vector.payroll.api.adapter;

import am.vector.payroll.api.entity.main.Role;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.util.Collections;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class RoleTypeAdapterUnitTests extends BaseTypeAdapterUnitTest<RoleTypeAdapter, Role> {

    private static Role role;

    @BeforeClass
    public static void setup(){

        role = new Role()
            .setCode("0002")
            .setName("Accountant")
            .setDescription("The accountant")
            .setEligibleDays(20);
    }

    @Before
    public void beforeTest() throws Exception {
        RoleTypeAdapter adapter = new RoleTypeAdapter();
        prepare(Collections.singletonList(Role.class), adapter, role, Collections.singleton("eligibleDays"));
    }

    @Test
    public void write_OnlySelectedFieldsAreReturned() throws Exception {
        new RoleTypeAdapter("code,description").write(writer, role);

        verify(writer).name("code");
        verify(writer).name("description");
        verify(writer, never()).name("name");
    }

    @Test
    public void write_SecondaryFieldsAreReturnedByRequest() throws Exception {
        new RoleTypeAdapter("eligibleDays").write(writer, role);

        verify(writer).name("eligibleDays");
    }

    @Test
    public void write_correctValuesAreReturned() throws Exception {
        new RoleTypeAdapter("*,eligibleDays").write(writer, role);

        verify(writer).value(ArgumentMatchers.eq(role.getCode()));
        verify(writer).value(ArgumentMatchers.eq(role.getName()));
        verify(writer).value(ArgumentMatchers.eq(role.getDescription()));
        verify(writer).value(ArgumentMatchers.eq(role.getEligibleDays()));
    }
}
