package am.vector.payroll.api.v1.integration.controller.cases;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import am.vector.payroll.PayrollApplication;
import am.vector.payroll.api.entity.main.Contract;
import am.vector.payroll.api.entity.main.Department;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.entity.main.Role;
import am.vector.payroll.api.v1.integration.controller.ControllerBaseTest;
import am.vector.payroll.util.EntityBuilder;
import am.vector.payroll.util.PayrollApi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PayrollApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ContractControllerIntegrationTests extends ControllerBaseTest {

    private Contract contractOne = EntityBuilder.getContractOne();

    @Before
    public void setup() {
        Person person = create(EntityBuilder.getPersonOne());
        Department department = create(EntityBuilder.getDepartmentOne().setHead(person));
        Role role = create(EntityBuilder.getRoleOne());
        contractOne.setPerson(person);
        contractOne.setRole(role);
        contractOne.setDepartment(department);
    }

    @Test
    public void createContract_success() {
        testCreate(contractOne, adapter.toJson(contractOne));
    }

    @Test
    public void getContract_success() {
        // Setup
        Contract contract = create(contractOne);
        // Act
        Contract actual = getContract(contract.getId());
        // Assert
        compareExpectedWithActual(contract, actual);
//        assertEquals("Contract laborRelation is not saved correctly", contract.isLaborRelation(), con.isLaborRelation());
    }

    @Test
    public void updateContract_success() {
        // Setup
        long id = create(contractOne).getId();
        Person person = create(EntityBuilder.getPersonTwo());
        Role role = create(EntityBuilder.getRoleTwo());
        Department department = create(EntityBuilder.getDepartmentTwo().setHead(person));

        Contract expected = contractOne.copy()
                .setSalary(1_000_000)
                .setStartDate(LocalDate.parse("2012-12-12"))
                .setDailyHours(1)
                .setPerson(person)
                .setDepartment(department)
                .setRole(role);
        expected.setId(id);
        expected.setState(RecordState.CURRENT);

        // Act
        int responseStatus = update(expected).getStatusCodeValue();
        assertEquals("Response status on Contract update isn't correct", 200, responseStatus);

        Contract actual = getContract(id);
        compareExpectedWithActual(expected, actual);
    }

    @Test
    public void updateContract_shouldThrowNotAllowedForUserException() {
        // Setup
        Contract contract = create(contractOne);
        // Act
        ResponseEntity<String> response =
                PayrollApi.update(getViewerHeaders(), getHostAndPort(), adapter.toJson(contract), contract.getId(), contract.getObjCode());
        // Assert
        testUserAuthority(response, EntityObject.CONTRACT, HttpMethod.PUT);
    }

    @Test
    public void editContractSingleField_success() {
        // Setup
        Contract contract = create(contractOne);
        // Act
        HttpStatus responseStatus = PayrollApi.updateFields(
                getHeaders(), getHostAndPort(), "{\"salary\":100000}", contract.getId(), contract.getObjCode()).getStatusCode();
        // Assert
        assertEquals("Response status on Contract update isn't correct", HttpStatus.OK, responseStatus);
        Contract actual = getContract(contract.getId());
        compareExpectedWithActual(contract.setSalary(100_000), actual);
    }

    private void compareExpectedWithActual(Contract expected, Contract actual) {
        assertEquals("Contract salary isn't saved/retrieved correctly", expected.getSalary(), actual.getSalary());
        assertEquals("Contract start date isn't saved/retrieved correctly", expected.getStartDate(), actual.getStartDate());
        assertEquals("Contract end date isn't saved/retrieved correctly", expected.getEndDate(), actual.getEndDate());
        assertEquals("Contract daily hours isn't saved/retrieved correctly", expected.getDailyHours(), actual.getDailyHours());
        assertEquals("Contract person id isn't saved/retrieved correctly", expected.getPerson().getId(), actual.getPerson().getId());
        assertEquals("Contract role id isn't saved/retrieved correctly", expected.getRole().getId(), actual.getRole().getId());
    }
}
