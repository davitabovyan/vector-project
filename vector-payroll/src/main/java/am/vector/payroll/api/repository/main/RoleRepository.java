package am.vector.payroll.api.repository.main;

import am.vector.common.persistence.BaseRepository;
import am.vector.payroll.api.entity.main.Role;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "role", path = "role", exported = false)
public interface RoleRepository extends BaseRepository<Role, Long> {
    @Query("SELECT r FROM Role r where r.companyId = :companyId")
    List<Role> findAllPerCompany(@Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Role r where r.id = :id")
    void deleteById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Role r WHERE r.code = :code AND r.companyId = :companyId")
    void deleteByCode(@Param("code") String code, @Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Role a where a.companyId = :companyId")
    void deleteAll(@Param("companyId") int companyId);
}
