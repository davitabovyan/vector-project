package am.vector.payroll.api.entity.internal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.domain.Persistable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.util.Objects;

@Entity
@IdClass(TaxReportId.class)
@Accessors(chain = true) @Getter
@NoArgsConstructor
public class TaxReport implements Comparable<TaxReport>, Persistable<TaxReportId> {
    @Id
    private int period;
    @Id
    private String ssn;

    private String name;
    private int salary;
    private int service;
    private int otherIncome;
    private int deductibleIncome;
    private int socialPaymentEmployer;
    private int socialPaymentBudget;
    private int returnOfOverpaidSocial;
    private int ssp;
    private int incomeTax;
    private int workedHours;
    private int companyId;

    private transient boolean isNew;

    @Override
    public int compareTo(TaxReport taxPayslip) {
        return  taxPayslip.period - this.period;
    }

    @Override
    public TaxReportId getId() {
        return new TaxReportId(this.period, this.ssn);
    }

    @Override
    public boolean isNew() {
        return this.isNew;
    }

    public TaxReport(int period, String ssn, String name, int companyId) {
        this.period = period;
        this.ssn = ssn;
        this.name = name;
        this.companyId = companyId;
        this.isNew = true;
    }

    public TaxReport(int period, String ssn, String name, int companyId, int salary, int service, int otherIncome, int deductibleIncome,
                     int socialPaymentEmployer, int socialPaymentBudget, int returnOfOverpaidSocial, int incomeTax, int ssp, int workedHours) {
        this.period = period;
        this.ssn = ssn;
        this.name = name;
        this.salary = salary;
        this.service = service;
        this.otherIncome = otherIncome;
        this.deductibleIncome = deductibleIncome;
        this.socialPaymentEmployer = socialPaymentEmployer;
        this.socialPaymentBudget = socialPaymentBudget;
        this.returnOfOverpaidSocial = returnOfOverpaidSocial;
        this.ssp = ssp;
        this.incomeTax = incomeTax;
        this.workedHours = workedHours;
        this.companyId = companyId;
        this.isNew = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaxReport payslip = (TaxReport) o;
        return period == payslip.period &&
                salary == payslip.salary &&
                service == payslip.service &&
                otherIncome == payslip.otherIncome &&
                deductibleIncome == payslip.deductibleIncome &&
                socialPaymentEmployer == payslip.socialPaymentEmployer &&
                socialPaymentBudget == payslip.socialPaymentBudget &&
                returnOfOverpaidSocial == payslip.returnOfOverpaidSocial &&
                incomeTax == payslip.incomeTax &&
                ssp == payslip.ssp &&
                workedHours == payslip.workedHours;
    }

    @Override
    public int hashCode() {
        return Objects.hash(period, salary, service, otherIncome, deductibleIncome, socialPaymentEmployer, socialPaymentBudget,
                returnOfOverpaidSocial, incomeTax, ssp, workedHours);
    }
}
