package am.vector.payroll.api.entity.main;


import am.vector.common.constants.EntityObject;
import am.vector.common.constants.Gender;
import am.vector.common.persistence.TimestampId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Accessors(chain = true) @Getter @Setter
public class Person extends TimestampId {

    private String ssn;
    private String code;
    private String firstName;
    private String lastName;
    private String middleName;
    private String email;
    private String cell;
    private LocalDate birthday;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    public void assignGender(String gender){
        this.gender = Gender.valueOf(gender);
    }

    public Person() {
        super(EntityObject.PERSON);
    }

    public Person(long uuid) {
        super(EntityObject.PERSON, uuid);
    }

    public Person copy(){
        return (Person) super.copy(this);
    }
}
