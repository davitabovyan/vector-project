package am.vector.common.exception.generic;

import am.vector.common.exception.VectorException;

public class ConflictVectorException extends VectorException {
    public ConflictVectorException(String... keys) {
        super(keys);
    }
}

