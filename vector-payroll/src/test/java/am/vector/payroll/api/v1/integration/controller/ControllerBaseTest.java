package am.vector.payroll.api.v1.integration.controller;

import am.vector.common.adapter.GsonAdapter;
import am.vector.common.constants.EntityObject;
import am.vector.common.persistence.UUIDEntity;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.Pair;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.*;
import am.vector.payroll.util.PayrollApi;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;
import java.util.Stack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class ControllerBaseTest {

    private static final Stack<Pair<EntityObject, Long>> autoDelete = new Stack<>();
    private static HttpHeaders headers;

    protected GsonPayrollAdapter adapter = new GsonPayrollAdapter();

    @LocalServerPort
    private int port;

    @Value("${values.test.users.admin}")
    private String admin;

    @Value("${values.test.users.viewer}")
    private String viewer;

    @Value("${values.test.users.password}")
    private String password;

    @BeforeClass
    public static void start() {
        PayrollApi.setTestApi();
    }

    @After
    public void tearDown() {
        doAutoDelete(getHeaders(), getHostAndPort());
    }

    protected <T extends UUIDEntity> void testCreate(T entity, String body){
        // Act
        Pair<Integer, String> response =
                PayrollApi.create(getHeaders(), getHostAndPort(), body, entity.getObjCode());
        addToAutoDelete(entity, response);
        // Assert
        assertEquals("Response status on Entity creation isn't correct",
                201, (int) response.getFirst());
        assertTrue(String.format("%s is not saved correctly",entity.getObjCode().getCode()),
                GeneralUtil.isValidUuid(response.getSecond(), entity.getObjCode()));
    }

    protected void testUserAuthority(ResponseEntity<String> response, EntityObject entityObject, HttpMethod method){
        assertEquals("Response code is not correct",
                HttpStatus.FORBIDDEN, response.getStatusCode());
        assertEquals("Exception message is not correct",
                String.format("Not allowed to apply \"%s\" action on \"%s\" object", method.name(), entityObject.getCode()), response.getBody());
    }

    public List<Person> create(List<Person> persons){
        List<Person> entities = PayrollApi.createPersons(getHeaders(), getHostAndPort(), persons);
        addToAutoDelete(entities);
        return entities;
    }

    public <T extends UUIDEntity> T create(T entity){
        T createdEntity = PayrollApi.createAndReturn(
                getHeaders(),
                getHostAndPort(),
                adapter.toJson(entity),
                entity
        );
        addToAutoDelete(createdEntity);
        return createdEntity;
    }

    public <T extends UUIDEntity> T create(T entity, Map<String, String> fields){
        T createdEntity = PayrollApi.createAndReturn(
                getHeaders(),
                getHostAndPort(),
                adapter.toJson(entity, fields),
                entity
        );
        addToAutoDelete(createdEntity);
        return createdEntity;
    }

    public <T extends UUIDEntity> ResponseEntity<String> update(T entity){
        return PayrollApi.update(getHeaders(), getHostAndPort(), adapter.toJson(entity), entity.getId(), entity.getObjCode());
    }

    public <T extends UUIDEntity> ResponseEntity<String> update(T entity, Map<String, String> fields){
        return PayrollApi.update(getHeaders(), getHostAndPort(), adapter.toJson(entity, fields), entity.getId(), entity.getObjCode());
    }

    public Person getPerson(long id){
        return PayrollApi.getPerson(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id));
    }

    public Person getPerson(long id, Map<String, String> fields){
        return PayrollApi.getPerson(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id), fields);
    }

    public Contract getContract(long id){
        return PayrollApi.getContract(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id));
    }

    public Employment getEmployment(long id){
        return PayrollApi.getEmployment(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id));
    }

    public Department getDepartment(long id){
        return PayrollApi.getDepartment(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id));
    }

    public Payslip getPayslip(long id){
        return PayrollApi.getPayslip(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id));
    }

    public Role getRole(long id){
        return PayrollApi.getRole(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id));
    }

    public Role getRole(long id, Map<String, String> map){
        return PayrollApi.getRole(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id), map);
    }

    public Absence getAbsence(long id){
        return PayrollApi.getAbsence(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id));
    }

    public TimeSheet getTimesheet(long id){
        return PayrollApi.getTimesheet(getHeaders(), getHostAndPort(), GeneralUtil.encodeUuid(id));
    }

    public <T extends UUIDEntity> Pair<T, HttpStatus> getResponse(long id, EntityObject objCode, Class<T> clazz){
        ResponseEntity<String> response = PayrollApi.getEntityById(getHeaders(), getHostAndPort(), id, objCode);
        return new Pair<>(GsonAdapter.GSON.fromJson(response.getBody(), clazz), response.getStatusCode());
    }

    protected String getHostAndPort() {
        return "http://localhost:" + port + "/";
    }

    protected HttpHeaders getHeaders() {
        if(headers == null){
            String admin_token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlYzgyNTNlYS1kNDA4LTQzZGYtYWZhYi05NGRkODRmZDVjZDciLCJzYSI6ZmFsc2UsImFkIjp0cnVlLCJyZCI6OTIyMzM3MjAzNjg1NDc3NTgwNywiZWQiOjkyMjMzNzIwMzY4NTQ3NzU4MDYsImNwIjoiNDBjZGJjIiwic3ViIjoidGVzdEB2ZWN0b3IuYW08MDAxMTE0NzI-IiwiZXhwIjoxNjAxODg2MTEwfQ.KgchFvXamuXWRKBmrPm0SC8AlVlE3egbqrhALj7pcsU";
            headers = createHeaders(admin_token);
        }
        return headers;
    }

    protected HttpHeaders getViewerHeaders(){
        String viewer_token = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJmYzVhYzIwMy1kNzZlLTQ4MzYtYWVlMC0zZDJmNjQ3ZWU2MDQiLCJzYSI6ZmFsc2UsImFkIjpmYWxzZSwicmQiOjY2OCwiZWQiOjAsImNwIjoiNDBjZGJjIiwic3ViIjoidmlld2VyQHZlY3Rvci5hbTwwMDExMTQ3Mj4iLCJleHAiOjE2MDE4ODYxNjN9.rm5w3dOs9EbblfllTHLcsWVO0LLEj5j-_g-VY7a-xfo";
        return createHeaders(viewer_token);
    }

    private HttpHeaders createHeaders(String token){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + token);
        httpHeaders.add("Content-Type", "application/json");
        return httpHeaders;
    }

    protected void addToAutoDelete(UUIDEntity entity){
        autoDelete.push(new Pair<>(entity.getObjCode(), entity.getId()));
    }

    private void addToAutoDelete(UUIDEntity entity, Pair<Integer, String> response){
        autoDelete.push(new Pair<>(entity.getObjCode(), GeneralUtil.decodeUuid(response.getSecond())));
    }

    private void addToAutoDelete(List<? extends UUIDEntity> entities){
        for(UUIDEntity entity : entities) {
            autoDelete.push(new Pair<>(entity.getObjCode(), entity.getId()));
        }
    }

    private void doAutoDelete(HttpHeaders headers, String host){
        while(!autoDelete.empty()){
            Pair<EntityObject, Long> record = autoDelete.pop();
            String uuid = GeneralUtil.encodeUuid(record.getSecond());
            Assert.assertEquals(record.getFirst()+ " object with id "+record.getSecond()+" is not removed by id",
                    200, PayrollApi.removeEntityById(headers, host, uuid, record.getFirst()));
        }
    }
}
