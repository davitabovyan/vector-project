package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.payroll.api.entity.main.Person;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class PersonTypeAdapter extends CustomAdapter<Person> {

    public PersonTypeAdapter(String fields){
        super(Person.class, fields);
    }

    public PersonTypeAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, Person person, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("ssn")) jsonWriter.name("ssn").value(person.getSsn());
        if(all || fields.containsKey("code")) jsonWriter.name("code").value(person.getCode());
        if(all || fields.containsKey("firstName")) jsonWriter.name("firstName").value(person.getFirstName());
        if(all || fields.containsKey("lastName")) jsonWriter.name("lastName").value(person.getLastName());
        if(all || fields.containsKey("middleName")) jsonWriter.name("middleName").value(person.getMiddleName());
        if(all || fields.containsKey("email")) jsonWriter.name("email").value(person.getEmail());
        if(all || fields.containsKey("cell")) jsonWriter.name("cell").value(person.getCell());
        if(all || fields.containsKey("birthday")) parseDate("birthday", jsonWriter, person.getBirthday());
        if(all || fields.containsKey("gender")) jsonWriter.name("gender").value(person.getGender().name());
        return jsonWriter;
    }
}
