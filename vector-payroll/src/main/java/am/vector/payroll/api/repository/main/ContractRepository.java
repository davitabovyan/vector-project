package am.vector.payroll.api.repository.main;

import am.vector.common.persistence.BaseRepository;
import am.vector.payroll.api.entity.main.Contract;
import am.vector.payroll.api.entity.main.Department;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "contract", path = "contract", exported = false)
public interface
ContractRepository extends BaseRepository<Contract, Long> {

    @Query(
            "SELECT c FROM Contract c " +
                    " JOIN FETCH c.role r JOIN FETCH c.department d JOIN FETCH c.person " +
                    " where c.companyId = :companyId " +
                    " AND (c.endDate IS NULL OR c.endDate BETWEEN :start AND :end)"
    )
    List<Contract> getFullPayrollList(@Param("companyId") int companyId, @Param("start") LocalDate start, @Param("end") LocalDate end);

    @Query("SELECT c FROM Contract c where c.companyId = :companyId AND c.person.id IN :personIds")
    List<Contract> getAllContractsByPersons(@Param("companyId") int companyId, @Param("personIds") long[] personIds);

    @Query("SELECT c FROM Contract c where c.companyId = :companyId AND c.startDate <= :endDate AND (c.endDate IS NULL OR c.endDate BETWEEN :startDate AND :endDate)")
    List<Contract> getAllCurrentContracts(@Param("companyId") int companyId, @Param("startDate") LocalDate start, @Param("endDate") LocalDate end);

    @Query("SELECT c FROM Contract c where c.companyId = :companyId AND c.person.id = :personId AND c.endDate IS NULL")
    List<Contract> getCurrentContractByPerson(@Param("companyId") int companyId, @Param("personId") long personId);

    @Query("SELECT c FROM Contract c where c.companyId = :companyId and c.department = :department AND (c.endDate IS NULL OR c.endDate BETWEEN :start AND :end)")
    List<Contract> getAllCurrentContractsByDepartment(@Param("companyId") int companyId, @Param("department") Department department, @Param("start") LocalDate start, @Param("end") LocalDate end);

    @Query("SELECT c FROM Contract c JOIN FETCH c.role r JOIN FETCH c.department d JOIN FETCH c.person " +
            " where c.companyId = :companyId ")
    List<Contract> getAllByCompanyId(@Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Contract c where c.id = :id")
    void deleteById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Contract a where a.companyId = :companyId")
    void deleteAll(@Param("companyId") int companyId);
}
