package am.vector.payroll.api.adapter;

import am.vector.common.adapter.CustomAdapter;
import am.vector.payroll.api.entity.main.Role;
import com.google.gson.stream.JsonWriter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Map;

@NoArgsConstructor
public class RoleTypeAdapter extends CustomAdapter<Role> {

    public RoleTypeAdapter(String fields){
        super(Role.class, fields);
    }

    public RoleTypeAdapter(Map<String, String> fields){
        super(fields);
    }

    @Override
    public JsonWriter writeObject(JsonWriter jsonWriter, Role role, Map<String, String> fields) throws IOException{
        boolean all = fields.containsKey("*");
        if(all || fields.containsKey("code")) jsonWriter.name("code").value(role.getCode());
        if(all || fields.containsKey("name")) jsonWriter.name("name").value(role.getName());
        if(all || fields.containsKey("description")) jsonWriter.name("description").value(role.getDescription());

        if(fields.containsKey("eligibleDays")) jsonWriter.name("eligibleDays").value(role.getEligibleDays());

        return jsonWriter;
    }
}
