package am.vector.payroll.api.repository.main;

import am.vector.common.persistence.BaseRepository;
import am.vector.payroll.api.entity.main.Employment;
import am.vector.payroll.api.entity.main.Person;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "employment", path = "employment", exported = false)
public interface EmploymentRepository extends BaseRepository<Employment, Long> {


    @Query("SELECT e FROM Employment e where e.person = :person")
    List<Employment> findAllPerPerson(@Param("person") Person person);

    @Query("SELECT e FROM Employment e JOIN FETCH e.person p where e.companyId = :companyId")
    List<Employment> findAll(@Param("companyId") int companyId);

    @Query("SELECT e FROM Employment e JOIN FETCH e.person p where e.companyId = :companyId AND e.terminationDate IS NULL")
    List<Employment> findAllCurrent(@Param("companyId") int companyId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Employment e where e.id = :id")
    void deleteById(@Param("id") long id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Employment a where a.companyId = :companyId")
    void deleteAll(@Param("companyId") int companyId);
}
