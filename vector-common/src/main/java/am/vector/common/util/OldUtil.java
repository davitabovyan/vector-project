package am.vector.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import am.vector.common.persistence.UUIDEntity;

public class OldUtil {

//	public static void createUUID(Object object){
//		if(object instanceof UUIDEntity){
//			UUIDEntity uuidEntity = (UUIDEntity) object;
//			long id =  (long) uuidEntity.getCompanyId() * 10_000_000_000_000L + System.currentTimeMillis();
//			uuidEntity.setId(id);
//		}
//	}

	public static String encodeTin(String tin){
		StringBuilder sb = new StringBuilder(8);
		for(int i=0;i<8;++i){
			if(i == 1){
				int second = (int) tin.charAt(i) -3*16 + 7;
				sb.append(second>10 ? second - 10 : second);
			} else if(i == 4){
				sb.append(tin.charAt(i));
			} else {
				int place = tin.charAt(i) + (i % 2 == 1 ? 16 * 3 + 1 : 16 * 2 - 7);
				sb.append((char) place);
			}
		}
		return sb.toString();
	}

	public static String decodeTin(String str){
		StringBuilder sb = new StringBuilder(8);
		for(int i=0;i<8;++i){
			if(i == 1){
				int second = (int) str.charAt(i) -3*16 - 7;
				sb.append(second<=0 ? second + 10 : second);
			} else if(i == 4){
				sb.append(str.charAt(i));
			} else {
				int place = str.charAt(i) - (i % 2 == 1 ? 16 * 3 + 1 : 16 * 2 - 7);
				sb.append((char) place);
			}
		}
		return sb.toString();
	}

	//	WAS USED IN SERVICE LAYER
	//
	//    @PersistenceContext
	//    private EntityManager entityManager;
	//
	//    public UUIDEntity save(UUIDEntity object){
	//        GeneralUtil.setRecordStatus(object);
	//        GeneralUtil.createUUID(object);
	//        try {
	//            Map<String, Object> map = JPAUtil.getActiveFileds(object);
	//            String sql = JPAUtil.generateInsertQueryString(map, object);
	//            Query query = entityManager.createNativeQuery(sql);
	//            for (Map.Entry<String, Object> entry: map.entrySet()) {
	//                query.setParameter(entry.getKey(), entry.getValue());
	//            }
	//            query.executeUpdate();
	//        } catch (IllegalAccessException ex){
	//            ex.printStackTrace();
	//        }
	//        return object;
	//    }

	public static String generateInsertQueryString(Map<String, Object> map, UUIDEntity entity){
		String fullName = entity.getClass().getName();
		CharSequence entityName = fullName.subSequence(fullName.lastIndexOf('.')+1, fullName.length());

		StringBuilder sb = new StringBuilder(String.format("INSERT INTO %s (", entityName));
		sb.append(map.keySet().stream().collect(Collectors.joining(",")));
		sb.append(") VALUES (:");
		sb.append(map.keySet().stream().collect(Collectors.joining(",:")));
		sb.append(")");
		return sb.toString();
	}

	public static Map<String, Object> getActiveFileds(UUIDEntity entity) throws IllegalAccessException {
		Map<String, Object> map = new HashMap<>();

		Class clazz = entity.getClass();
		if(clazz != null) {
			getFieldsNameValues(map, clazz, entity);
			while (clazz.getSuperclass() != null) {
				clazz = clazz.getSuperclass();
				getFieldsNameValues(map, clazz, entity);
			}
		} else {
			throw new IllegalAccessException("Class of entity is null");
		}

		return map;
	}

	private static void getFieldsNameValues(Map<String, Object> map, Class clazz, Object object)
		throws IllegalAccessException {

		for(Field field : clazz.getDeclaredFields()) {
			String fieldName = field.getName();
			field.setAccessible(true);
			Object fieldValue = field.get(object);
			if(fieldValue != null && !Modifier.isTransient(field.getModifiers())) {
				if(fieldValue instanceof UUIDEntity) {
					UUIDEntity has = (UUIDEntity) fieldValue;
					fieldValue = has.getId();
					fieldName+="_uuid";
				} else if(fieldValue.getClass().isEnum()){
					fieldValue = ((Enum) fieldValue).name();
				}
				map.put(fromCamelCaseToUnderscore(fieldName), fieldValue);
			}
		}
	}

	private static String fromCamelCaseToUnderscore(String text){
		StringBuilder sb = new StringBuilder(text);
		for(int i = 0; i < text.length(); ++i){
			char ch = text.charAt(i);
			if(Character.isUpperCase(ch)){
				sb.setCharAt(i, '_');
				sb.insert(i+1, Character.toLowerCase(ch));
			}
		}
		return sb.toString();
	}
}
