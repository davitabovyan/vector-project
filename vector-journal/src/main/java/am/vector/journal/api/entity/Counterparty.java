package am.vector.journal.api.entity;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.journal.CounterpartyType;
import am.vector.common.persistence.ClientCodeId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
@Accessors(chain = true) @Getter @Setter
public class Counterparty extends ClientCodeId {

    private String name;
    private String legalName;
    private CounterpartyType type;
    private String tin;
    private String registrationNumber;
    private LocalDate registrationDate;
    private String ssn;
    private String passport;
    private LocalDate issueDate;
    private String issuedBy;
    private String addressLegal;
    private String addressCurrent;
    private Boolean isPhysical;

    public Counterparty() {
        super(EntityObject.COUNTERPARTY);
    }

    public Counterparty(long uuid) {
        super(EntityObject.COUNTERPARTY, uuid);
    }

    public Counterparty copy(){
        return (Counterparty) super.copy(this);
    }
}
