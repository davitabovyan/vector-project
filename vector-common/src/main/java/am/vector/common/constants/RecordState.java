package am.vector.common.constants;

public enum RecordState {
    CURRENT,
    DELETED,
    REMOVED
}
