package am.vector.common.constants.journal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum FSItems {
    PPE("Property, plant and equipment", FSType.BALANCE_SHEET),
    INTANGIBLE("Intangible assets and goodwill", FSType.BALANCE_SHEET),
    INVESTMENT("Investment property", FSType.BALANCE_SHEET),
    DT("Deferred tax assets", FSType.BALANCE_SHEET),
    OTHER_ASSET_NC("Other Non Current Assets", FSType.BALANCE_SHEET),
    INVENTORY("Inventories", FSType.BALANCE_SHEET),
    TAX_ASSET("Current tax assets", FSType.BALANCE_SHEET),
    RECEIVABLE("Trade and other receivables", FSType.BALANCE_SHEET),
    PREPAYMENT("Prepayments", FSType.BALANCE_SHEET),
    CASH("Cash and cash equivalents", FSType.BALANCE_SHEET),
    OTHER_ASSET_C("Other Current Assets", FSType.BALANCE_SHEET),
    CAPITAL("Share capital", FSType.BALANCE_SHEET),
    EARNINGS("Retained earnings", FSType.BALANCE_SHEET),
    OTHER_EQUITY("Other Equity", FSType.BALANCE_SHEET),
    LOAN_NC("Loans and borrowings", FSType.BALANCE_SHEET),
    PAYABLE_NC("Trade and other payable", FSType.BALANCE_SHEET),
    PROVISION_NC("Provisions Non Current", FSType.BALANCE_SHEET),
    OTHER_LIABILITY_NC("Other Non Current Liabilities", FSType.BALANCE_SHEET),
    TAX_LIABILITY("Current tax liabilities", FSType.BALANCE_SHEET),
    LOAN_C("Loans and borrowings Current", FSType.BALANCE_SHEET),
    PAYABLE_C("Trade and other payable", FSType.BALANCE_SHEET),
    PROVISION_C("Provisions Current", FSType.BALANCE_SHEET),
    OTHER_LIABILITY_C("Other Current Liabilities", FSType.BALANCE_SHEET),
    REVENUE("Revenue", FSType.INCOME_STATEMENT),
    COS("Cost of sales", FSType.INCOME_STATEMENT),
    OTHER_INCOME("Other income", FSType.INCOME_STATEMENT),
    SALE_EXP("Selling and distribution expenses", FSType.INCOME_STATEMENT),
    ADMIN_EXP("Administrative expenses", FSType.INCOME_STATEMENT),
    RD("Research and development expenses", FSType.INCOME_STATEMENT),
    IMPAIRMENT("Impairment loss on trade receivables and contract assets", FSType.INCOME_STATEMENT),
    OTHER_EXP("Other expenses", FSType.INCOME_STATEMENT),
    FIN_INCOME("Finance income", FSType.INCOME_STATEMENT),
    FIN_EXP("Finance costs", FSType.INCOME_STATEMENT),
    CIT("Corporate Income tax expense", FSType.INCOME_STATEMENT);

    private String description;
    private FSType type;
}
