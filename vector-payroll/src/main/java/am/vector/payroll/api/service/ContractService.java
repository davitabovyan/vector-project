package am.vector.payroll.api.service;

import am.vector.common.persistence.CommonService;
import am.vector.payroll.api.entity.main.Contract;
import am.vector.payroll.api.support.PayrollPeriod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface ContractService extends CommonService<Contract> {
	Logger logger = LogManager.getLogger();

	List<Contract> getAllCurrents(int companyId, PayrollPeriod period, long departmentId);
	List<Contract> setRoleByPersons(long roleId, long[] personIds);
	List<Contract> setDepartmentByPersons(long departmentId, long[] personIds);
	List<Contract> getFullPayrollList(int companyId, PayrollPeriod period);
	boolean isFiveDayWeek(long personId);
}
