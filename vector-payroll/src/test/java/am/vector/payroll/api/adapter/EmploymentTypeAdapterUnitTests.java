package am.vector.payroll.api.adapter;

import am.vector.payroll.api.entity.main.Employment;
import am.vector.payroll.api.entity.main.Person;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class EmploymentTypeAdapterUnitTests extends BaseTypeAdapterUnitTest<EmploymentTypeAdapter, Employment> {

    private static Employment employment;
    private EmploymentTypeAdapter adapter;

    @BeforeClass
    public static void setup(){
        employment = new Employment()
            .setHireDate(LocalDate.of(2018, Month.MARCH, 30))
            .setTerminationDate(LocalDate.of(2019, Month.JULY, 10))
            .setVacationBalance(25)
            .setPerson(new Person());
    }

    @Before
    public void beforeTest() throws Exception {
        adapter = new EmploymentTypeAdapter();
        prepare(Arrays.asList(Employment.class, Person.class), adapter, employment, Collections.emptySet());
    }

    @Test
    public void write_OnlySelectedFieldsAreReturned() throws Exception {
        new EmploymentTypeAdapter("hireDate,terminationDate").write(writer, employment);

        verify(writer).name("hireDate");
        verify(writer).name("terminationDate");
        verify(writer, never()).name("vacationBalance");
    }

    @Test
    public void write_correctValuesAreReturned() throws Exception {
        adapter.write(writer, employment);

        verify(writer).value(ArgumentMatchers.eq("2018-3-30"));
        verify(writer).value(ArgumentMatchers.eq("2019-7-10"));
        verify(writer).value(ArgumentMatchers.eq(employment.getVacationBalance()));
    }
}
