package am.vector.journal.api.service;

import am.vector.common.exception.custom.EntityNotFoundException;
import am.vector.common.persistence.ClientCodeIdRepository;
import am.vector.common.persistence.CommonService;
import am.vector.common.persistence.UUIDEntity;
import am.vector.common.util.GeneralUtil;
import lombok.AllArgsConstructor;

import java.util.List;


@AllArgsConstructor
public abstract class BaseClientCodeIdService<T extends UUIDEntity> implements CommonService<T> {

    private ClientCodeIdRepository<T> repository;

    public List<T> findAllPerCompany() {
        int companyId = GeneralUtil.getCompanyId();
        return repository.findAllPerCompany(companyId * ID_LIMIT, (companyId+1) * ID_LIMIT);
    }

    public T findById(long id) {
        return findEntity(id);
    }

    public List<T> findAllById(List<Long> ids) {
        return repository.findAllByIdIn(ids);
    }

    public T findEntity(long id) {
        return repository.findById(id).orElseThrow(
            ()->new EntityNotFoundException(GeneralUtil.getObjectTypeFromId(id).getClazz(), GeneralUtil.encodeUuid(id))
        );
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public void deleteAll(int customerId) {
        int companyId = GeneralUtil.getCompanyId();
        repository.deleteAll(companyId * ID_LIMIT, (companyId+1) * ID_LIMIT);
    }
}
