package am.vector.payroll.util;

import am.vector.common.constants.Holiday;
import am.vector.common.util.CalculationUtil;
import am.vector.payroll.api.entity.admin.Workday;
import am.vector.payroll.api.support.PayrollPeriod;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.chrono.IsoChronology;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class DayUtil {

//    public static Map<LocalDate, ShiftDayModel> dayShifts = getDayShifts();

    public static String generatePeriod(LocalDate date){
        return date.format(DateTimeFormatter.ofPattern("MMYY"));
    }

    public static PayrollPeriod parsePeriod(String period){
        int year = Integer.parseInt("20" + period.substring(2));
        Month month = Month.of(Integer.parseInt(period.substring(0,2)));
        return new PayrollPeriod(year, month);
    }

    public static LocalDate parsePeriod(String period, int day){
        int year = Integer.parseInt("20" + period.substring(2));
        int month =Integer.parseInt(period.substring(0,2));
        if (day > 28) {
            int dom = 31;
            switch (month) {
                case 2:
                    dom = IsoChronology.INSTANCE.isLeapYear(year) ? 29 : 28;
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                default:
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    dom = 30;
            }

            if (day > dom) return null;
        }
        return LocalDate.of(year, month, day);
    }

    public static PayrollPeriod parsePeriod(int period){
        int year = period / 100;
        Month month = Month.of(period % 100);
        return new PayrollPeriod(year, month);
    }

    public static Workday generateDay(LocalDate day){
            Workday workday = new Workday(day);
            workday.setWeekDay(day.getDayOfWeek());
            Holiday holiday = CalculationUtil.getHolidayStatus(day);
            workday.setHoliday(holiday);

//            DayShift dayShift = getShiftStatus(day);
//            workday.setDayShift(dayShift);

        return workday;
    }

    public static Map<String, Object> parseWorkDays(Iterable<Workday> workdays, boolean isSaturdayNonWorking){
        Map<String, Object> workDaysParsed = new HashMap<>();
        workDaysParsed.put("list", workdays);

        int numOfWorkingDays = 0;
        List<Holiday> holidays = new LinkedList<>();
//        List<DayShift> dayShifts = new LinkedList<>();
        for(Workday workday : workdays) {
            if(workday.getWeekDay() == DayOfWeek.SUNDAY){
                continue;
            }
            if(workday.getWeekDay() == DayOfWeek.SATURDAY && isSaturdayNonWorking){
                continue;
            }
//            if(isStandardWeek && workday.getWeekDay() == DayOfWeek.SATURDAY){
//                if(workday.getDayShift() == DayShift.TO_WORKING) {
//                    dayShifts.add(workday.getDayShift());
//                } else continue;
//            }
            if(workday.getHoliday() != Holiday.NONE) {
                holidays.add(workday.getHoliday());
                continue;
            }
//            if(workday.getDayShift() == DayShift.TO_NONE_WORKING){
//                dayShifts.add(workday.getDayShift());
//                continue;
//            }
            numOfWorkingDays++;
        }
        workDaysParsed.put("numOfWorkingDays", numOfWorkingDays);
        workDaysParsed.put("holidays", holidays);
//        workDaysParsed.put("dayShifts", dayShifts);
        return workDaysParsed;
    }

/*
    private static DayShift getShiftStatus(LocalDate date){

        ShiftDayModel shiftDay = dayShifts.get(date);
        if(shiftDay != null){
          if(shiftDay.isWorking()){
              return DayShift.TO_WORKING;
          } else {
              return DayShift.TO_NONE_WORKING;
          }
        } else {
            return DayShift.NONE;
        }
    }

    private static Map<LocalDate, ShiftDayModel> getDayShifts(){
        Map<LocalDate, ShiftDayModel> shiftDays = new HashMap<>();
        Document doc = IOUtil.readXML("day_shift");
        NodeList nList = doc.getElementsByTagName("shiftDays");

        String dateShiftFrom, dateShiftTo, text, decree, reasonIsHoliday;
        LocalDate date;
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                ShiftDayModel shiftDayFrom = new ShiftDayModel();
                ShiftDayModel shiftDayTo = new ShiftDayModel();
                Element eElement = (Element) nNode;
                dateShiftFrom = eElement.getElementsByTagName("fromDate").item(0).getTextContent();
                dateShiftTo = eElement.getElementsByTagName("toDate").item(0).getTextContent();
                text = eElement.getElementsByTagName("text").item(0).getTextContent();
                decree = eElement.getElementsByTagName("decree").item(0).getTextContent();
                reasonIsHoliday = eElement.getElementsByTagName("reasonIsHoliday").item(0).getTextContent();

                date = LocalDate.parse(dateShiftFrom);
                shiftDayFrom.setWorking(false)
                        .setDate(date)
                        .setDecree(decree)
                        .setText(text)
                        .setReasonIsHoliday("true".equals(reasonIsHoliday));
                shiftDays.put(date, shiftDayFrom);

                date = LocalDate.parse(dateShiftTo);
                shiftDayFrom.setWorking(true)
                        .setDate(date)
                        .setDecree(decree)
                        .setText(text)
                        .setReasonIsHoliday("true".equals(reasonIsHoliday));
                shiftDays.put(date, shiftDayTo);
            }
        }
        return shiftDays;
    }
*/

    public static int removeMonthPeriod(int period){
        return (period % 100 != 1) ? --period : period - 101 + 12;
    }


    @SuppressWarnings("RedundantIfStatement")
    public static boolean isWorkingDay(Workday day, boolean isSaturdayNonWorking){
        if(day.getWeekDay() == DayOfWeek.SUNDAY) return false;
        if(day.getWeekDay() == DayOfWeek.SATURDAY && isSaturdayNonWorking) return false;
        if(day.getHoliday() != Holiday.NONE) return false;
        return true;
    }
}
