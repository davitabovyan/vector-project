package am.vector.payroll.api.service;

import am.vector.common.persistence.BaseService;
import am.vector.payroll.api.entity.main.Employment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public interface EmploymentService extends BaseService<Employment> {
	Logger logger = LogManager.getLogger();

	List<Employment> findAllPerPerson(long personId);
	List<Employment> findAll(boolean includeTerminated);
}
