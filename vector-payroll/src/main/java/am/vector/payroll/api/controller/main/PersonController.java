package am.vector.payroll.api.controller.main;

import am.vector.common.adapter.GsonAdapter;
import am.vector.common.model.payroll.PersonModel;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.Person;
import am.vector.payroll.api.service.PersonService;
import am.vector.payroll.util.Mapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/person", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class PersonController {

    private final PersonService personService;
    private final GsonPayrollAdapter adapter;

    @Transactional
    @PostMapping("/balk")
    public ResponseEntity<String> addAll(@RequestBody String jsonArray){
        JsonArray array = GsonAdapter.GSON.fromJson(jsonArray, JsonArray.class);
        List<Person> persons = new LinkedList<>();
        for(JsonElement obj: array){
            PersonModel person = GsonAdapter.GSON.fromJson(obj, PersonModel.class);
            person.setCompanyId(GeneralUtil.getCompanyId());
            persons.add(Mapper.map(person));
        }

        Iterable<Person> results = personService.saveAll(persons);
        return new ResponseEntity<>(adapter.toJson(results), HttpStatus.CREATED);
    }

    @PostMapping
    public ResponseEntity<Map<String,Object>> create(@RequestBody PersonModel model){
        model.setCompanyId(GeneralUtil.getCompanyId());

        Person person = personService.save(Mapper.map(model));
        return new ResponseEntity<>(GeneralUtil.uuidJSON(person), HttpStatus.CREATED);
    }

    @GetMapping("/{uuid}")
    public ResponseEntity<String> findById(@PathVariable String uuid,
                                   @RequestParam(value = "fields", required = false) String fields){
        Person person = personService.findById(GeneralUtil.decodeUuid(uuid));
        return ResponseEntity.ok(adapter.toJson(person, fields));
    }

    @GetMapping
    public ResponseEntity<String> findAll(@RequestParam(value = "fields", required = false) String fields){
        List<Person> results = personService.findAllPerCompany();
        return ResponseEntity.ok(adapter.toJson(results, fields));
    }

    @Transactional
    @PutMapping("/{uuid}")
    public void update(@RequestBody PersonModel person, @PathVariable String uuid){
        person.setCompanyId(GeneralUtil.getCompanyId());
        personService.update(Mapper.map(person));
    }

    @PatchMapping("/{uuid}")
    public void updateFields(@RequestBody PersonModel updated, @PathVariable String uuid) {
        Person entity = personService.findById(GeneralUtil.decodeUuid(uuid));
        entity.mergeObject(Mapper.map(updated));
        personService.update(entity);
    }

    @DeleteMapping("/{uuid}")
    public void delete(@PathVariable String uuid){
        personService.deleteById(GeneralUtil.decodeUuid(uuid));
    }
}
