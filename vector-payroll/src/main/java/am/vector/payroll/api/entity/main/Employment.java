package am.vector.payroll.api.entity.main;

import am.vector.common.constants.EntityObject;
import am.vector.common.persistence.TimestampId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Accessors(chain = true) @Getter @Setter
public class Employment extends TimestampId {

    private LocalDate hireDate;
    private LocalDate terminationDate;
    private Integer vacationBalance;

    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @ManyToOne(fetch=FetchType.LAZY, optional=false)
    private Person person;

    public Employment setPerson(Person person) {
        this.person = person;
        this.setCompanyId(person.getCompanyId());
        return this;
    }

    public Employment() {
        super(EntityObject.EMPLOYMENT);
    }

    public Employment(long uuid) {
        super(EntityObject.EMPLOYMENT, uuid);
    }

    public Employment copy(){
        return (Employment) super.copy(this);
    }
}
