package am.vector.payroll.api.controller.admin;

import am.vector.common.model.admin.CompanyModel;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.entity.admin.Company;
import am.vector.payroll.api.service.CompanyService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/api/v1/company", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class CompanyController {

    private final CompanyService companyService;
    private final Gson GSON = new Gson();


    @GetMapping
    public ResponseEntity<String> findAll(){
        Iterable<Company> list = companyService.findAll();
        return ResponseEntity.ok(GSON.toJson(list));
    }

    @GetMapping("/{id}/")
    public ResponseEntity<String> get(@PathVariable Integer id){
        Company company = companyService.findById(id);
        return ResponseEntity.ok(GSON.toJson(company));
    }

    @PostMapping
    public ResponseEntity<Map<String,String>> save(@RequestBody CompanyModel json){
        Company company = companyService.save(Company.fromMode(json));
        return new ResponseEntity<>(Collections.singletonMap("uuid", GeneralUtil.encodeUuid(company.getId())), HttpStatus.CREATED);
    }

    @PutMapping
    public void update(@RequestBody CompanyModel json){
        companyService.save(Company.fromMode(json));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        companyService.deleteById(id);
    }
}
