package am.vector.payroll.api.entity.main;

import am.vector.common.constants.EntityObject;
import am.vector.common.persistence.TimestampId;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@Entity
@Accessors(chain = true) @Getter @Setter
public class Contract extends TimestampId {

    private Integer salary;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer dailyHours;
    private Boolean laborRelation;
    private Integer workWeek;

    @ManyToOne(fetch = FetchType.LAZY, optional=false)
    private Person person;
    @ManyToOne(fetch = FetchType.LAZY, optional=false)
    private Role role;
    @ManyToOne(fetch = FetchType.LAZY, optional=false)
    private Department department;

    public boolean isLaborRelation(){
        return null != laborRelation && laborRelation;
    }

    public boolean isCivilContract(){
        return !laborRelation;
    }

    public Contract() {
        super(EntityObject.CONTRACT);
    }

    public Contract(long uuid) {
        super(EntityObject.CONTRACT, uuid);
    }

    public Contract copy(){
        return (Contract) super.copy(this);
    }
}
