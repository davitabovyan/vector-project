package am.vector.payroll.util;

import am.vector.common.adapter.GsonAdapter;
import am.vector.common.adapter.LocalDateAdapter;
import am.vector.common.constants.EntityObject;
import am.vector.common.model.UUIDModel;
import am.vector.common.model.common.DepartmentModel;
import am.vector.common.model.payroll.*;
import am.vector.common.persistence.UUIDEntity;
import am.vector.common.rest.Api;
import am.vector.common.util.GeneralUtil;
import am.vector.payroll.api.adapter.GsonPayrollAdapter;
import am.vector.payroll.api.entity.main.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class PayrollApi extends Api {

    private static final GsonAdapter adapter = new GsonPayrollAdapter();
    private static final BaseApi api = new BaseApi();
    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(LocalDate.class, new LocalDateAdapter().nullSafe()).create();

    private static final String EMPTY_OBJECT = "{}";

    public static void setTestApi() {
        Api.setApi(api);
    }

    public static <T extends UUIDModel> T parseJson(String json, Class<T> clazz) {
        return GSON.fromJson(json, clazz);
    }

    public static ResponseEntity<String> login(String host, String username, String password){
        ResponseEntity response = api.makePost(new HttpHeaders(), String.format("%slogin?username=%s&password=%s",
                host, username, password), "");
        if (response.getStatusCodeValue() != 200) throw new RuntimeException("login failed");
        return response;
    }

    public static List<Person> createPersons(HttpHeaders headers, String host, List<Person> persons){
        ResponseEntity<String> response = Api.createEntity(headers, host, adapter.toJson(persons),  EntityObject.PERSON,"balk");
        JsonArray results = GsonAdapter.GSON.fromJson(response.getBody(), JsonArray.class);

        List<Person> list = new LinkedList<>();
        for(JsonElement element : results) {
            Person person = Mapper.map(parseJson(element.toString(), PersonModel.class));
            list.add(person);
        }
        return list;
    }

    public static Person getPerson(HttpHeaders headers, String host, String uuid, Map<String, String> fields){
        ResponseEntity<String> response = Api.getEntityById(headers, host, uuid, fields, EntityObject.PERSON);
        return Mapper.map(parseJson(response.getBody(), PersonModel.class));
//        return adapter.fromJson(response.getBody(), Person.class, fields);
    }

    public static Person getPerson(HttpHeaders headers, String host, String uuid){
        return GsonAdapter.GSON.fromJson(Api.getEntityById(headers, host, uuid, null, EntityObject.PERSON).getBody(), Person.class);
    }

    public static Contract getContract(HttpHeaders headers, String host, String uuid, Map<String, String> fields){
        ResponseEntity<String> response = Api.getEntityById(headers, host, uuid, fields, EntityObject.CONTRACT);
        return Mapper.map(parseJson(response.getBody(), ContractModel.class));
    }

    public static Contract getContract(HttpHeaders headers, String host, String uuid){
        return getContract(headers, host, uuid, null);
    }

    public static Role getRole(HttpHeaders headers, String host, String uuid, Map<String, String> fields){
        ResponseEntity<String> response = Api.getEntityById(headers, host, uuid, fields, EntityObject.ROLE);
        return Mapper.map(parseJson(response.getBody(), RoleModel.class));
//        return adapter.fromJson(response.getBody(), Role.class, fields);
    }

    public static Role getRole(HttpHeaders headers, String host, String uuid){
        return getRole(headers, host, uuid, null);
    }

    public static Employment getEmployment(HttpHeaders headers, String host, String uuid, Map<String, String> fields){
        ResponseEntity<String> response = Api.getEntityById(headers, host, uuid, fields, EntityObject.EMPLOYMENT);
        return Mapper.map(parseJson(response.getBody(), EmploymentModel.class));
//        return adapter.fromJson(response.getBody(), Employment.class, fields);
    }

    public static Employment getEmployment(HttpHeaders headers, String host, String uuid) {
        return getEmployment(headers, host, uuid, null);
    }

    public static Absence getAbsence(HttpHeaders headers, String host, String uuid, Map<String, String> fields){
        ResponseEntity<String> response = Api.getEntityById(headers, host, uuid, fields, EntityObject.ABSENCE);
        return Mapper.map(parseJson(response.getBody(), AbsenceModel.class));
    }

    public static Absence getAbsence(HttpHeaders headers, String host, String uuid) {
        return getAbsence(headers, host, uuid, null);
    }

    public static List<Absence> getGroupAbsences(HttpHeaders headers, String host, long personId){
        List<String> queries = Collections.singletonList("personUuid="+GeneralUtil.encodeUuid(personId));
        ResponseEntity<String> response = api.makeGet(headers, Api.getUrl(host, EntityObject.ABSENCE, queries));
        String json = null != response.getBody() ? response.getBody() : EMPTY_OBJECT;
        AbsenceModel[] absences = GSON.fromJson(json, AbsenceModel[].class);
        Long parentId = absences[0].getId();
        return Arrays.stream(absences).map(a->a.setParentId(null)).map(Mapper::map).map(a->a.setParentId(parentId))
                .collect(Collectors.toList());
    }

    public static TimeSheet getTimesheet(HttpHeaders headers, String host, String uuid, Map<String, String> fields){
        ResponseEntity<String> response = Api.getEntityById(headers, host, uuid, fields, EntityObject.TIMESHEET);
        return Mapper.map(parseJson(response.getBody(), TimeSheetModel.class));
    }

    public static TimeSheet getTimesheet(HttpHeaders headers, String host, String uuid) {
        return getTimesheet(headers, host, uuid, null);
    }

    public static Department getDepartment(HttpHeaders headers, String host, String uuid, Map<String, String> fields){
        ResponseEntity<String> response = Api.getEntityById(headers, host, uuid, fields, EntityObject.DEPARTMENT);
        return Mapper.map(parseJson(response.getBody(), DepartmentModel.class));
    }

    public static Department getDepartment(HttpHeaders headers, String host, String uuid){
        return getDepartment(headers, host, uuid, null);
    }

    public static Payslip getPayslip(HttpHeaders headers, String host, String uuid, Map<String, String> fields){
        ResponseEntity<String> response = Api.getEntityById(headers, host, uuid, fields, EntityObject.PAYSLIP);
        return Mapper.map(parseJson(response.getBody(), PayslipModel.class));
    }

    public static Payslip getPayslip(HttpHeaders headers, String host, String uuid){
        return getPayslip(headers, host, uuid, null);
    }

    public static <T extends UUIDEntity> T createAndReturn(HttpHeaders headers, String host, String body, T entity) {
        String uuid = createCompactResponse(headers, host, body, entity.getObjCode()).getSecond();
        entity.setId(GeneralUtil.decodeUuid(uuid));
        return entity;
    }
}
