package am.vector.common.exception.custom;

import am.vector.common.constants.EntityObject;

public class IncorrectIdException extends ProvidedJsonException {
    public IncorrectIdException(String uuid, EntityObject entityObject) {
        this(uuid + "," + entityObject.getCode());
    }
    public IncorrectIdException(String message) {
        super(message);
    }
}
