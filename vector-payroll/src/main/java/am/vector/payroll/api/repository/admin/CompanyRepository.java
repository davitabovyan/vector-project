package am.vector.payroll.api.repository.admin;

import am.vector.payroll.api.entity.admin.Company;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

@RepositoryRestResource(collectionResourceRel = "company", path = "company", exported = false)
public interface CompanyRepository extends CrudRepository<Company, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM Company c where c.tin = :tin")
    void deleteById(@Param("tin") String tin);
}
