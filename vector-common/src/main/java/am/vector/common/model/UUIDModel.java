package am.vector.common.model;

import am.vector.common.constants.EntityObject;
import am.vector.common.constants.RecordState;
import am.vector.common.exception.VectorException;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.IOUtil;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.lang.reflect.Field;


@EqualsAndHashCode(of = {"uuid"})
@AllArgsConstructor
@NoArgsConstructor
public abstract class UUIDModel implements Serializable {

    private String uuid;
    private RecordState state;

    public abstract  int getCompanyId();

    public abstract int getObjCodeIndex();

    public abstract EntityObject getObjCode();

    public RecordState getState() {
        return state;
    }

    public UUIDModel setState(RecordState state) {
        this.state = state;
        return this;
    }

    public UUIDModel setId(long id) {
        this.uuid = GeneralUtil.encodeUuid(id);
        return this;
    }

    public Long getId() {
        return GeneralUtil.decodeUuid(uuid);
    }

    public String getUuid() {
        return uuid;
    }

    public UUIDModel setUuid(String uuid) {
        this.uuid = uuid;
//        this.id = GeneralUtil.decodeUuid(uuid);
        return this;
    }

    public UUIDModel copy(Object orig) {
        Object obj = IOUtil.copy(orig);
        return  (UUIDModel) obj;
//        if(null != copy){
//            copy.setId(0);
//        }
//        return copy;
    }

    public <T> void mergeObject(T entity) {
        Class<?> clazz = entity.getClass();
        if (this.getClass() != clazz) throw new VectorException("Merging incompatible objects");
        Field[] fields = clazz.getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                Object newValue = field.get(entity);
                if (newValue != null) field.set(this, newValue);
            }
        }catch (IllegalAccessException ex){
            throw new VectorException("",ex);
        }
    }

}
