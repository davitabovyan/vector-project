package am.vector.security.entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import am.vector.common.constants.EntityObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Accessors(chain = true) @Getter @Setter
@NoArgsConstructor
public class User implements UserDetails {

    transient private static final EntityObject OBJ = EntityObject.USER;

    @Id
    private String username;
    @ManyToOne(fetch = FetchType.LAZY, optional=false)
    private Client client;
    private int companyId;
    private String password;
    private long readAccess;
    private long editAccess;
    private String locale;
    private boolean isAdmin;
    private boolean isSuperAdmin;
    private boolean enabled;
    private boolean emailVerified;

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return null;//Collections.singletonList(new Authority(isAdmin));
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

}
