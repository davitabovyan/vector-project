package am.vector.common.config;

import am.vector.common.exception.VectorException;
import am.vector.common.exception.custom.*;
import am.vector.common.exception.generic.BadRequestVectorException;
import am.vector.common.exception.generic.ConflictVectorException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.rest.core.RepositoryConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.stream.Collectors;

@ControllerAdvice
@SuppressWarnings("unused")
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final HttpHeaders httpHeaders = new HttpHeaders();
    private static final Logger logger = LogManager.getLogger();

    @Autowired
    private Messages messages;

    //TODO find where it is applicable
    @ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        return logAndHandle(ex, request,"Incorrect argument values provided", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> duplicateKey(DataIntegrityViolationException ex, WebRequest request) {
        return logAndHandle(null, request, ex.getCause().getCause().getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    protected ResponseEntity<Object> emptyGetResult(EmptyResultDataAccessException ex, WebRequest request) {
        return handleExceptionInternal(ex, "{}", httpHeaders, HttpStatus.OK, request);
    }

    @ExceptionHandler(BadCredentialsException.class)
    protected ResponseEntity<Object> authenticationFailed(BadCredentialsException ex, WebRequest request) {
        return logAndHandle(ex, request, ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(AccessDeniedException.class)
    protected ResponseEntity<Object> handleUnauthorisedMethodAccess(AccessDeniedException ex, WebRequest request) {
        return logAndHandle(ex, request, ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    //TODO check is still needed
    @ExceptionHandler(RepositoryConstraintViolationException.class)
    public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
        RepositoryConstraintViolationException nevEx = (RepositoryConstraintViolationException) ex;

        String errors = nevEx.getErrors().getAllErrors().stream()
                .map(ObjectError::toString).collect(Collectors.joining("\n"));

        return new ResponseEntity<>(errors, httpHeaders, HttpStatus.PARTIAL_CONTENT);
    }

    //region Generic Vector exceptions

    @ExceptionHandler(BadRequestVectorException.class )
    protected ResponseEntity<Object> handleBadRequestVectorException(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.getGeneric(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConflictVectorException.class )
    protected ResponseEntity<Object> handleConflictVectorException(RuntimeException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.getGeneric(ex.getMessage()), HttpStatus.CONFLICT);
    }

    //endregion

    //region Custom Vector exceptions

    @ExceptionHandler(VectorException.class )
    protected ResponseEntity<Object> handleGeneralException(RuntimeException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.general", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    //TODO check where is used
    @ExceptionHandler(RequiredFieldMissingException.class)
    public ResponseEntity<Object> handleIncompleteJsonException(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.missing_field", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNameAlreadyTakenException.class)
    public ResponseEntity<Object> handleDuplicateUserNameException(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.registration.used_username", ex.getMessage()),
                HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntityNotFoundException.class )
    protected ResponseEntity<Object> handleNoSuchElement(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.not_found", ex.getMessage()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ActionNotAllowedForUserException.class )
    protected ResponseEntity<Object> handleActionNotAllowed(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.not_allowed", ex.getMessage()),
                HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(FailedReadingFileException.class )
    protected ResponseEntity<Object> handleFileUploadFailure(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.not_allowed", ex.getMessage()),
                HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(RetryActionException.class )
    protected ResponseEntity<Object> handleSomethingWentWrong(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.retry_action", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IncorrectIdException.class )
    protected ResponseEntity<Object> handleIncorrectIdLength(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.incorrect_id", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnsupportedFieldException.class )
    protected ResponseEntity<Object> handleUnsupportedField(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.unsupported_field", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnsupportedValueException.class )
    protected ResponseEntity<Object> handleUnsupportedValue(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.unsupported_value", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

        @ExceptionHandler(UnsupportedJsonKeyException.class )
    protected ResponseEntity<Object> handleIncorrectJsonKey(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.incorrect_json.key", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnsupportedJsonValueException.class )
    protected ResponseEntity<Object> handleIncorrectJsonValue(VectorException ex, WebRequest request) {
        return logAndHandle(ex, request, messages.get("error.incorrect_json.value", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }
    //endregion

    private ResponseEntity<Object> logAndHandle(RuntimeException ex, WebRequest request, String message, HttpStatus status){

        String stackTrace = "";
        String cause = "";
        if(null != ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            stackTrace = sw.toString();
            cause = null != ex.getCause() ? ex.getCause().getMessage() : "";
        }
        logger.printf(
                Level.INFO,
                "{\"date\": %d, \"name\": \"warning\", \"sessionId\": \"%s\", \"status\": \"%d\", \"warningMessage\": \"%s caused by %s\", \"errorTrace\": \"%s\"}",
                System.currentTimeMillis(),
                request.getSessionId(),
                status.value(),
                message,
                cause,
                stackTrace
        );
        return new ResponseEntity<>(message, httpHeaders, status);
    }
}