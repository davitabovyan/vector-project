package am.vector.common.constants.journal;

public enum AccountGroupType {
    SUPPLIER,
    CLIENT,
    ON_ACCOUNT,
    REVENUE,
    EXPENSE,
    NOT_APPLICABLE
}
