package am.vector.payroll;

import am.vector.payroll.api.v1.integration.controller.ControllersTestSuite;
import am.vector.payroll.api.v1.unit.UnitTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
		ControllersTestSuite.class,
		UnitTestSuite.class
})
public class PayrollApplicationTests {
}
