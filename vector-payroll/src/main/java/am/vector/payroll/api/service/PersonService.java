package am.vector.payroll.api.service;

import am.vector.common.persistence.CommonService;
import am.vector.payroll.api.entity.main.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface PersonService extends CommonService<Person> {
    Logger logger = LogManager.getLogger();

//    List<Person> saveAll(User user, Iterable<Person> objects);
}
