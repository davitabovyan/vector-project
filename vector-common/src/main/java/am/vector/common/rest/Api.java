package am.vector.common.rest;

import am.vector.common.adapter.LocalDateAdapter;
import am.vector.common.constants.EntityObject;
import am.vector.common.constants.Module;
import am.vector.common.model.MainModel;
import am.vector.common.model.UUIDModel;
import am.vector.common.util.GeneralUtil;
import am.vector.common.util.Pair;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class Api {

    private static BaseApiInterface baseApi = new BaseApi();
    private static final String host;

    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(LocalDate.class, new LocalDateAdapter().nullSafe()).create();

    private static final HttpHeaders httpHeaders = new HttpHeaders();
    static {
        host = "https://localhost:";
        httpHeaders.add("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJlZDc5NDZmOC1hZGUwLTQ3OTEtYTA5OS1lZDY5YmU4NDFlZGUiLCJzYSI6dHJ1ZSwiYWQiOnRydWUsInJkIjo5MjIzMzcyMDM2ODU0Nzc1ODA3LCJlZCI6OTIyMzM3MjAzNjg1NDc3NTgwNywiY3AiOiI0MGNkYmMiLCJzdWIiOiJkYXZpdC5hYm92eWFuQGdtYWlsLmNvbTwwMDExMTQ3Mj4iLCJleHAiOjE2MDQxNTgzNTF9.DmMQ731_VqfK2fvCYe8VO1DufT2S-rRYFPiBfF46tdY");
        httpHeaders.add("Content-Type", "application/json");
    }

    public static void setApi(BaseApiInterface api) {
        baseApi = api;
    }

    public static <T extends UUIDModel> T parseJson(String json, Class<T> clazz) {
        return GSON.fromJson(json, clazz);
    }

    public static <T extends MainModel> T createAndReturn(HttpHeaders headers, String host, String body, T entity) {
        String uuid = createCompactResponse(headers, host, body, entity.getObjCode()).getSecond();
        entity.setId(GeneralUtil.decodeUuid(uuid));
        return entity;
    }

    public static Pair<Integer, String> create(Module module, String body, EntityObject objCode){
        return createCompactResponse(httpHeaders, host + module.getPort() + "/", body,  objCode);
    }

    public static Pair<Integer, String> create(HttpHeaders headers, String host, String body, EntityObject objCode){
        return createCompactResponse(headers, host, body,  objCode);
    }

    public static ResponseEntity<String> update(HttpHeaders headers, String host, String body, long id, EntityObject objCode){
        return updateEntity(headers, host, body, GeneralUtil.encodeUuid(id), objCode);
    }

    public static ResponseEntity<String> updateFields(
            HttpHeaders headers, String host, String body, long id, EntityObject objCode){

        return updateFields(headers, host, body, GeneralUtil.encodeUuid(id), objCode);
    }

    public static Pair<Integer, String> createCompactResponse(HttpHeaders headers, String host, String body, EntityObject entityObject){
        ResponseEntity<String> response = createEntity(headers, host, body, entityObject);

        JsonObject result = GSON.fromJson(response.getBody(), JsonObject.class);
        String uuid = "";
        if(!result.isJsonNull() && result.has("uuid")) {
            uuid = result.get("uuid").getAsString();
        }
        return new Pair<>(response.getStatusCodeValue(), uuid);
    }

    public static ResponseEntity<String> createEntity(HttpHeaders headers, String host, String body, EntityObject entityObject){
        return createEntity(headers, host, body, entityObject , null);
    }

    public static ResponseEntity<String> getEntityById(Module module, int id, EntityObject entityObject){
        return getEntityById(httpHeaders, host + module.getPort() + "/", ""+id, null, entityObject);
    }

    public static ResponseEntity<String> getEntityById(HttpHeaders headers, String host, long id, EntityObject entityObject){
        return getEntityById(headers, host, GeneralUtil.encodeUuid(id), null, entityObject);
    }

    public static ResponseEntity<String> getEntityById(HttpHeaders headers, String host, long id, EntityObject entityObject, Map<String, String> fields){
        return getEntityById(headers, host, GeneralUtil.encodeUuid(id), fields, entityObject);
    }

    public static ResponseEntity<String> getEntityById(HttpHeaders headers, String host, String uuid, Map<String, String> fields, EntityObject entityObject){
        String requestedFields;
        if(fields == null){
            requestedFields = "*";
        } else {
            StringBuilder sb = new StringBuilder();
            for(Map.Entry<String, String> field : fields.entrySet()){
                sb.append(field.getKey());
                sb.append(",");
            }
            requestedFields = sb.toString();
            requestedFields = requestedFields.substring(0,requestedFields.length()-1);
        }
        return baseApi.makeGet(headers, getUrl(host, entityObject, uuid, requestedFields));
    }

    public static int removeEntityById(HttpHeaders headers, String host, String uuid, EntityObject entityObject){
        return baseApi.makeDelete(headers, getUrl(host, entityObject, uuid, null)).getStatusCodeValue();
    }

    public static ResponseEntity<String> createEntity(HttpHeaders headers, String host, String body, EntityObject entityObject, String other){
        return baseApi.makePost(headers, getUrl(host, entityObject, other, null), body);
    }

    public static ResponseEntity<String> updateEntity(HttpHeaders headers, String host, String body, String uuid, EntityObject entityObject){
        return baseApi.makePut(headers, getUrl(host, entityObject, uuid, null), body);
    }

    public static ResponseEntity<String> updateFields(HttpHeaders headers, String host, String body, String uuid, EntityObject entityObject){
        return baseApi.makePatch(headers, getUrl(host, entityObject, uuid, null), body);
    }

    public static String getUrl(String host, EntityObject entityObject, String uuid, String fields){
        StringBuilder sb = new StringBuilder(10);
        sb.append(host).append("api/v1/").append(entityObject.getCode()).append("/");
        if(null != uuid){
            sb.append(uuid).append("/");
        }
        sb.append("?fields=").append(null != fields ? fields : "*");
        return sb.toString();
    }

    public static String getUrl(String host, EntityObject entityObject, List<String> queries){
        return getUrl(host, entityObject, null, queries, null);
    }

    public static String getUrl(String host, EntityObject entityObject, String uuid, List<String> queries, String fields){
        StringBuilder sb = new StringBuilder();
        sb.append(host).append("api/v1/").append(entityObject.getCode()).append("/");
        if(null != uuid){
            sb.append(uuid).append("/");
        }
        sb.append("?");
        for(String query : queries){
            sb.append(query);
            sb.append("&");
        }
        sb.append("fields=").append(null != fields ? fields : "*");
        return sb.toString();
    }

//    public static ResponseEntity<String> makeGet(HttpHeaders headers, String uri){
//        return restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity<>(headers), String.class);
//    }
//
//    public static ResponseEntity<String> makePost(HttpHeaders headers, String uri, String body){
//        return restTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<>(body, headers), String.class);
//    }
//
//    public static ResponseEntity<String> makePut(HttpHeaders headers, String uri, String body){
//        return restTemplate.exchange(uri, HttpMethod.PUT, new HttpEntity<>(body, headers), String.class);
//    }
//
//    public static ResponseEntity<String> makePatch(HttpHeaders headers, String uri, String body){
//        return restTemplate.exchange(uri, HttpMethod.PATCH, new HttpEntity<>(body, headers), String.class);
//    }
//
//    public static ResponseEntity<String> makeDelete(HttpHeaders headers, String uri) {
//        return restTemplate.exchange(uri, HttpMethod.DELETE, new HttpEntity<>(headers), String.class);
//    }
}
